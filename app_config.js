module.exports = {
  env: {
    local: {
      ENV: 'local',
      NODE_ENV: 'development',
      DADATA_AUTHORIZATION_TOKEN: 'f2609d24a773d7211b420460b9313ad12dea159c',
      API_PROXY_URL: 'https://api.test2.finspin.ru',
      METRICS: false,
      ERROR_LOG: false,
      REDUX_LOG: true,
      RECAPTCHA_SITE_KEY: '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI'
    },
    test: {
      ENV: 'test',
      NODE_ENV: 'development',
      DADATA_AUTHORIZATION_TOKEN: 'f2609d24a773d7211b420460b9313ad12dea159c',
      API_PROXY_URL: 'https://api.test.finspin.ru',
      METRICS: true,
      ERROR_LOG: true,
      REDUX_LOG: true,
      RECAPTCHA_SITE_KEY: '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI'
    },
    test2: {
      ENV: 'test2',
      NODE_ENV: 'development',
      DADATA_AUTHORIZATION_TOKEN: 'f2609d24a773d7211b420460b9313ad12dea159c',
      API_PROXY_URL: 'https://api.test2.finspin.ru',
      METRICS: true,
      ERROR_LOG: true,
      REDUX_LOG: true,
      RECAPTCHA_SITE_KEY: '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI'
    },
    production: {
      ENV: 'production',
      NODE_ENV: 'production',
      DADATA_AUTHORIZATION_TOKEN: '0e26df7b53ec534412d1a5535bc94b6100de7a19',
      API_PROXY_URL: 'https://api.finspin.ru',
      METRICS: true,
      ERROR_LOG: true,
      REDUX_LOG: false,
      RECAPTCHA_SITE_KEY: '6LdhppMUAAAAADzucCsQdPhuczqNR0wxXaXAIzMN'
    }
  },

  remote: {
    test: '10.111.68.132',
    test2: '10.111.68.132',
    production: '10.111.69.67'
  },

  string: {
    default: {
      SUPPORT_PHONE: '8005558716'
    },
    tele2: {
      SUPPORT_PHONE: '8007070658'
    },
    beeline: {
      SUPPORT_PHONE: '8005558716'
    }
  }
}
