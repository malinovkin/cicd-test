module.exports = {
    "apps": [
      {
        "exec_mode": "fork_mode",
        "script": "server.js",
        "name": "beeline",
        "cwd": "./src/",
        "args": "-p 3000",
        "watch": true,
        "max_memory_restart": "300M",
        "error_file": "/var/log/node/beeline.err.log",
        "out_file": "/var/log/node/beeline.out.log"
      }
    ]
  }
  