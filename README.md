# Finspin Loans web client

## Requirements

* [Node js](https://nodejs.org/en/) latest LTS version;

## How to develop and build

At first, read this [EBC frontend development guide](https://ebcteam.atlassian.net/wiki/spaces/ebc/pages/67994582/How+We+Develop+Frontend).

```
npm install         - Install project dependencies (shortcut: npm i).

npm run test        - Run the tests (shortcut: npm t).
npm run lint        - Lint JavaScript.

For the next commands additional info see in the appropriate files in the bin folder.

npm run start       - Start dev server, watch for changes (shortcut: npm start).
npm run build       - Build a dev version of the app.
npm run deploy      - Deploy builds on a remote server.

                      For deploy you should create a file "identity.json"
                      in the root folder with the following structure:

                      {
                        "filepath": "PATH_TO_YOUR_IDENTITY_FILE_HERE",
                        "username": "REMOTE_USER_NAME_HERE"
                      }
```

## Contributors

* Ivan Shvetsov, [shvetsov@exbico.ru](shvetsov@exbico.ru)
