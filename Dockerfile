# FROM node:10.16.3-buster-slim As build

# COPY . .

# RUN set -xe                                                                                                         \
#   && echo 'deb http://mirror.yandex.ru/debian stretch main contrib non-free' >> /etc/apt/sources.list               \
#   && echo 'deb http://mirror.yandex.ru/debian stretch-updates main contrib non-free' >> /etc/apt/sources.list       \
#   && echo 'deb http://security.debian.org/ stretch/updates main contrib non-free' >> /etc/apt/sources.list          \
#   && echo 'deb http://mirror.yandex.ru/debian stretch-backports main contrib non-free' >> /etc/apt/sources.list     \
#   && apt-get update                                                                                                 \
#   && apt-get install -y git                                                                                         \
#   && rm -rf /var/lib/{apt,dpkg,cache,log}/                                                                          \
#   && apt-get clean

# RUN npm install \
#     && npm run build -- -s -t eng -e test \
#     && npm run sentry -- eng
#-----------------------------------------------------------------------

# FROM keymetrics/pm2:8-stretch
FROM node:10.16.3-buster-slim

LABEL maintainer="malinovkin@exbico.com"

# COPY --chown=node:node --from=build /build/eng/index.html /src/
# COPY --chown=node:node --from=build /build/eng/server.js* /src/

COPY build/eng/index.html /src/
COPY build/eng/server.js* /src/

# Nginx static artifacts
#COPY --chown=node:node --from=build /build/eng/{index.html,server.js*} /src/

RUN set -xe                                                                                                         \
  && echo 'deb http://mirror.yandex.ru/debian stretch main contrib non-free' >> /etc/apt/sources.list               \
  && echo 'deb http://mirror.yandex.ru/debian stretch-updates main contrib non-free' >> /etc/apt/sources.list       \
  && echo 'deb http://security.debian.org/ stretch/updates main contrib non-free' >> /etc/apt/sources.list          \
  && echo 'deb http://mirror.yandex.ru/debian stretch-backports main contrib non-free' >> /etc/apt/sources.list     \
  && apt-get update                                                                                                 \
  && apt-get install -y git apt-transport-https lsb-release ca-certificates gnupg gnupg-agent gnupg-l10n gnupg2     \
  && rm -rf /var/lib/{apt,dpkg,cache,log}/                                                                          \
  && apt-get clean

#apt-get -y install aptitude joe nano vim; \
	# aptitude -y install apt-transport-https lsb-release ca-certificates sudo; \
	# aptitude -y install wget atop htop tcpdump apt-file dnsutils net-tools telnet; \
	# aptitude -y install gnupg gnupg-agent gnupg-l10n gnupg2; \
	# aptitude -y update; apt-file update; aptitude -y full-upgrade


# Bundle Pre-Build APP files
COPY --chown=node:node ecosystem.config.js .
COPY --chown=node:node healthcheck.sh .

# It's a good idea to use dumb-init to help prevent zombie chrome processes.
ADD https://github.com/Yelp/dumb-init/releases/download/v1.2.2/dumb-init_1.2.2_amd64 /usr/local/bin/dumb-init
RUN chmod +x /usr/local/bin/dumb-init /healthcheck.sh

# Install app dependencies
ENV NPM_CONFIG_LOGLEVEL warn

EXPOSE 3000

RUN npm install 

# Create folder for logs
RUN npm install pm2 -g \
  && mkdir /var/log/node \
  && chown node:node /var/log/node

USER node

ENTRYPOINT ["dumb-init", "--"]
CMD [ "pm2-runtime", "start", "ecosystem.config.js" ]

HEALTHCHECK \
    --interval=30s \
    --timeout=30s  \
    --start-period=5s \
    --retries=3 CMD [ "/healthcheck.sh" ]
