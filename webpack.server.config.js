const R = require('ramda')
const path = require('path')
const webpack = require('webpack')

const AppConfig = require('./app_config')

module.exports = {
  mode: AppConfig.env[process.env.ENV].NODE_ENV,
  entry: './src/server/server.js',
  target: 'node',
  devtool: process.env.ENV === 'local' ? 'cheap-module-eval-source-map' : 'source-map',
  stats: 'errors-only',

  output: {
    path: path.resolve(__dirname, `build/${process.env.THEME}`),
    filename: 'server.js'
  },

  node: {
    __filename: true,
    __dirname: true
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      }
    ]
  },

  resolve: {
    modules: [
      path.resolve(__dirname, 'src'),
      'node_modules'
    ],

    extensions: ['.js', '.jsx']
  },

  externals: {
    express: 'commonjs express',
    superagent: 'commonjs superagent',
    compression: 'commonjs compression',
    winston: 'commonjs winston',
    // newrelic: 'commonjs newrelic',
    'http-proxy-middleware': 'commonjs http-proxy-middleware'
  },

  plugins: [
    new webpack.DefinePlugin({
      'process.env': R.mapObjIndexed(
        JSON.stringify,
        R.mergeAll([
          AppConfig.env[process.env.ENV],
          AppConfig.string[process.env.THEME],
          {
            THEME: process.env.THEME,
            REDUX_LOG: false
          }]))
    }),

    new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /ru/)
  ]
}
