#!/usr/bin/env sh

if [ -z "$1" ]
then
    THEME=default
else
    THEME="$1"
fi

CMD=node_modules/@sentry/cli/sentry-cli
VERSION=`${CMD} releases propose-version`
ARTIFACT_DIR=build/"$THEME"/static/js

$CMD releases files "$VERSION" upload-sourcemaps $ARTIFACT_DIR
