const R = require('ramda')
const fs = require('fs')
const shell = require('shelljs')
const cli = require('commander')

const packageJSON = require('../package.json')

const RESOURCES_DIR = 'resources'
const BUILD_DIR = 'build'

cli
  .option('-t --theme [theme]', 'Set theme', /^(default|tele2|beeline|eng)$/i, 'default')
  .option('-e --env [env]', 'Set environment', /^(production|test2|test)$/i, 'test')
  .option('-s') // for backward compatibility with deploy script (need to remove this flag from deploy script)
  .parse(process.argv)

const pParam = cli.env === 'production' ? '-p' : ''
const browserCmd = `cross-env ENV=${cli.env} webpack ${pParam}`
const nodeCmd = `cross-env ENV=${cli.env} webpack --config=webpack.server.config.js ${pParam}`

shell.env.THEME = cli.theme
shell.env.ENV = cli.env

// Run tests
if (shell.exec('npm run test').code !== 0) {
  process.exit(1)
}

// Clean up previous build
shell.rm('-rf', `${BUILD_DIR}/${cli.theme}`)

// Build both client and server
shell.exec(`concurrently "${browserCmd}" "${nodeCmd}"`)

// Require webpack config after initializing of ENV process.env variable
const webpackServerConfig = require('../webpack.server.config')

// Prepare package.json config for server app
fs.writeFileSync(
  `./${BUILD_DIR}/${cli.theme}/package.json`,
  JSON.stringify(
    R.keys(webpackServerConfig.externals).reduce(
      (acc, key) => R.assocPath(['dependencies', key], packageJSON.dependencies[key], acc),
      {dependencies: {}})),
  null, 2)

// Copy some public resources
if (cli.env !== 'production') {
  shell.cp(`${RESOURCES_DIR}/robots.txt`, `${BUILD_DIR}/${cli.theme}`)
}
shell.cp(`${RESOURCES_DIR}/labor.html`, `${BUILD_DIR}/${cli.theme}`)
shell.mkdir(`${BUILD_DIR}/${cli.theme}/static/doc`)
shell.cp(`${RESOURCES_DIR}/Сводная ведомость_финспин.pdf`, `${BUILD_DIR}/${cli.theme}/static/doc`)
