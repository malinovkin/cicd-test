const shell = require('shelljs')
const cli = require('commander')

const Utils = require('./utils')
const AppConfig = require('../app_config')
const Identity = require('../identity.json')

Utils.isCLIToolAvailable('ssh')

cli
  .option('-e --env [env]', 'Select an environment', /^(test2|test|production)$/i, 'test')
  .option('-t --theme [theme]', 'Select the theme', /^(all|default|tele2|beeline)$/i, 'all')
  .option('-b --branch <branch>', 'Select a git branch')
  .parse(process.argv)

const host = AppConfig.remote[cli.env]
const branch = cli.env === 'production' ? (cli.branch || 'master') : (cli.branch || Utils.getHeadBranchName())

shell.exec(`ssh -i ${Identity.filepath} ${Identity.username}@${host} "sudo deploy_frontend.sh -s ${cli.env} -p mfo -t ${cli.theme} -b ${branch}"`)
