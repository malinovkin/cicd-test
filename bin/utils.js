const shell = require('shelljs')

module.exports = {
  isCLIToolAvailable (name) {
    if (!shell.which(name)) {
      shell.echo(`Sorry, this script requires ${name}`)
      shell.exit(1)
    }
  },

  exec (cmd) {
    const result = shell.exec(cmd)
    if (result.stderr) {
      throw new Error(result.stderr)
    }
  },

  getHeadSHA () {
    return shell.exec('git rev-parse HEAD', {silent: true})
      .stdout.replace(/\n/g, '')
  },

  getHeadBranchName () {
    return shell.exec('git rev-parse --abbrev-ref HEAD', {silent: true})
      .stdout.replace(/\n/g, '')
  }
}
