const shell = require('shelljs')
const cli = require('commander')

cli
  .option('-t --theme [theme]', 'Select theme', /^(default|tele2|beeline|eng)$/i, 'default')
  .parse(process.argv)

const serverCmd = 'cross-env ENV=local webpack --config=webpack.server.config.js'
const clientCmd = 'cross-env ENV=local webpack'
const nodemonCmd = `cd ./build/${cli.theme} && nodemon -e js,html --ignore static/ ./server.js -p 8080`

shell.rm('-rf', 'build')

shell.env.THEME = cli.theme

// Before nodemon, build files must exist
shell.exec(serverCmd)
shell.exec(clientCmd)

// Run client/server webpacks and nodemon for server code reloading
shell.exec(`concurrently "${serverCmd} -w" "${clientCmd} -w" "${nodemonCmd}"`)
