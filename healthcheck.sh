#!/bin/bash

# set -ex

# echo "Curling against the NodeJS server"
# echo "Should expect a 200"

URL=(http://127.0.0.1:3000/ping)
STATUS_CODE=$(curl -sk -w "%{http_code}" "$URL" -o /dev/null \
    --max-time 15)

if [[ "$STATUS_CODE" == "200" ]]; then
    echo "NodeJS has come up and ready to use"
    exit 0
else
    echo "NodeJS did not return a correct status code."
    echo "Returned: $STATUS_CODE"
    exit 1
fi
