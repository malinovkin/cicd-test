import * as R from 'ramda'
import moment from 'moment'
import SuperAgent from 'superagent'

function hasValue (x) {
  return !R.isNil(x) && !R.isEmpty(x)
}

let serverErrorHandler = () => {}

function convertApplicationDTO (dto) {
  function convertAddress (address) {
    if (!address) return null
    return R.pipe(
      R.assoc('value', address.value),
      R.assoc('unrestrictedValue', address.unrestrictedValue),
      R.assoc(
        'street',
        R.path(['data', 'streetIsAbsent'], address)
          ? 'Без улицы'
          : R.path(['data', 'street'], address)
      ),
      R.assoc('withoutFlat', R.path(['data', 'flatIsAbsent'], address))
    )(R.filter(hasValue, address.data))
  }

  const profile = dto.profile || {}
  return {
    profile: R.pipe(
      R.mergeLeft({
        registrationAddress: convertAddress(profile.registrationAddress),
        factAddress: convertAddress(profile.factAddress),
        jobAddress: convertAddress(profile.jobAddress)
      }),
      R.filter(hasValue)
    )(profile),
    required: dto.required || [],
    fixed: dto.disabled || []
  }
}

function convertToApplicationDTO (application) {
  function convertAddress (address) {
    if (!address) return null
    const data = R.pipe(
      R.dissoc('value'),
      R.dissoc('unrestrictedValue'),
      R.mergeLeft({
        street: address.street === 'Без улицы' ? '' : address.street,
        streetIsAbsent: address.street === 'Без улицы'
      }),
      R.mergeLeft({
        flat: address.withoutFlat ? '' : address.flat,
        flatIsAbsent: address.withoutFlat
      }),
      R.dissoc('withoutFlat'),
      R.filter(hasValue)
    )(address)

    return {
      value: address.value,
      unrestrictedValue: address.unrestrictedValue,
      data
    }
  }

  return R.pipe(
    R.mergeLeft({
      registrationAddress: convertAddress(application.registrationAddress),
      jobAddress: convertAddress(application.jobAddress),
      factAddress: convertAddress(application.factAddress)
    }),
    R.filter(hasValue)
  )(application)
}

function convertApplicationValidationErrors (errors) {
  function convertAddressErrors (errors, addressName) {
    return {
      region: errors[`${addressName}.data.region`],
      city: errors[`${addressName}.data.city`],
      street: errors[`${addressName}.data.street`],
      house: errors[`${addressName}.data.house`],
      flat: errors[`${addressName}.data.flat`]
    }
  }

  function getErrorsWithoutFlatAddress (errors, addressesName) {
    return R.pipe(
      R.dissoc(`${addressesName[0]}.data.flat`),
      R.dissoc(`${addressesName[1]}.data.flat`),
      R.dissoc(`${addressesName[2]}.data.flat`)
    )(errors)
  }

  return R.mergeRight(
    getErrorsWithoutFlatAddress(errors, [
      'registrationAddress',
      'factAddress',
      'jobAddress'
    ]),
    {
      registrationAddress: convertAddressErrors(errors, 'registrationAddress'),
      factAddress: convertAddressErrors(errors, 'factAddress'),
      jobAddress: convertAddressErrors(errors, 'jobAddress')
    }
  )
}

function transformValidationErrors (arr) {
  const newObj = arr.reduce(
    (acc, error) => R.assoc(error.path, error.message, acc),
    {}
  )
  return newObj
}

function transformErrorResponse (fn) {
  return res => {
    const { body } = res.response
    const error = new Error('ERROR')
    error.status = res.status
    if (res.status === 400) {
      error.data = fn(transformValidationErrors(body.details))
    } else {
      error.data = body.details
    }
    throw error
  }
}

export function setServerErrorHandler (handler) {
  serverErrorHandler = handler
}

function makeAPIRequest (method, url, params = {}) {
  const headers = R.merge(
    {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'Request-Date': new Date().toISOString()
    },
    params.headers
  )

  let agent = SuperAgent(method, `/api${url}`)
    .withCredentials()
    .set(headers)

  const body = params.body

  if (method === 'post' && body) {
    agent = agent.send(body)
  } else if (method === 'get' && body) {
    agent = agent.query(body)
  }
  return agent.on('error', serverErrorHandler)
}

export const API = {
  fetchNewLoanOptions () {
    return makeAPIRequest('get', '/v3/loanConditions').then(({ body }) => body)
  },

  fetchLoanStatistics () {
    return makeAPIRequest('get', '/v3/statistics').then(({ body }) => {
      return {
        issuedLoansTotal: body.issuedLoansTotal,
        issuedLoansToday: body.issuedLoansToday,
        approvalPercentage: body.approvalPercentage
      }
    })
  },

  fetchUser () {
    return makeAPIRequest('get', '/v3/user').then(({ body }) => ({
      permissions: {
        newLoan: body.canCreateNewLoan,
        emptyArchiveLoans: body.emptyArchiveLoans
      },
      userPhone: body.phone
    }))
  },

  requestSMS (phone) {
    const now = new Date()

    return makeAPIRequest('post', '/v3/sendVerificationCode', {
      body: { phone },
      headers: {
        'Request-Token': window
          .btoa(now.toISOString() + '82EF09DB7BE7F7236FCD53D4F7B26B7F')
          .toLowerCase(),
        'Request-Date': now.toISOString()
      }
    })
      .then(({ body }) => ({
        primaryUser: body.isNewUser,
        resendTimeout: body.canRepeatAfter
      }))
      .catch(transformErrorResponse(res => ({ phone: res.phone })))
  },

  authorize (code, primaryUser) {
    const timezone = moment().format('Z')
    const host = document.location.hostname

    // Факт того, что нужно отправлять какие-то согласия находится здесь, тк
    // флага primaryUser достаточно внутри приложения, чтобы отправить согласия в API слое
    const agreements = primaryUser
      ? {
        processDataAgreement: true,
        receiveInfoAgreement: true,
        creditHistoryRequestAgreement: true,
        usePersonalSignAgreement: true
      }
      : null

    return makeAPIRequest('post', '/v3/auth', {
      body: {
        code,
        timezone,
        host,
        ...(R.isNil(agreements) ? {} : agreements)
      }
    }).catch(
      transformErrorResponse(res => {
        return { confirmationCode: res.code }
      })
    )
  },

  logout () {
    return makeAPIRequest('post', '/v3/logOut').then(({ body }) => body)
  },

  createLoanRequest (commonLeadProviderParams) {
    return makeAPIRequest('post', '/v3/loanRequests', {
      body: R.filter(hasValue, commonLeadProviderParams || {})
    }).then(({ body }) => ({
      id: body.id
    }))
  },

  fetchActiveLoanRequest () {
    return makeAPIRequest('get', `/v3/loanRequests`, {
      body: { type: 'active' }
    }).then(({ body }) => {
      const request = body.items[0]
      return body.items.length === 0
        ? { activeLoanRequest: {} }
        : {
          activeLoanRequest: {
            id: request.id,
            status: request.status,
            step: request.stage,
            countLoanRequestItem: request.countLoanRequestItem,
            createdDate: request.createdDate,
            expirationDate: request.expirationDate,
            requestedTerm: request.requestedTerm,
            requestedAmount: request.requestedAmount,
            isLongProcess: request.isLongProcess
          }
        }
    })
  },

  fetchArchiveLoanRequest (limit = 10, offset = 0) {
    return makeAPIRequest('get', '/v3/loanRequests', {
      body: {
        type: 'archive'
      }
    }).then(({ body }) => {
      return body.items.length === 0
        ? { archiveLoanRequest: null }
        : {
          archiveLoanRequest: body.items
        }
    })
  },

  fetchLoanRequest (loanRequestId) {
    return makeAPIRequest('get', `/v3/loanRequests/${loanRequestId}`).then(
      ({ body }) => {
        return {
          id: body.id,
          status: body.status,
          step: body.stage,
          creationDate: body.createdDate,
          expirationDate: body.expirationDate,
          term: body.requestedTerm,
          amount: body.requestedAmount,
          isLongProcess: body.isLongProcess,
          countLoanRequestItem: body.countLoanRequestItem
        }
      }
    )
  },

  fetchLoanRequestSuggestion (loanRequestId) {
    return makeAPIRequest(
      'get',
      `/v3/loanRequests/${loanRequestId}/items`
    ).then(({ body }) => body)
  },

  fetchProfile (loanRequestId) {
    return makeAPIRequest(
      'get',
      `/v3/loanRequests/${loanRequestId}/application`
    ).then(({ body }) => convertApplicationDTO(body))
  },

  sendApplication (loanRequestId, profile) {
    return makeAPIRequest(
      'post',
      `/v3/loanRequests/${loanRequestId}/application`,
      {
        body: convertToApplicationDTO(profile)
      }
    )
      .then(res => res)
      .catch(transformErrorResponse(convertApplicationValidationErrors))
  },

  saveLoanConditions (loanRequestId, amount, term) {
    // Отправляем 2 запроса на loanRequests/${id}/applicationField;
    // сразу отправить данные в запросе на создание заявки - это слишком просто
    return makeAPIRequest(
      'post',
      `/v3/loanRequests/${loanRequestId}/applicationField`,
      {
        body: { amount, term }
      }
    ).then(res => res)
  },

  saveProfileField (loanRequestId, field, value) {
    return makeAPIRequest(
      'post',
      `/v3/loanRequests/${loanRequestId}/applicationField`,
      {
        body: convertToApplicationDTO({ [field]: value })
      }
    ).then(res => res)
  },

  fetchAlternativeOffers (loanRequestId) {
    return makeAPIRequest(
      'get',
      `/v3/loanRequests/${loanRequestId}/offersForRejectScreen`
    ).then(({ body }) => body.offers)
  }
}
