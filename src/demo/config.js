/* eslint-disable global-require */
import moment from 'moment'

moment.locale('ru')

export const config = {
  base: [
    require('./base/address.demo').AddressDemo,
    require('./base/alert.demo').AlertDemo,
    require('./base/autocomplete.demo').AutocompleteDemo,
    require('./base/button.demo').ButtonDemo,
    require('./base/calc_placeholder.demo').CalcPlaceholderDemo,
    require('./base/card.demo').CardDemo,
    require('./base/checkbox.demo').CheckboxDemo,
    require('./base/dropdown.demo').DropDownDemo,
    require('./base/expansion_panel.demo').ExpansionPanelDemo,
    require('./base/image_uploader.demo').ImageUploaderDemo,
    require('./base/footer.demo').FooterDemo,
    require('./base/form.demo').FormDemo,
    require('./base/grid.demo').GridDemo,
    require('./base/icon.demo').IconDemo,
    require('./base/icon_button.demo').IconButtonDemo,
    require('./base/link.demo').LinkDemo,
    require('./base/menu.demo').MenuDemo,
    require('./base/modal.demo').ModalDemo,
    require('./base/navbar.demo').NavbarDemo,
    require('./base/panel.demo').PanelDemo,
    require('./base/radio_button_group.demo').RadioButtonGroupDemo,
    require('./base/select.demo').SelectDemo,
    require('./base/slider.demo').SliderDemo,
    require('./base/text_field.demo').TextFieldDemo,
    require('./base/typography.demo').TypographyDemo
  ],

  blocks: [
    require('./blocks/not_active_card.demo').NotActiveCardDemo,
    require('./blocks/approved_card.demo').ApprovedCardDemo,
    require('./blocks/approved_loan_card.demo').ApprovedLoanCardDemo,
    require('./blocks/new_approved_loan_card.demo').NewApprovedLoanCardDemo,
    require('./blocks/archive_expired_loan_card.demo').ArchiveExpiredLoanCardDemo,
    require('./blocks/archive_rejected_loan_card.demo').ArchiveRejectedLoanCardDemo,
    require('./blocks/archive_repaid_loan_card.demo').ArchiveRepaidLoanCardDemo,
    require('./blocks/incomplete_loan_card.demo').IncompleteLoanCardDemo,
    require('./blocks/list_loader.demo').ListLoaderDemo,
    require('./blocks/loan_calculator.demo').LoanCalculatorDemo,
    require('./blocks/money_transfer_loan_card.demo').MoneyTransferLoanCardDemo,
    require('./blocks/new_loan_blocking_card.demo').NewLoanBlockingCardDemo,
    require('./blocks/new_loan_card.demo').NewLoanCardDemo,
    require('./blocks/overdue_loan_card.demo').OverdueLoanCardDemo,
    require('./blocks/processed_loan_card.demo').ProcessedLoanCardDemo,
    require('./blocks/alternative_rejected_loan_card.demo').AlternativeRejectedLoanCardDemo,
    require('./blocks/serviced_loan_card.demo').ServicedLoanCardDemo,
    require('./blocks/support.demo').SupportDemo
  ],

  pages: [
    require('./pages/loan_request.demo').LoanRequestDemo,
    require('./pages/loan_request_list.demo').LoanRequestListDemo,
    require('./pages/additional_application.demo').AdditionalApplicationDemo,
    require('./pages/application_processing.demo').ApplicationProcessingDemo,
    require('./pages/approved_loan.demo').ApprovedLoanDemo,
    require('./pages/base_application.demo').BaseApplicationDemo,
    require('./pages/common_application.demo').CommonApplicationDemo,
    require('./pages/expired_loan.demo').ExpiredLoanDemo,
    require('./pages/loan_params_application.demo').LoanParamsApplicationDemo,
    require('./pages/money_transfer_loan.demo').MoneyTransferLoanDemo,
    require('./pages/overdue_loan.demo').OverdueLoanDemo,
    require('./pages/alternative_rejected_loan.demo').AlternativeRejectedLoanDemo,
    require('./pages/repaid_loan.demo').RepaidLoanDemo,
    require('./pages/serviced_loan.demo').ServicedLoanDemo
  ]
}
