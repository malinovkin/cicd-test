import React from 'react'
import {Card} from '../../base'
import {Layout, Section} from '../helpers'

export function CardDemo () {
  return (
    <Layout>

      <Section title='Card' padding>
        <Card>
          Card content
        </Card>
      </Section>

      <Section title='Card accent' padding>
        <Card borderType='accent'>
          Card content
        </Card>
      </Section>

      <Section title='Card success' padding>
        <Card borderType='success'>
          Card content
        </Card>
      </Section>

      <Section title='Card danger' padding>
        <Card borderType='danger'>
          Card content
        </Card>
      </Section>

      <Section title='Card neutral' padding>
        <Card borderType='neutral'>
          Card content
        </Card>
      </Section>

    </Layout>
  )
}
