import React from 'react'
import {ContentWithFooter, ContentBeforeFooter, Footer} from '../../base'
import {Layout, Section} from '../helpers'

export function FooterDemo () {
  return (
    <Layout>

      <Section title='Default' padding>
        <div style={{height: 200}}>
          <ContentWithFooter>
            <ContentBeforeFooter>Content</ContentBeforeFooter>
            <Footer>Footer</Footer>
          </ContentWithFooter>
        </div>
      </Section>

      <Section title='Scrolling past the first screen' padding>
        <div style={{height: 200, overflowY: 'scroll'}}>
          <ContentWithFooter>
            <ContentBeforeFooter>
              <div className='card' style={{height: 300}}>Lots of content</div>
            </ContentBeforeFooter>
            <Footer>Footer</Footer>
          </ContentWithFooter>
        </div>
      </Section>

    </Layout>
  )
}
