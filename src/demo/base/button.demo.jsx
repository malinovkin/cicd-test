import React from 'react'
import {Button} from '../../base'
import {handleClick, Layout, Section} from '../helpers'

export function ButtonDemo () {
  return (
    <Layout>

      <Section title='Button primary' padding>
        <Button onClick={handleClick}>
          Button default
        </Button>
        <Button disabled onClick={handleClick}>
          Button disabled
        </Button>
        <Button isPending onClick={handleClick}>
          Some text
        </Button>
      </Section>

      <Section title='Button primary small' padding>
        <Button size='sm' onClick={handleClick}>
          Button default
        </Button>
        <Button size='sm' disabled onClick={handleClick}>
          Button disabled
        </Button>
        <Button size='sm' isPending onClick={handleClick}>
          Some text
        </Button>
      </Section>

      <Section title='Button secondary' padding>
        <Button color='secondary' onClick={handleClick}>
          Button default
        </Button>
        <Button color='secondary' disabled onClick={handleClick}>
          Button disabled
        </Button>
        <Button color='secondary' isPending onClick={handleClick}>
          Some text
        </Button>
      </Section>

      <Section title='Data attrs' padding>
        <Button onClick={handleClick} dataAttrs={{value: 0, attr: 'Cool'}}>
          Data attrs
        </Button>
      </Section>

      <Section title='Type submit' padding>
        <Button type='submit' onClick={handleClick}>Submit button</Button>
      </Section>

      <Section title='Max width' padding>
        <div>
          <Button maxWidth onClick={handleClick}>Long long button</Button>
        </div>
        <div>
          <Button isPending onClick={handleClick} maxWidth>Some text</Button>
        </div>
      </Section>

    </Layout>
  )
}
