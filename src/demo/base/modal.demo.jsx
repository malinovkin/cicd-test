import React from 'react'
import _ from 'lodash'
import {Modal} from '../../base'
import {Layout, Section} from '../helpers'

class ModalContainer extends React.Component {
  state = {
    open: false
  }

  handleOpen = (ev) => {
    this.setState({
      open: ev.currentTarget.checked
    })
  }

  handleClose = () => {
    this.setState({
      open: false
    })
  }

  render () {
    return (
      <div>
        <input
          type='checkbox'
          checked={this.state.open}
          onChange={this.handleOpen}
        />
        <div>Open</div>
        {React.cloneElement(this.props.children, {
          onClose: this.props.children.props.hasCloseButton
            ? this.handleClose : null,
          open: this.state.open
        })}
      </div>
    )
  }
}

export function ModalDemo () {
  return (
    <Layout>

      <Section title='Modal'>
        <ModalContainer>
          <Modal
            hasCloseButton
          >Modal</Modal>
        </ModalContainer>
      </Section>

      <Section title='Modal with a title'>
        <ModalContainer>
          <Modal
            hasCloseButton
            title='This is an important modal with a title'
          >Modal content</Modal>
        </ModalContainer>
      </Section>

      <Section title='Modal with close after overlay click'>
        <ModalContainer>
          <Modal
            hasCloseButton
            shouldCloseOnOverlayClick
          >Modal</Modal>
        </ModalContainer>
      </Section>

      <Section title='Modal with top position'>
        <ModalContainer>
          <Modal
            position='top'
            hasCloseButton
          >
            {_.range(100).map(el => <div key={el}>{el}</div>)}
          </Modal>
        </ModalContainer>
      </Section>

    </Layout>
  )
}
