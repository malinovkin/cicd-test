import React from 'react'
import {RawSlider} from '../../base/slider'
import {handleChange, Layout, Section} from '../helpers'

export function SliderDemo () {
  return (
    <Layout>

      <Section title='Slider' padding>
        <RawSlider onChange={handleChange} min={0} max={100} value={100} />
      </Section>

      <Section title='Slider with info' padding>
        <RawSlider
          onChange={handleChange}
          min={0}
          max={100}
          value={50}
          title='Slider title'
          getValueCaption={value => value}
          minValueCaption={`Min value: ${0}`}
          maxValueCaption={`Max value: ${100}`}
        />
      </Section>

      <Section title='Disabled' padding>
        <RawSlider
          onChange={handleChange}
          min={0}
          max={100}
          value={50}
          title='Slider title'
          getValueCaption={value => value}
          minValueCaption='Min value: 0'
          maxValueCaption='Max value: 100'
          disabled
        />
      </Section>

      <Section title='Fixed' padding>
        <RawSlider
          onChange={handleChange}
          min={0}
          max={100}
          value={50}
          title='Slider title'
          getValueCaption={value => value}
          minValueCaption='Min value: 0'
          maxValueCaption='Max value: 100'
          fixed
          label='Label'
        />
      </Section>

    </Layout>
  )
}
