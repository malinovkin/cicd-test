import React from 'react'
import * as Kit from '../../base'
import {handleSubmit, Layout, Section} from '../helpers'

const data = [
  {value: 0, caption: 'City A'},
  {value: 1, caption: 'City Aa'},
  {value: 2, caption: 'City B'},
  {value: 3, caption: 'City C'},
  {value: 4, caption: 'Cat'}
]

function validate (values) {
  if (values.password !== '123') {
    return {
      password: 'Password is not valid'
    }
  }
  return {}
}

function getCaption (item) {
  return item.caption
}

function getData () {
  return data
}

function getID (item) {
  return item.value
}

export function FormDemo () {
  return (
    <Layout>

      <Section title='Form' padding>
        <Kit.Form
          defaultValues={{
            salary: 5000,
            text: 'Boo!'
          }}
          onSubmit={handleSubmit}
        >
          <Kit.Autocomplete
            field='city'
            label='City'
            onGetSuggestions={getData}
            onGetSuggestionCaption={getCaption}
            onGetValueCaption={getCaption}
            onGetSuggestionId={getID}
          />

          <Kit.Checkbox
            field='agreement'
            caption='Agreement'
          />

          <Kit.RadioButtonGroup
            field='gender'
            items={[
              {value: 'male', caption: 'Male'},
              {value: 'female', caption: 'Female'}
            ]}
          />

          <Kit.Select
            field='otherCity'
            items={data}
            label='City'
            onGetOptionCaption={getCaption}
            onGetValueCaption={getCaption}
            onGetOptionId={getID}
          />

          <Kit.TextField
            field='text'
            label='Some text'
          />
          <Kit.TextField
            field='phone'
            label='Phone'
            mask='+7 (999) 999-99-99'
          />
          <Kit.Slider
            field='salary'
            min={0}
            max={10000}
            step={100}
            getValueCaption={value => value}
            minValueCaption='Min value: 0'
            maxValueCaption='Max value: 10000'
          />
          <Kit.Button type='submit'>Submit</Kit.Button>
        </Kit.Form>
      </Section>

      <Section title='Form in grid' padding>
        <Kit.Container>
          <Kit.Form
            onSubmit={handleSubmit}
            defaultValues={{
              a: 'Some very very \nlong text',
              b: data[0],
              c: data[4]
            }}
          >
            <Kit.Row>
              <Kit.Col size={8} offset={2}>
                <Kit.TextField
                  field='a'
                  label='Multiline'
                  multiline
                />
              </Kit.Col>
            </Kit.Row>
            <Kit.Row>
              <Kit.Col size={8} offset={2}>
                <Kit.Autocomplete
                  field='b'
                  label='B'
                  onGetSuggestions={getData}
                  onGetSuggestionCaption={getCaption}
                  onGetValueCaption={getCaption}
                  onGetSuggestionId={getID}
                />
              </Kit.Col>
            </Kit.Row>
            <Kit.Row>
              <Kit.Col size={8} offset={2}>
                <Kit.Select
                  field='c'
                  label='C'
                  items={data}
                  onGetOptionCaption={getCaption}
                  onGetValueCaption={getCaption}
                  onGetOptionId={getID}
                />
              </Kit.Col>
            </Kit.Row>
            <Kit.Row>
              <Kit.Col size={8} offset={2}>
                <Kit.Button type='submit' maxWidth>
                  Submit
                </Kit.Button>
              </Kit.Col>
            </Kit.Row>
          </Kit.Form>
        </Kit.Container>
      </Section>

      <Section title='Validation' padding>
        <Kit.Form validate={validate}>
          {({isInvalid}) => (
            <div>
              <Kit.TextField
                field='password'
                label='Password'
                type='password'
              />
              <Kit.Button disabled={isInvalid}>Submit</Kit.Button>
            </div>
          )}
        </Kit.Form>
      </Section>

    </Layout>
  )
}
