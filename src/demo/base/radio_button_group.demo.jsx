/* eslint-disable react/prop-types */
import {assign} from 'lodash'
import React from 'react'
import {RawRadioButtonGroup} from '../../base/radio_button_group'
import {handleChange, Layout, Section} from '../helpers'

class RadioButtonGroupContainer extends React.Component {
  state = {
    value: this.props.value
  }

  handleCheck = (value) => {
    this.setState({value})
    handleChange(value)
  }

  render () {
    const props = assign({}, this.props.control.props, {
      onChange: this.handleCheck,
      value: this.state.value
    })

    return React.cloneElement(this.props.control, props)
  }
}

export function RadioButtonGroupDemo () {
  return (
    <Layout>

      <Section title='Radio button'>
        <RadioButtonGroupContainer
          control={
            <RawRadioButtonGroup
              name='a'
              onChange={handleChange}
              items={[
                {value: 'red', caption: 'Red red red red'},
                {value: 'yellow', caption: 'Yellow'},
                {value: 'black', caption: 'Black'}
              ]}
            />
          }
        />
      </Section>

      <Section title='Radio button (disabled)'>
        <RadioButtonGroupContainer
          value='black'
          control={
            <RawRadioButtonGroup
              name='b'
              onChange={handleChange}
              items={[
                {value: 'red', caption: 'Red red red red', disabled: true},
                {value: 'yellow', caption: 'Yellow'},
                {value: 'black', caption: 'Black'}
              ]}
            />
          }
        />
      </Section>

      <Section title='Radio button (all disabled)'>
        <RadioButtonGroupContainer
          value='yellow'
          control={
            <RawRadioButtonGroup
              name='c'
              onChange={handleChange}
              disabled
              items={[
                {value: 'red', caption: 'Red red red red', disabled: true},
                {value: 'yellow', caption: 'Yellow'},
                {value: 'black', caption: 'Black'}
              ]}
            />
          }
        />
      </Section>

      <Section title='Inline'>
        <RadioButtonGroupContainer
          control={
            <RawRadioButtonGroup
              name='d'
              onChange={handleChange}
              inline
              items={[
                {value: 'red', caption: 'Red red red red', disabled: true},
                {value: 'yellow', caption: 'Yellow'},
                {value: 'black', caption: 'Black'}
              ]}
            />
          }
        />
      </Section>

      <Section title='Fixed'>
        <RadioButtonGroupContainer
          control={
            <RawRadioButtonGroup
              name='f'
              onChange={handleChange}
              label='Color'
              items={[
                {value: 'red', caption: 'Red red red red', disabled: true},
                {value: 'yellow', caption: 'Yellow'},
                {value: 'black', caption: 'Black'}
              ]}
              fixed
            />
          }
        />
      </Section>

    </Layout>
  )
}
