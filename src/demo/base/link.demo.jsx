import React from 'react'
import {Link} from '../../base'
import {handleClick, Layout, Section} from '../helpers'

export function LinkDemo () {
  return (
    <Layout>

      <Section title='Link' padding>
        <Link onClick={handleClick}>Link</Link>
      </Section>

      <Section title='Link with href' padding>
        <Link href='#some-place'>Link</Link>
      </Section>

      <Section title='Link disabled' padding>
        <Link disabled>Disabled link</Link>
      </Section>

    </Layout>
  )
}
