import React from 'react'
import {Alert} from '../../base'
import {handleClick, Layout, Section} from '../helpers'

class Container extends React.Component {
  state = {
    open: false
  }

  handleSwitch = (ev) => {
    this.setState({
      open: ev.currentTarget.checked
    })
  }

  render () {
    return (
      <div>
        <input
          type='checkbox'
          checked={this.state.open}
          onChange={this.handleSwitch}
        />
        <div>Open</div>
        {React.cloneElement(this.props.children, {open: this.state.open})}
      </div>
    )
  }
}

export function AlertDemo () {
  return (
    <Layout>

      <Section title='Success Alert' padding>
        <Alert open>
          Well done! You successfully read this important alert message.
        </Alert>
      </Section>

      <Section title='Warning Alert' padding>
        <Alert open type='warning'>
          Well done! You successfully read this important alert message.
        </Alert>
      </Section>

      <Section title='Danger Alert' padding>
        <Alert open type='danger'>
          Well done! You successfully read this important alert message.
        </Alert>
      </Section>

      <Section title='Animatio' padding>
        <Container>
          <Alert>
            Well done! You successfully read this important alert message.
          </Alert>
        </Container>
      </Section>

      <Section title='Fixed' padding>
        <Alert open type='danger' fixed>
          Well done! You successfully read this important alert message.
        </Alert>
      </Section>

      <Section title='Fixed and closable' padding>
        <Alert open type='danger' fixed closable onClose={handleClick}>
          Well done! You successfully read this important alert message.
        </Alert>
      </Section>

    </Layout>
  )
}
