import React from 'react'
import {RawCheckbox} from '../../base/checkbox'
import {handleChange, Layout, Section} from '../helpers'

export function CheckboxDemo () {
  return (
    <Layout>

      <Section title='Checkbox' padding>
        <RawCheckbox
          checked
          name='a'
          caption='Checkbox'
          onChange={handleChange}
        />
      </Section>

      <Section title='Checkbox disabled' padding>
        <RawCheckbox
          disabled
          name='b'
          caption='Checkbox'
          onChange={handleChange}
        />
      </Section>

      <Section title='Checkbox disabled (checked)' padding>
        <RawCheckbox
          disabled
          checked
          name='c'
          caption='Checkbox'
          onChange={handleChange}
        />
      </Section>

      <Section title='Checkbox with data attrs' padding>
        <RawCheckbox
          name='d'
          caption='Checkbox'
          onChange={handleChange}
          dataAttrs={{value: 0}}
        />
      </Section>

      <Section title='Fixed' padding>
        <RawCheckbox
          name='d'
          caption='Checkbox'
          onChange={handleChange}
          checked
          fixed
        />
      </Section>

    </Layout>
  )
}
