import React from 'react'
import * as R from 'ramda'
import {Dropdown} from '../../base'
import {handleChange, Layout, Section} from '../helpers'

const items = [
  {value: 0, city: 'Москва'},
  {value: 1, city: 'Екатеринбург'},
  {value: 2, city: 'Ростов', street: 'Большая Садовая'}
]

function getCaption (item) {
  return (
    <span>
      {item.city} <strong> {item.street} </strong>
    </span>
  )
}

function getId (item) {
  return item.value
}

export function DropDownDemo () {
  return (
    <Layout>

      <Section title='Dropdown' padding>
        <Dropdown
          open
          items={items}
          onSelect={handleChange}
          onGetItemCaption={getCaption}
          onGetItemId={getId}
        />
      </Section>

      <Section title='Dropdown empty' padding>
        <Dropdown
          open
          items={[]}
          onSelect={handleChange}
          onGetItemCaption={getCaption}
          onGetItemId={getId}
        />
      </Section>

      <Section title='Dropdown item with data' padding>
        <Dropdown
          open
          items={R.map(el => R.assoc('data', 'some additional string', el), items)}
          onSelect={handleChange}
          onGetItemCaption={getCaption}
          onGetItemId={getId}
        />
      </Section>

    </Layout>
  )
}
