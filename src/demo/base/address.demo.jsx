import React from 'react'
import {Layout, printMessage, Section} from '../helpers'
import {Container} from '../../base'
import {RawAddress} from '../../base/address'
import * as AddressApi from '../../dadata'

class AddressWrapper extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      address: null
    }
  }

  handleChange = (value) => {
    printMessage('On change')(value)
    this.setState({
      address: value
    })
  }

  render () {
    return (
      <Container>
        <RawAddress
          value={this.state.address}
          searchRegion={AddressApi.searchRegions}
          searchCity={AddressApi.searchCities}
          searchStreet={AddressApi.searchStreets}
          searchHouse={AddressApi.searchHouses}
          onChange={this.handleChange}
          title='Address'
        />
      </Container>
    )
  }
}

export function AddressDemo () {
  return (
    <Layout>

      <Section title='Default'>
        <AddressWrapper />
      </Section>

      <Section title='Validation errors'>
        <RawAddress
          searchRegion={AddressApi.searchRegions}
          searchCity={AddressApi.searchCities}
          searchStreet={AddressApi.searchStreets}
          searchHouse={AddressApi.searchHouses}
          onChange={printMessage('On change')}
          title='Address'
          error={{
            region: 'Error',
            city: 'Error',
            street: 'Error',
            house: 'Error',
            flat: 'Error'
          }}
        />
      </Section>

    </Layout>
  )
}
