import React from 'react'
import {Icon} from '../../base'
import {Layout, Section} from '../helpers'

export function IconDemo () {
  return (
    <Layout>
      <Section title='Some icons'>
        <Icon name='time-icon' />
        <Icon name='danger-icon' />
        <Icon name='arrow-left-icon-lg' />
      </Section>
    </Layout>
  )
}
