/* eslint-disable react/prop-types */
import React from 'react'
import {Text, Title} from '../../base'
import {Layout, Section} from '../helpers'

const styles = {
  backgroundColor: 'black',
  display: 'inline-block',
  padding: '10px'
}

function InverseContainer (props) {
  return <div style={styles}>{props.children}</div>
}

export function TypographyDemo () {
  return (
    <Layout>

      <Section title='Title 1'>
        <Title size={1}>Title 1. Очень длинный заголовок в две строки</Title>
      </Section>

      <Section title='Title 2'>
        <Title size={2}>Title 2. Очень длинный заголовок второго уровня в две строки</Title>
      </Section>

      <Section title='Title 3'>
        <Title size={3}>Title 3. Очень длинный заголовок третьего уровня в две строки</Title>
      </Section>

      <Section title='Title 4'>
        <Title size={4}>Title 4. Стиль для второстепенных заголовков</Title>
      </Section>

      <Section title='Title 5'>
        <Title size={5}>TITLE 5. СТИЛЬ ДЛЯ ВТОРОСТЕПЕННЫХ ЗАГОЛОВКОВ ЗАГЛАВНЫМИ БУКВАМИ</Title>
      </Section>

      <Section title='Body 1'>
        <Text>Body 1</Text>
      </Section>

      <Section title='Body 2'>
        <Text type='body-2'>Body 2</Text>
      </Section>

      <Section title='Subtitle'>
        <Text type='subtitle'>Subtitle</Text>
      </Section>

      <Section title='Color secondary'>
        <Text color='secondary'>Text secondary</Text>
      </Section>

      <Section title='Color primary inverse'>
        <InverseContainer>
          <Text color='inverse'>Inverse</Text>
        </InverseContainer>
      </Section>

      <Section title='Color secondary inverse'>
        <InverseContainer>
          <Text color='secondary-inverse'>Inverse</Text>
        </InverseContainer>
      </Section>

      <Section title='Color danger'>
        <Text color='danger'>Danger</Text>
      </Section>

      <Section title='Inline'>
        <div>
          <Text inline>Inline</Text>
          <Text inline>Inline</Text>
        </div>
      </Section>

      <Section title='Inline block'>
        <Text inline='block'>Inline block</Text>
        <Text inline='block'>Inline block</Text>
      </Section>

      <Section title='Data attrs'>
        <Text dataAttrs={{value: '0'}}>Some content</Text>
      </Section>

    </Layout>
  )
}
