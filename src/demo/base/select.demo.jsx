import React from 'react'
import {RawSelect} from '../../base/select'
import {
  handleBlur,
  handleChange,
  handleFocus,
  Layout,
  Section
} from '../helpers'

const items = [
  {value: 0, caption: 'User Name 1'},
  {value: 1, caption: 'User Name 2'},
  {value: 2, caption: 'User Name 3'},
  {value: 3, caption: 'User Name 4'},
  {value: 4, caption: 'User Name 5'},
  {value: 5, caption: 'User Name 5 User Name 5 User Name 5 User Name 5'}
]

function getOptionCaption (item) {
  return item.caption
}

function getValueCaption (item) {
  return item.caption
}

function getOptionID (item) {
  return item.value
}

export function SelectDemo () {
  return (
    <Layout>

      <Section title='Select' padding>
        <RawSelect
          value={items[1]}
          label='Users'
          items={items}
          onChange={handleChange}
          onBlur={handleBlur}
          onFocus={handleFocus}
          onGetOptionCaption={getOptionCaption}
          onGetValueCaption={getValueCaption}
          onGetOptionId={getOptionID}
        />
      </Section>

      <Section title='Select custome option' padding>
        <RawSelect
          value={items[1]}
          label='Users'
          items={items}
          onChange={handleChange}
          onBlur={handleBlur}
          onFocus={handleFocus}
          onGetOptionCaption={item =>
            <span>{item.value} - {item.caption}</span>
          }
          onGetValueCaption={getValueCaption}
          onGetOptionId={getOptionID}
        />
      </Section>

      <Section title='Select array option' padding>
        <RawSelect
          value={'1'}
          label='Users'
          items={['1', '2', '3']}
          onChange={handleChange}
          onBlur={handleBlur}
          onFocus={handleFocus}
          onGetOptionCaption={item => item}
          onGetValueCaption={item => item}
          onGetOptionId={getOptionID}
        />
      </Section>

      <Section title='Select empty' padding>
        <RawSelect
          label='Users'
          items={items}
          onChange={handleChange}
          onGetOptionCaption={getOptionCaption}
          onGetValueCaption={getValueCaption}
          onGetOptionId={getOptionID}
        />
      </Section>

      <Section title='Disabled' padding>
        <RawSelect
          label='Users'
          disabled
          value={items[0]}
          items={items}
          onChange={handleChange}
          onGetOptionCaption={getOptionCaption}
          onGetValueCaption={getValueCaption}
          onGetOptionId={getOptionID}
        />
      </Section>

      <Section title='With error' padding>
        <RawSelect
          label='Users'
          items={items}
          onChange={handleChange}
          error='Validation error'
          onGetOptionCaption={getOptionCaption}
          onGetValueCaption={getValueCaption}
          onGetOptionId={getOptionID}
        />
      </Section>

      <Section title='With error (without message)' padding>
        <RawSelect
          label='Users'
          items={items}
          onChange={handleChange}
          error
          onGetOptionCaption={getOptionCaption}
          onGetValueCaption={getValueCaption}
          onGetOptionId={getOptionID}
        />
      </Section>

      <Section title='Optional' padding>
        <RawSelect
          label='Users'
          items={items}
          onChange={handleChange}
          optional
          onGetOptionCaption={getOptionCaption}
          onGetValueCaption={getValueCaption}
          onGetOptionId={getOptionID}
        />
      </Section>

      <Section title='Fixed' padding>
        <RawSelect
          label='Users'
          items={items}
          value={items[0]}
          onChange={handleChange}
          onGetOptionCaption={getOptionCaption}
          onGetValueCaption={getValueCaption}
          onGetOptionId={getOptionID}
          fixed
        />
      </Section>

    </Layout>
  )
}
