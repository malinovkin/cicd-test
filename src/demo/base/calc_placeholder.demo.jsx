import React from 'react'
import {CalcPlaceholder} from '../../base'
import {Layout, Section} from '../helpers'

export function CalcPlaceholderDemo () {
  return (
    <Layout>

      <Section title='Default' padding>
        <CalcPlaceholder />
      </Section>

    </Layout>
  )
}
