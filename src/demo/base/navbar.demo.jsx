import React from 'react'
import {Navbar} from '../../base'
import {Layout, Section} from '../helpers'

export function NavbarDemo () {
  return (
    <Layout>

      <Section title='Default' padding>
        <Navbar>
          Navbar
        </Navbar>
      </Section>

      <Section title='Transparent bg' style={{backgroundColor: 'yellow'}} padding>
        <Navbar transparent>
          Transparent bg
        </Navbar>
      </Section>

    </Layout>
  )
}
