import React from 'react'
import {Panel} from '../../base'
import {Layout, Section} from '../helpers'

export function PanelDemo () {
  return (
    <Layout>

      <Section title='Success'>
        <Panel color='success'>
          Success panel
        </Panel>
      </Section>

      <Section title='Danger'>
        <Panel color='danger'>
          Danger panel
        </Panel>
      </Section>

    </Layout>
  )
}
