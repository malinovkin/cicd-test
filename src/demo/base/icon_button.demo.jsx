import React from 'react'
import {IconButton} from '../../base'
import {handleClick, Layout, Section} from '../helpers'

export function IconButtonDemo () {
  return (
    <Layout>

      <Section title='Icon button'>
        <IconButton name='arrow-left' onClick={handleClick} />
      </Section>

      <Section title='Icon button lg'>
        <IconButton name='arrow-left' size='lg' onClick={handleClick} />
      </Section>

      <Section title='Icon button disabled'>
        <IconButton disabled name='arrow-left' onClick={handleClick} />
      </Section>

      <Section title='All icons'>
        <div>
          <IconButton
            size='lg'
            name='arrow-right'
            onClick={handleClick}
          />
          <IconButton
            size='lg'
            name='arrow-left'
            onClick={handleClick}
          />
          <IconButton
            size='lg'
            name='close'
            onClick={handleClick}
          />
        </div>
        <div>
          <IconButton name='arrow-right' onClick={handleClick} />
          <IconButton name='arrow-left' onClick={handleClick} />
          <IconButton name='close' onClick={handleClick} />
        </div>
        <div>
          <IconButton
            size='sm'
            name='arrow-right'
            onClick={handleClick}
          />
          <IconButton
            size='sm'
            name='arrow-left'
            onClick={handleClick}
          />
          <IconButton
            size='sm'
            name='close'
            onClick={handleClick}
          />
        </div>
      </Section>

      <Section title='Icon button with data attrs'>
        <IconButton
          name='arrow-left'
          onClick={handleClick}
          dataAttrs={{value: 0}}
        />
      </Section>

    </Layout>
  )
}
