import React from 'react'
import {printMessage, Layout, Section} from '../helpers'
import {RawImageUploader} from '../../base/image_uploader'

export function ImageUploaderDemo () {
  return (
    <Layout>

      <Section title='Default' padding>
        <RawImageUploader
          accept='.jpeg,.jpg,.png'
          label='Passport photo'
          onChange={printMessage('Handle change:')}
          uploadUrl='/api/file/images'
          previewUrl={id => `/api/file/images/${id}?size=preview`}
          metaUrl={id => `/api/file/images/${id}/meta`}
          parseSuccessResponse={res => res.data.id}
          parseFailureResponse={err => err}
          parseMetaResponse={res => ({
            name: res.data.filename,
            size: res.data.fileSize,
            width: res.data.width,
            height: res.data.height
          })}
          sizeLimit={14000}
        />
      </Section>

    </Layout>
  )
}
