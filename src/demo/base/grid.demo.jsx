import React from 'react'
import {Col, Container, Row} from '../../base'
import {createStyleTag, Layout, Section} from '../helpers'

const tag = createStyleTag(`
    .col,
    [class*="col-"] {
        background-color: blanchedalmond;
        border: 2px solid rgb(255, 194, 103);
    }
`)

export function GridDemo () {
  return (
    <Layout didMount={tag.append} willUnmount={tag.remove}>

      <Section title='Full width'>
        <Container>
          <Row>
            <Col>1 of 2</Col>
            <Col>1 of 2</Col>
          </Row>
          <Row>
            <Col>1 of 3</Col>
            <Col>1 of 3</Col>
            <Col>1 of 3</Col>
          </Row>
        </Container>
      </Section>

      <Section title='Setting width'>
        <Container>
          <Row>
            <Col>1 of 3</Col>
            <Col size={6}>1 of 3 (wider)</Col>
            <Col>1 of 3</Col>
          </Row>
          <Row>
            <Col>1 of 3</Col>
            <Col size={8}>1 of 3 (wider)</Col>
            <Col>1 of 3</Col>
          </Row>
        </Container>
      </Section>

      <Section title='Variable width content'>
        <Container>
          <Row>
            <Col size={2}>1 of 3</Col>
            <Col size='auto' className='u-p-0'>Variable width content</Col>
            <Col size={2}>1 of 3</Col>
          </Row>
        </Container>
      </Section>

      <Section title='Breakpoints'>
        <Container>
          <Row>
            <Col size={8}>col-8</Col>
            <Col size={4}>col-4</Col>
          </Row>
          <Row>
            <Col size={4}>col-4</Col>
            <Col size={2}>col-2</Col>
            <Col size={6}>col-6</Col>
          </Row>
        </Container>
      </Section>

      <Section title='Stacked to horizonral'>
        <Container>
          <Row>
            <Col sm={8}>col-sm-8</Col>
            <Col sm={4}>col-sm-4</Col>
          </Row>
          <Row>
            <Col sm>col-sm</Col>
            <Col sm>col-sm</Col>
            <Col sm>col-sm</Col>
            <Col sm>col-sm</Col>
          </Row>
        </Container>
      </Section>

      <Section title='No gutters'>
        <Container>
          <Row noGutters>
            <Col>Col 1</Col>
            <Col>Col 2</Col>
            <Col>Col 3</Col>
            <Col>Col 4</Col>
            <Col>Col 5</Col>
          </Row>
        </Container>
      </Section>

      <Section title='Horizontal alignment'>
        <Container>
          <Row hStart>
            <Col size={4}>One of two columns</Col>
            <Col size={4}>One of two columns</Col>
          </Row>
          <Row hCenter>
            <Col size={4}>One of two columns</Col>
            <Col size={4}>One of two columns</Col>
          </Row>
          <Row hEnd>
            <Col size={4}>One of two columns</Col>
            <Col size={4}>One of two columns</Col>
          </Row>
          <Row hAround>
            <Col size={4}>One of two columns</Col>
            <Col size={4}>One of two columns</Col>
          </Row>
          <Row hBetween>
            <Col size={4}>One of two columns</Col>
            <Col size={4}>One of two columns</Col>
          </Row>
        </Container>
      </Section>

      <Section title='Offsetting columns'>
        <Container>
          <Row>
            <Col size={4}>col-4</Col>
            <Col size={4} offset={4}>col-4 offset-4</Col>
          </Row>
          <Row>
            <Col size={3} offset={3}>col-3 offset-3</Col>
            <Col size={3} offset={3}>col-3 offset-3</Col>
          </Row>
        </Container>
      </Section>

      <Section title='Nesting'>
        <Container>
          <Row>
            <Col size={9}>
              Level 1: col-9
              <Row>
                <Col size={8}>Level 2: col-8</Col>
                <Col size={4}>Level 2: col-4</Col>
              </Row>
            </Col>
          </Row>
        </Container>
      </Section>

      <Section title='Flex order'>
        <Container>
          <Row>
            <Col unordered>First, but unordered</Col>
            <Col last>Second, but last</Col>
            <Col first>Third, but first</Col>
          </Row>
        </Container>
      </Section>

    </Layout>
  )
}
