import React from 'react'
import {RawTextField} from '../../base/text_field'
import {
  handleBlur,
  handleChange,
  handleFocus,
  Layout,
  Section
} from '../helpers'

export function TextFieldDemo () {
  return (
    <Layout>

      <Section title='Text text' padding>
        <RawTextField
          label='Name'
          onChange={handleChange}
          onFocus={handleFocus}
          onBlur={handleBlur}
        />
      </Section>

      <Section title='With error' padding>
        <RawTextField
          onChange={handleChange}
          label='Text field'
          error='Error message'
        />
      </Section>

      <Section title='Input type' padding>
        <RawTextField
          onChange={handleChange}
          label='Password'
          type='password'
        />
      </Section>

      <Section title='Optional' padding>
        <RawTextField
          onChange={handleChange}
          label='Optional'
          optional
        />
      </Section>

      <Section title='Placeholder' padding>
        <RawTextField
          onChange={handleChange}
          label='Text field'
          placeholder='Placeholder'
        />
      </Section>

      <Section title='Disabled' padding>
        <RawTextField
          onChange={handleChange}
          label='Text field'
          disabled
        />
      </Section>

      <Section title='Disabled (with value)' padding>
        <RawTextField
          value='Some value'
          onChange={handleChange}
          label='Text field'
          disabled
        />
      </Section>

      <Section title='Data attrs' padding>
        <RawTextField
          onChange={handleChange}
          label='Text field'
          dataAttrs={{
            value: 0
          }}
        />
      </Section>

      <Section title='Mask (date)' padding>
        <RawTextField
          onChange={handleChange}
          label='Date'
          mask='99.99.9999'
        />
      </Section>

      <Section title='Mask (phone)' padding>
        <RawTextField
          onChange={handleChange}
          label='Phone'
          mask='+7 (999) 999-99-99'
        />
      </Section>

      <Section title='Mask (filled phone)' padding>
        <RawTextField
          onChange={handleChange}
          label='Phone'
          mask='+7 (999) 999-99-99'
          value='9220000000'
        />
      </Section>

      <Section title='Dropdown slot' padding>
        <RawTextField
          onChange={handleChange}
          label='Text field'
          dropdownSlot={<div>Dropdown</div>}
          error='Error'
        />
      </Section>

      <Section title='Textarea' padding>
        <RawTextField
          onChange={handleChange}
          label='Message'
          multiline
        />
      </Section>

      <Section title='Textarea (with initial value)' padding>
        <RawTextField
          onChange={handleChange}
          label='Message'
          multiline
          value={'Text text\nText text'}
        />
      </Section>

      <Section title='Fixed' padding>
        <RawTextField
          onChange={handleChange}
          label='Message'
          value={'Text text\nText text'}
          fixed
        />
      </Section>

    </Layout>
  )
}
