import React from 'react'
import {RawAutocomplete} from '../../base/autocomplete'
import {
  handleBlur,
  handleChange,
  handleFocus,
  Layout,
  Section
} from '../helpers'

const data = [
  {value: 0, caption: 'City A'},
  {value: 1, caption: 'City Aa'},
  {value: 2, caption: 'City B'},
  {value: 3, caption: 'City C'},
  {value: 4, caption: 'City D'},
  {value: 5, caption: 'City E'},
  {value: 6, caption: 'City F'},
  {value: 7, caption: 'City G'},
  {
    value: 8,
    caption: 'Давно выяснено, что при оценке ' +
    'дизайна и композиции читаемый текст мешает сосредоточиться. ' +
    'Lorem Ipsum используют потому, что тот обеспечивает более' +
    'или менее стандартное заполнение шаблона, а также реальное ' +
    'распределение букв и пробелов в абзацах, которое не получается ' +
    'при простой дубликации "Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст.."'
  }
]

function getSyncData (term) {
  return data.filter(el => el.caption.toLowerCase().indexOf(term.toLowerCase()) >= 0)
}

function getAsyncData (term) {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(data.filter(el => el.caption.toLowerCase().indexOf(term.toLowerCase()) >= 0))
    }, 2000)
  })
}

function getValueCaption (item) {
  return item.caption
}

function getSuggestionCaption (item) {
  return (
    <span>
      {item.value}: <strong>{item.caption}</strong>
    </span>
  )
}

function getSuggestionID (item) {
  return item.value
}

export function AutocompleteDemo () {
  return (
    <Layout>

      <Section title='RawAutocomplete'>
        <RawAutocomplete
          label='Autocomplete'
          onGetSuggestions={() => data}
          onGetSuggestionCaption={getSuggestionCaption}
          onGetValueCaption={getValueCaption}
          onGetSuggestionId={getSuggestionID}
          onChange={handleChange}
          onFocus={handleFocus}
          onBlur={handleBlur}
        />
      </Section>

      <Section title='Autocomplete with value'>
        <RawAutocomplete
          label='Autocomplete'
          onGetSuggestions={getSyncData}
          onGetSuggestionCaption={getSuggestionCaption}
          onGetValueCaption={getValueCaption}
          onGetSuggestionId={getSuggestionID}
          onChange={handleChange}
          value={data[0]}
        />
      </Section>

      <Section title='Async data'>
        <RawAutocomplete
          label='Autocomplete'
          onGetSuggestions={getAsyncData}
          onGetSuggestionCaption={getSuggestionCaption}
          onGetValueCaption={getValueCaption}
          onGetSuggestionId={getSuggestionID}
          onChange={handleChange}
        />
      </Section>

      <Section title='Multiline'>
        <RawAutocomplete
          label='Autocomplete'
          onGetSuggestions={getSyncData}
          onGetSuggestionCaption={getSuggestionCaption}
          onGetValueCaption={getValueCaption}
          onGetSuggestionId={getSuggestionID}
          onChange={handleChange}
          multiline
        />
      </Section>

      <Section title='Fixed'>
        <RawAutocomplete
          label='Autocomplete'
          onGetSuggestions={getSyncData}
          onGetSuggestionCaption={getSuggestionCaption}
          onGetValueCaption={getValueCaption}
          onGetSuggestionId={getSuggestionID}
          onChange={handleChange}
          value={data[0]}
          fixed
        />
      </Section>

    </Layout>
  )
}
