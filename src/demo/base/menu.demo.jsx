import React from 'react'
import {Menu} from '../../base'
import {Layout, printMessage, Section} from '../helpers'

export function MenuDemo () {
  return (
    <Layout>

      <Section title='Default' padding>
        <Menu
          items={[
            {id: 0, caption: 'Ваши займы', iconName: 'ruble-icon'},
            {id: 2, caption: 'Выйти', iconName: 'exit-icon'}
          ]}
          caption='Профиль'
          captionIconName='person-icon'
          onSelect={printMessage('On select')}
        />
      </Section>

    </Layout>
  )
}
