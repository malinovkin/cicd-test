import React from 'react'
import {ExpansionPanel} from '../../base'
import {Layout, Section} from '../helpers'

export function ExpansionPanelDemo () {
  return (
    <Layout>

      <Section title='Default' padding>
        <ExpansionPanel caption='История займов'>
          Content
        </ExpansionPanel>
      </Section>

      <Section title='Closed by default' padding>
        <ExpansionPanel caption='История займов' defaultClosed>
          Content
        </ExpansionPanel>
      </Section>

    </Layout>
  )
}
