import React from 'react'
import {Col, Container, DataField, Row} from '../../base'
import {Layout, Section} from '../helpers'

export function DataFieldDemo () {
  return (
    <Layout>

      <Section title='Data fields'>
        <Container>
          <Row>
            <Col>
              <DataField label='Field A' value='Some value A' />
            </Col>
          </Row>
          <Row>
            <Col>
              <DataField label='Field A' value='Some value A' />
            </Col>
          </Row>
          <Row>
            <Col>
              <DataField label='Empty data field' />
            </Col>
          </Row>
        </Container>
      </Section>

    </Layout>
  )
}
