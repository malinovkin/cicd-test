/* eslint-disable no-console */
import P from 'prop-types'
import React from 'react'
import * as R from 'ramda'
import _ from 'lodash'
import {Link} from 'react-router-dom'

export class Layout extends React.Component {
  static propTypes = {
    children: P.node.isRequired,
    didMount: P.func,
    willUnmount: P.func
  }

  static defaultProps = {
    didMount: null,
    willUnmount: null
  }

  componentDidMount () {
    if (this.props.didMount) {
      this.props.didMount()
    }
  }

  componentWillUnmount () {
    if (this.props.willUnmount) {
      this.props.willUnmount()
    }
  }

  render () {
    return (
      <div>
        <Link to='/' className='example-back'>{'<'} Back</Link>
        <div>
          {this.props.children}
        </div>
      </div>
    )
  }
}

export function Section ({title, children, padding, style}) {
  return (
    <section className='example-section' style={style}>
      <div className='example-title'>{title}</div>
      <div className={padding ? 'example-body' : ''}>{children}</div>
    </section>
  )
}

Section.propTypes = {
  title: P.string.isRequired,
  children: P.node.isRequired,
  padding: P.bool,
  /* eslint-disable react/forbid-prop-types */
  style: P.object
  /* eslint-enable */
}

Section.defaultProps = {
  padding: false,
  style: null
}

export function createStyleTag (innerText) {
  const $style = document.createElement('style')
  $style.innerText = innerText

  const id = _.uniqueId('style-')
  $style.id = id

  return {
    append () {
      document.body.insertBefore($style, document.querySelector('#app'))
    },

    remove () {
      const style = document.querySelector(`#${id}`)
      if (style) {
        style.remove()
      }
    }
  }
}

export function handleClick () {
  console.log('onClick')
}

export function handleChange (data) {
  console.log('onChange: ', data)
}

export function handleFocus () {
  console.log('onFocus')
}

export function handleBlur () {
  console.log('onBlur')
}

export function handleSubmit (values) {
  console.log('Handle submit', values)
  return Promise.resolve(values)
}

export function handleSubmitFail (values) {
  console.log('Handle submit fail', values)
  return Promise.reject(R.mapObjIndexed(() => 'Server validation error', values))
}

export function printMessage (message) {
  return (...args) => {
    if (args.length) {
      console.log(message, ' Args: ', ...args)
    } else {
      console.log(message)
    }
  }
}
