import React from 'react'
import moment from 'moment'
import {Layout, printMessage, Section} from '../helpers'
import {ApprovedCard} from '../../modules/loan_request/approved_card'
import {Creditor} from '../../domain'

export function ApprovedCardDemo () {
  return (
    <Layout>

      <Section title='Default' padding>
        <ApprovedCard
          suggestion={{
            id: '0',
            amount: 12500,
            term: 23,
            rate: 2,
            creditor: Creditor.DoZarplaty,
            overpayment: 1000,
            deadline: moment().toISOString(),
            expirationDate: moment().add(58, 'hours').toISOString()
          }}
          onGetLoanDetails={printMessage('Go to LoanDetails page')}
          onGetMoney={printMessage('Open modal to get money')}
        />
      </Section>

    </Layout>
  )
}
