import React from 'react'
import {Layout, printMessage, Section} from '../helpers'
import {NewLoanCard} from '../../modules/loan_list/new_loan_card'

export function NewLoanCardDemo () {
  return (
    <Layout>

      <Section title='Default' padding>
        <NewLoanCard
          defaultValues={{}}
          minAmount={1000}
          maxAmount={50000}
          minTerm={1}
          maxTerm={40}
          amountStep={500}
          termStep={1}
          rate={3}
          isSubmitPending={false}
          onSubmit={printMessage('onSubmit')}
        />
      </Section>

    </Layout>
  )
}
