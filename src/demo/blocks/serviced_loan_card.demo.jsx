import React from 'react'
import moment from 'moment'
import {Layout, printMessage, Section} from '../helpers'
import {ServicedLoanCard} from '../../modules/loan_list/serviced_loan_card'
import {Creditor} from '../../domain'

export function ServicedLoanCardDemo () {
  return (
    <Layout>

      <Section title='Default' padding>
        <ServicedLoanCard
          loan={{
            id: '0',
            amount: 12500,
            term: 23,
            rate: 2,
            creditor: Creditor.SmsFinance,
            overpayment: 1000,
            decisionDate: moment().toISOString(),
            deadline: moment().toISOString(),
            closeDate: moment().toISOString()
          }}
          onGetLoanDetails={printMessage('On continue')}
        />
      </Section>

    </Layout>
  )
}
