import React from 'react'
import {Layout, Section} from '../helpers'
import {NewLoanBlockingCard} from '../../modules/loan_list/new_loan_blocking_card'

export function NewLoanBlockingCardDemo () {
  return (
    <Layout>

      <Section title='Default' padding>
        <NewLoanBlockingCard />
      </Section>

    </Layout>
  )
}
