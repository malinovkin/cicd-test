import React from 'react'
import {Layout, printMessage, Section} from '../helpers'
import {AlternativeRejectedLoanCard} from '../../modules/loan_list/alternative_rejected_loan_card'

export function AlternativeRejectedLoanCardDemo () {
  return (
    <Layout>

      <Section title='Default' padding>
        <AlternativeRejectedLoanCard
          loan={{id: 0}}
          onGetLoanDetails={printMessage('On continue')}
        />
      </Section>

    </Layout>
  )
}
