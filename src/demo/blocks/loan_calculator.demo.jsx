import React from 'react'
import {Layout, printMessage, Section} from '../helpers'
import {LoanCalculator} from '../../modules/shared/loan_calculator'

export function LoanCalculatorDemo () {
  return (
    <Layout>

      <Section title='Default values'>
        <LoanCalculator
          minTerm={6}
          maxTerm={30}
          minAmount={1000}
          maxAmount={30000}
          amountStep={500}
          termStep={1}
          rate={2}
          defaultValues={{}}
          onSubmit={printMessage('On submit')}
          isSubmitPending={false}
        />
      </Section>

      <Section title='Previous selected values'>
        <LoanCalculator
          minTerm={6}
          maxTerm={30}
          minAmount={1000}
          maxAmount={30000}
          amountStep={500}
          termStep={1}
          rate={2}
          defaultValues={{
            amount: 28000,
            term: 20
          }}
          onSubmit={printMessage('On submit')}
          isSubmitPending={false}
        />
      </Section>

    </Layout>
  )
}
