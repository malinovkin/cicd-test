import React from 'react'
import {Layout, Section} from '../helpers'
import {ListLoader} from '../../modules/loan_list/list_loader'

export function ListLoaderDemo () {
  return (
    <Layout>

      <Section title='Default' padding>
        <ListLoader />
      </Section>

    </Layout>
  )
}
