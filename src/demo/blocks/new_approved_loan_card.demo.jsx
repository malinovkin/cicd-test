import React from 'react'
import moment from 'moment'
import {Layout, printMessage, Section} from '../helpers'
import {NewApprovedLoanCard} from '../../modules/loan_request_list/new_approved_loan_card'
import {Creditor} from '../../domain'

export function NewApprovedLoanCardDemo () {
  return (
    <Layout>

      <Section title='Default' padding>
        <NewApprovedLoanCard
          suggestion={{
            id: '0',
            amount: 12500,
            term: 23,
            rate: 2,
            creditor: Creditor.PayPS,
            overpayment: 1000,
            deadline: moment().toISOString(),
            expirationDate: moment().add(58, 'hours').toISOString()
          }}
          onGetLoanDetails={printMessage('On continue')}
        />
      </Section>

    </Layout>
  )
}
