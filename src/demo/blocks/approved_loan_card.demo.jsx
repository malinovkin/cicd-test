import React from 'react'
import moment from 'moment'
import {Layout, printMessage, Section} from '../helpers'
import {ApprovedLoanCard} from '../../modules/loan_list/approved_loan_card'
import {Creditor} from '../../domain'

export function ApprovedLoanCardDemo () {
  return (
    <Layout>

      <Section title='Default' padding>
        <ApprovedLoanCard
          loan={{
            id: '0',
            amount: 12500,
            term: 23,
            rate: 2,
            creditor: Creditor.DoZarplaty,
            overpayment: 1000,
            deadline: moment().toISOString()
          }}
          onGetLoanDetails={printMessage('On continue')}
        />
      </Section>

    </Layout>
  )
}
