import React from 'react'
import moment from 'moment'
import {Layout, printMessage, Section} from '../helpers'
import {NotActiveCard} from '../../modules/loan_request/not_active_card'
import {Creditor} from '../../domain'

export function NotActiveCardDemo () {
  return (
    <Layout>

      <Section title='Срок одобрения истек' padding>
        <NotActiveCard
          suggestion={{
            id: '0',
            amount: 12500,
            term: 23,
            rate: 2,
            creditor: Creditor.DoZarplaty,
            overpayment: 1000,
            deadline: moment().toISOString(),
            rejectionReason: 'Срок одобрения истек'
          }}
          onGetLoanDetails={printMessage('Go to LoanDetails page')}
        />
      </Section>

      <Section title='Использовано' padding>
        <NotActiveCard
          suggestion={{
            id: '0',
            amount: 22500,
            term: 13,
            rate: 2,
            creditor: Creditor.HoneyMoney,
            overpayment: 2000,
            deadline: moment().toISOString(),
            rejectionReason: 'Использовано'
          }}
          onGetLoanDetails={printMessage('Go to LoanDetails page')}
        />
      </Section>

    </Layout>
  )
}
