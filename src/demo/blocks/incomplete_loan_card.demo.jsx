import React from 'react'
import moment from 'moment'
import {Layout, printMessage, Section} from '../helpers'
import {IncompleteLoanCard} from '../../modules/loan_list/incomplete_loan_card'
import {Creditor} from '../../domain'

export function IncompleteLoanCardDemo () {
  return (
    <Layout>

      <Section title='Default' padding>
        <IncompleteLoanCard
          loan={{
            id: '0',
            amount: 12500,
            term: 23,
            rate: 2,
            creditor: Creditor.SmsFinance,
            overpayment: 1000,
            decisionDate: moment().toISOString(),
            deadline: moment().toISOString(),
            closeDate: moment().toISOString()
          }}
          onGetLoanDetails={printMessage('On continue')}
        />
      </Section>

    </Layout>
  )
}
