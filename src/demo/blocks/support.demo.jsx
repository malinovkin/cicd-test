import React from 'react'
import {Layout, Section} from '../helpers'
import {Support} from '../../modules/shared/support'

export function SupportDemo () {
  return (
    <Layout>
      <Section title='Default'>
        <Support phone='1234567890' />
      </Section>
    </Layout>
  )
}
