import React from 'react'
import moment from 'moment'
import {Layout, printMessage, Section} from '../helpers'
import {ArchiveRejectedLoanCard} from '../../modules/loan_list/archive_rejected_loan_card'
import {Creditor} from '../../domain'

export function ArchiveRejectedLoanCardDemo () {
  return (
    <Layout>

      <Section title='Default' padding>
        <ArchiveRejectedLoanCard
          loan={{
            id: '0',
            amount: 12500,
            term: 23,
            rate: 2,
            creditor: Creditor.SmsFinance,
            overpayment: 1000,
            decisionDate: moment().toISOString(),
            deadline: moment().toISOString()
          }}
          onGetLoanDetails={printMessage('On continue')}
        />
      </Section>

    </Layout>
  )
}
