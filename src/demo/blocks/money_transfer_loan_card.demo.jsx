import React from 'react'
import moment from 'moment'
import {Layout, printMessage, Section} from '../helpers'
import {MoneyTransferLoanCard} from '../../modules/loan_list/money_transfer_loan_card'
import {Creditor} from '../../domain'

export function MoneyTransferLoanCardDemo () {
  return (
    <Layout>

      <Section title='Default' padding>
        <MoneyTransferLoanCard
          loan={{
            id: '0',
            amount: 12500,
            term: 23,
            rate: 2,
            creditor: Creditor.SmsFinance,
            overpayment: 1000,
            deadline: moment().toISOString()
          }}
          onGetLoanDetails={printMessage('On continue')}
        />
      </Section>

    </Layout>
  )
}
