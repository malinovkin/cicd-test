import React from 'react'
import {HashRouter, Link, Route, Switch} from 'react-router-dom'
import ReactDOM from 'react-dom'
import {config} from './config'

const DEFAULT_THEME = 'default'
const THEMES = ['default', 'tele2', 'beeline']

function getTheme () {
  const theme = window.sessionStorage.getItem('currentTheme')
  return theme || DEFAULT_THEME
}

function setTheme (theme) {
  const $prev = document.querySelector('#theme')
  let isChanged = false

  if ($prev && $prev.getAttribute('data-theme') === theme) {
    return
  } else if ($prev) {
    $prev.remove()
    isChanged = true
  }

  const $script = document.createElement('script')
  $script.id = 'theme'
  $script.setAttribute('data-theme', theme)
  $script.src = `${theme}.theme_styles.js`
  document.body.appendChild($script)

  window.sessionStorage.setItem('currentTheme', theme)
  if (isChanged) {
    window.location.reload()
  }
}

function ThemeSelector () {
  return (
    <div style={{position: 'absolute', top: 0, right: 0}}>
      Theme:
      <select
        defaultValue={getTheme()}
        onChange={ev => setTheme(ev.currentTarget.value)}
      >
        {THEMES.map(theme =>
          <option key={theme} value={theme}>{theme}</option>)}
      </select>
    </div>
  )
}

function transformDemoComponentName (name) {
  if (!/.*Demo$/.test(name)) {
    throw new Error(`Demo component name must end with "Demo". Got: ${name}`)
  }

  return name.replace('Demo', '')
}

function generateLinksBlock (title, components) {
  return (
    <div>
      <h2>{title}</h2>
      <ul>
        {components.map((c) => {
          const name = transformDemoComponentName(c.name)

          return (
            <li key={name}>
              <Link to={`/${name}`}>{name}</Link>
            </li>
          )
        })}
      </ul>
    </div>
  )
}

function generateRoutesBlock (components) {
  return components.map((c) => {
    const name = transformDemoComponentName(c.name)

    return <Route path={`/${name}`} render={() => React.createElement(c)} key={name} />
  })
}

function Links () {
  return (
    <div>
      {generateLinksBlock('Base', config.base)}
      {generateLinksBlock('Blocks', config.blocks)}
      {generateLinksBlock('Pages', config.pages)}
    </div>
  )
}

function Demo () {
  return (
    <div>
      <ThemeSelector />
      <h1>Prunto components</h1>
      <HashRouter>
        <Switch>
          <Route exact path='/' component={Links} />
          {generateRoutesBlock(config.base)}
          {generateRoutesBlock(config.blocks)}
          {generateRoutesBlock(config.pages)}
        </Switch>
      </HashRouter>
    </div>
  )
}

setTheme(getTheme())

// Render UI with timeout, because the styles are initialized after logic initialize.
// As a result, some logic (ex.: init textarea height) works incorrect.
setTimeout(() => {
  ReactDOM.render(<Demo />, document.querySelector('#app'))
}, 500)
