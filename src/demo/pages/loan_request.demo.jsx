import React from 'react'
import moment from 'moment'
import { Layout, printMessage, Section } from '../helpers'
import { LoanRequest } from '../../modules/loan_request/loan_request'
import { Creditor, LoanStatus } from '../../domain'

import { inactiveProps as archiveProps } from './loan_request_list.demo'

const approvedSuggestion = [
  {
    id: '0',
    amount: 12500,
    term: 23,
    rate: 2,
    creditor: Creditor.DoZarplaty,
    overpayment: 1000,
    deadline: moment().toISOString(),
    expirationDate: moment()
      .add(58, 'hours')
      .toISOString(),
    status: LoanStatus.Approved
  },
  {
    id: '1',
    amount: 5000,
    term: 21,
    rate: 1.8,
    creditor: Creditor.SmsFinance,
    overpayment: 1000,
    deadline: moment().toISOString(),
    expirationDate: moment()
      .add(28, 'hours')
      .toISOString(),
    status: LoanStatus.Approved
  },
  {
    id: '2',
    amount: 5000,
    term: 5,
    rate: 1.8,
    creditor: Creditor.PayPS,
    overpayment: 1000,
    deadline: moment().toISOString(),
    expirationDate: moment()
      .add(78, 'hours')
      .toISOString(),
    status: LoanStatus.Approved
  }
]

const notActiveSuggestion = [
  {
    id: '8',
    amount: 20000,
    term: 10,
    rate: 2,
    creditor: Creditor.PayPS,
    overpayment: 1000,
    deadline: moment().toISOString(),
    rejectionReason: 'Срок одобрения истек',
    status: LoanStatus.Expired
  },
  {
    id: '6',
    amount: 20000,
    term: 10,
    rate: 2,
    creditor: Creditor.PayPS,
    overpayment: 1000,
    deadline: moment().toISOString(),
    rejectionReason: 'Использовано',
    status: LoanStatus.repaid
  },
  {
    id: '9',
    amount: 20000,
    term: 10,
    rate: 2,
    creditor: Creditor.PayPS,
    overpayment: 1000,
    deadline: moment().toISOString(),
    rejectionReason: 'Срок одобрения истек',
    status: LoanStatus.Expired
  }
]

const defaultActiveProps = {
  onGetLoanDetails: printMessage('Go to LoanDetails page'),
  onGetMoney: printMessage('Open modal to get money'),
  suggestions: { approvedSuggestion, notActiveSuggestion },
  userRequest: {
    amount: 15000,
    term: 23,
    createDate: moment().toISOString()
  }
}

const defaultArchiveProps = {
  onGetLoanDetails: printMessage('Go to LoanDetails page'),
  onGetMoney: printMessage('Open modal to get money'),
  requestStatus: archiveProps.requestStatus,
  suggestions: archiveProps.history[0].suggestions,
  userRequest: archiveProps.history[0].userRequest

}

export function LoanRequestDemo () {
  return (
    <Layout>
      <Section title='Suggestion list  requestStatus="finished"'>
        <LoanRequest
          {...defaultActiveProps}
          requestStatus='finished'
        />
      </Section>

      <Section title='Suggestion list  requestStatus="processing"'>
        <LoanRequest
          {...defaultActiveProps}
          requestStatus='processing'
        />
      </Section>

      <Section title='Suggestion list  processingStatus="history"'>
        <LoanRequest
          {...defaultArchiveProps}
        />
      </Section>
    </Layout>
  )
}
