import React from 'react'
import moment from 'moment'
import {Layout, printMessage, Section} from '../helpers'
import {RepaidLoan} from '../../modules/loan/repaid_loan'
import {Creditor} from '../../domain'

export function RepaidLoanDemo () {
  return (
    <Layout>

      <Section title='Default'>
        <RepaidLoan
          onCreateLoan={printMessage('On get new loan')}
          onClose={printMessage('On close')}
          canCreateLoan
          loan={{
            id: '0',
            term: 5,
            amount: 30500,
            decisionDate: moment().subtract(10, 'days').toISOString(),
            rate: 1.2,
            creditor: Creditor.SmsFinance,
            closeDate: moment().toISOString(),
            moneyIssuedDate: moment().toISOString(),
            deadline: moment().toISOString(),
            overpayment: 1000
          }}
          supportPhone='1234567890'
        />
      </Section>

      <Section title='Can not create new loan'>
        <RepaidLoan
          onCreateLoan={printMessage('On get new loan')}
          onClose={printMessage('On close')}
          canCreateLoan={false}
          loan={{
            id: '0',
            term: 5,
            amount: 30500,
            decisionDate: moment().subtract(10, 'days').toISOString(),
            rate: 1.2,
            creditor: Creditor.SmsFinance,
            closeDate: moment().toISOString(),
            moneyIssuedDate: moment().toISOString(),
            deadline: moment().toISOString(),
            overpayment: 1000
          }}
          supportPhone='1234567890'
        />
      </Section>
    </Layout>)
}
