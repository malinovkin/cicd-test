import React from 'react'
import { Layout, Section } from '../helpers'
import { Creditor, LoanStatus } from '../../domain'
import LoanRequestList from '../../modules/loan_request_list/loan_request_list'
import moment from 'moment'

// ActiveLoanRequest suggestion
const suggestions = {
  approvedSuggestion: [
    {
      id: '0',
      amount: 12500,
      term: 23,
      rate: 2,
      creditor: Creditor.DoZarplaty,
      overpayment: 1000,
      deadline: moment().toISOString(),
      expirationDate: moment()
        .add(58, 'hours')
        .toISOString(),
      status: LoanStatus.Approved
    },
    {
      id: '1',
      amount: 5000,
      term: 21,
      rate: 1.8,
      creditor: Creditor.SmsFinance,
      overpayment: 1000,
      deadline: moment().toISOString(),
      expirationDate: moment()
        .add(28, 'hours')
        .toISOString(),
      status: LoanStatus.Approved
    },
    {
      id: '2',
      amount: 5000,
      term: 5,
      rate: 1.8,
      creditor: Creditor.PayPS,
      overpayment: 1000,
      deadline: moment().toISOString(),
      expirationDate: moment()
        .add(78, 'hours')
        .toISOString(),
      status: LoanStatus.Approved
    },
    {
      id: '3',
      amount: 22500,
      term: 16,
      rate: 2,
      creditor: Creditor.DoZarplaty,
      overpayment: 3000,
      deadline: moment().toISOString(),
      expirationDate: moment()
        .add(108, 'hours')
        .toISOString(),
      status: LoanStatus.Approved
    }
  ],
  notActiveSuggestion: [
    {
      id: '6',
      amount: 20000,
      term: 10,
      rate: 2,
      creditor: Creditor.HoneyMoney,
      overpayment: 1000,
      deadline: moment().toISOString(),
      status: LoanStatus.Expired
    },
    {
      id: '7',
      amount: 20000,
      term: 10,
      rate: 2,
      creditor: Creditor.EcoFinance,
      overpayment: 1000,
      deadline: moment().toISOString(),
      status: LoanStatus.repaid
    },
    {
      id: '8',
      amount: 20000,
      term: 10,
      rate: 2,
      creditor: Creditor.WebBankir,
      overpayment: 1000,
      deadline: moment().toISOString(),
      status: LoanStatus.Expired
    },
    {
      id: '9',
      amount: 20000,
      term: 10,
      rate: 2,
      creditor: Creditor.SmsFinance,
      overpayment: 1000,
      deadline: moment().toISOString(),
      status: LoanStatus.Expired
    },
    {
      id: '10',
      amount: 20000,
      term: 10,
      rate: 2,
      creditor: Creditor.SlonFinance,
      overpayment: 1000,
      deadline: moment().toISOString(),
      status: LoanStatus.repaid
    }
  ]
}

// ArchiveSuggestion
const suggestion1 = [
  {
    id: '6',
    amount: 14000,
    term: 15,
    rate: 2,
    creditor: Creditor.HoneyMoney,
    overpayment: 2000,
    deadline: moment().toISOString(),
    status: LoanStatus.Expired
  },
  {
    id: '7',
    amount: 25000,
    term: 18,
    rate: 2,
    creditor: Creditor.EcoFinance,
    overpayment: 4300,
    deadline: moment().toISOString(),
    status: LoanStatus.repaid
  },
  {
    id: '8',
    amount: 30000,
    term: 10,
    rate: 2,
    creditor: Creditor.WebBankir,
    overpayment: 3000,
    deadline: moment().toISOString(),
    status: LoanStatus.Expired
  },
  {
    id: '9',
    amount: 21400,
    term: 12,
    rate: 2,
    creditor: Creditor.SmsFinance,
    overpayment: 2100,
    deadline: moment().toISOString(),
    status: LoanStatus.Expired
  },
  {
    id: '10',
    amount: 18000,
    term: 5,
    rate: 2,
    creditor: Creditor.SlonFinance,
    overpayment: 800,
    deadline: moment().toISOString(),
    status: LoanStatus.repaid
  }
]

const suggestion2 = [
  {
    id: '23',
    amount: 22300,
    term: 5,
    rate: 2,
    creditor: Creditor.HoneyMoney,
    overpayment: 1000,
    deadline: moment().toISOString(),
    status: LoanStatus.Expired
  },
  {
    id: '123',
    amount: 16300,
    term: 1,
    rate: 2,
    creditor: Creditor.EcoFinance,
    overpayment: 1000,
    deadline: moment().toISOString(),
    status: LoanStatus.repaid
  },
  {
    id: '84',
    amount: 12000,
    term: 4,
    rate: 2,
    creditor: Creditor.WebBankir,
    overpayment: 1000,
    deadline: moment().toISOString(),
    status: LoanStatus.Expired
  }
]

// requestStatus = finished/processing/notStarted/archived

const activeProps = {
  userRequest: {
    amount: 15000,
    term: 23,
    createDate: moment().toISOString()
  },
  suggestions: suggestions
}

export const inactiveProps = {
  requestStatus: 'archived',
  history: [
    {
      suggestions: suggestion1,
      userRequest: {
        amount: 11200,
        term: 13,
        createDate: moment()
          .subtract(151, 'days')
          .toISOString()
      }
    },
    {
      suggestions: suggestion2,
      userRequest: {
        amount: 25000,
        term: 16,
        createDate: moment()
          .subtract(351, 'days')
          .toISOString()
      }
    }
  ]
}

export function LoanRequestListDemo () {
  return (
    <Layout>
      <Section padding title='Финальный результат'>
        <LoanRequestList
          requestData={{
            activeLoanRequest: {
              ...activeProps,
              requestStatus: 'finished',
              isApplicationFilled: true
            },
            inactiveLoanRequest: {
              ...inactiveProps
            }
          }}
        />
      </Section>
      <Section padding title='Не заполнен 1 шаг анкеты'>
        <LoanRequestList
          requestData={{
            activeLoanRequest: {
              ...activeProps,
              requestStatus: 'notStarted',
              isApplicationFilled: false,
              suggestions: { approvedSuggestion: [], notActiveSuggestion: [] }
            },
            inactiveLoanRequest: {
              ...inactiveProps
            }
          }}
        />
      </Section>
      <Section padding title='В процессе'>
        <LoanRequestList
          requestData={{
            activeLoanRequest: {
              ...activeProps,
              requestStatus: 'processing',
              isApplicationFilled: true
            },
            inactiveLoanRequest: {
              ...inactiveProps
            }
          }}
        />
      </Section>
      <Section padding title='В процессе с недозаполненной анкетой'>
        <LoanRequestList
          requestData={{
            activeLoanRequest: {
              ...activeProps,
              requestStatus: 'processing',
              isApplicationFilled: false
            },
            inactiveLoanRequest: {
              ...inactiveProps
            }
          }}
        />
      </Section>
    </Layout>
  )
}
