import React from 'react'
import * as R from 'ramda'
import moment from 'moment'
import {Layout, printMessage, Section} from '../helpers'
import {LoanList} from '../../modules/loan_list/loan_list'
import {Creditor, LoanStatus} from '../../domain'

const active = [
  {
    id: '0',
    status: LoanStatus.Serviced,
    amount: 5000,
    term: 21,
    loanDate: moment().toISOString(),
    rate: 1.8,
    creditor: Creditor.SmsFinance,
    overpayment: 1000,
    deadline: moment().toISOString()
  },
  {
    id: '1',
    status: LoanStatus.Overdue,
    amount: 5000,
    term: 5,
    loanDate: moment().subtract(10, 'days').toISOString(),
    rate: 1.8,
    creditor: Creditor.PayPS,
    overpayment: 1000,
    deadline: moment().toISOString()
  }
]

const other = [
  {
    id: '2',
    status: LoanStatus.Approved,
    amount: 3500,
    term: 10,
    rate: 2.4,
    creditor: Creditor.PayPS,
    overpayment: 1000,
    deadline: moment().toISOString()
  },
  {
    id: '3',
    status: LoanStatus.Rejected,
    amount: 4200,
    term: 26,
    timeout: 30,
    expirationDate: moment().add(10, 'hours').toISOString(),
    overpayment: 1000,
    deadline: moment().toISOString()
  },
  {
    id: '4',
    status: LoanStatus.Incomplete,
    amount: 40000,
    term: 50,
    rate: 1.3,
    overpayment: 1000,
    deadline: moment().toISOString()
  },
  {
    id: '5',
    status: LoanStatus.Processed,
    amount: 21000,
    term: 17,
    rate: 1.9,
    overpayment: 1000,
    isLongProcess: true,
    deadline: moment().toISOString()
  }
]

const archive = [
  {
    id: '8',
    status: LoanStatus.Expired,
    amount: 20000,
    term: 10,
    rate: 2,
    decisionDate: moment().subtract(3, 'days').toISOString(),
    closeDate: moment().subtract(3, 'days').toISOString(),
    creditor: Creditor.PayPS,
    overpayment: 1000,
    deadline: moment().toISOString()
  },
  {
    id: '6',
    status: LoanStatus.Repaid,
    amount: 20000,
    term: 10,
    rate: 2,
    closeDate: moment().subtract(3, 'days').toISOString(),
    creditor: Creditor.PayPS,
    overpayment: 1000,
    deadline: moment().toISOString()
  },
  {
    id: '9',
    status: LoanStatus.Rejected,
    amount: 20000,
    term: 10,
    rate: 2,
    decisionDate: moment().subtract(3, 'days').toISOString(),
    creditor: Creditor.PayPS,
    overpayment: 1000,
    deadline: moment().toISOString()
  },
  {
    id: '10',
    status: LoanStatus.Repaid,
    amount: 20000,
    term: 10,
    rate: 2,
    closeDate: moment().subtract(3, 'days').toISOString(),
    creditor: Creditor.PayPS,
    overpayment: 1000,
    deadline: moment().toISOString()
  },
  {
    id: '11',
    status: LoanStatus.Rejected,
    amount: 20000,
    term: 10,
    rate: 2,
    decisionDate: moment().subtract(30, 'days').toISOString(),
    creditor: Creditor.PayPS,
    overpayment: 1000,
    deadline: moment().toISOString()
  }
]

const calcParams = {
  minTerm: 5,
  maxTerm: 35,
  minAmount: 1000,
  maxAmount: 35000,
  amountStep: 500,
  termStep: 1,
  rate: 2
}

const defaultProps = {
  calc: calcParams,
  permissions: {},
  supportPhone: process.env.SUPPORT_PHONE,
  onMount: printMessage('On mount'),
  onUnmount: printMessage('On unmount'),
  onGetLoanDetails: printMessage('On get details'),
  fetchOpenLoans: printMessage('Fetch open loans'),
  fetchArchiveLoans: printMessage('Fetch archive loans'),
  fetchPermissions: printMessage('Fetch permissions'),
  fetchCalc: printMessage('On get calc details'),
  onGetMoreArchiveLoans: printMessage('On get more close loans'),
  hasMoreArchiveLoans: false,
  canCreateLoan: false,
  isNewLoanPending: false,
  onCreateLoan: printMessage('On create new loan'),
  onCreated: printMessage('On created'),
  componentDidMount: printMessage('On loan list componentDidMount'),
  componentWillUnmount: printMessage('On loan list componentWillUnmount')
}

export function LoanListDemo () {
  return (
    <Layout>

      <Section title='New loan is available'>
        <LoanList
          {...defaultProps}
          openLoans={[]}
          archiveLoans={[]}
          permissions={{canCreateLoan: true}}
        />
      </Section>

      <Section title='Active loans'>
        <LoanList
          {...defaultProps}
          openLoans={active}
          archiveLoans={[]}
        />
      </Section>

      <Section title='Other loans'>
        <LoanList
          {...defaultProps}
          openLoans={other}
          archiveLoans={[]}
        />
      </Section>

      <Section title='Active + other loans'>
        <LoanList
          {...defaultProps}
          openLoans={R.concat(active, other)}
          archiveLoans={[]}
        />
      </Section>

      <Section title='New loan + archive loans'>
        <LoanList
          {...defaultProps}
          openLoans={[]}
          archiveLoans={R.take(4, archive)}
          permissions={{canCreateLoan: true}}
        />
      </Section>

      <Section title='New loan + archive loans with more button'>
        <LoanList
          {...defaultProps}
          openLoans={[]}
          archiveLoans={archive}
          hasMoreArchiveLoans
          permissions={{canCreateLoan: true}}
        />
      </Section>

    </Layout>
  )
}
