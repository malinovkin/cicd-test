import React from 'react'
import moment from 'moment'
import {Layout, printMessage, Section} from '../helpers'
import {ExpiredLoan} from '../../modules/loan/expired_loan'
import {Creditor} from '../../domain'

export function ExpiredLoanDemo () {
  return (
    <Layout>

      <Section title='Default'>
        <ExpiredLoan
          onCreateLoan={printMessage('On get new loan')}
          onClose={printMessage('On close')}
          loan={{
            id: '0',
            term: 5,
            amount: 30500,
            decisionDate: moment().subtract(10, 'days').toISOString(),
            rate: 1.2,
            creditor: Creditor.SmsFinance,
            expirationDate: moment().subtract(5, 'days').toISOString(),
            overpayment: 1000
          }}
          supportPhone='1234567890'
          canCreateLoan
        />
      </Section>

      <Section title='Can not create new loan'>
        <ExpiredLoan
          onCreateLoan={printMessage('On get new loan')}
          onClose={printMessage('On close')}
          loan={{
            id: '0',
            term: 5,
            amount: 30500,
            decisionDate: moment().subtract(10, 'days').toISOString(),
            rate: 1.2,
            creditor: Creditor.SmsFinance,
            expirationDate: moment().subtract(5, 'days').toISOString(),
            overpayment: 1000
          }}
          supportPhone='1234567890'
          canCreateLoan={false}
        />
      </Section>

    </Layout>
  )
}
