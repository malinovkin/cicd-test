import React from 'react'
import {Layout, printMessage, Section} from '../helpers'
import {Processing} from '../../modules/loan/processing'

export function ApplicationProcessingDemo () {
  return (
    <Layout>

      <Section title='Default'>
        <Processing
          onClose={printMessage('On close')}
          onCheckLoanStatus={printMessage('On check loan status')}
          supportPhone='1234567890'
          loan={{}}
        />
      </Section>

      <Section title='Is long process'>
        <Processing
          onClose={printMessage('On close')}
          onCheckLoanStatus={printMessage('On check loan status')}
          supportPhone='1234567890'
          isLongProcess
          loan={{isLongProcess: true}}
        />
      </Section>

    </Layout>
  )
}
