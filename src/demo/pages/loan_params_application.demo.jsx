import React from 'react'
import {
  searchRegions,
  searchCities,
  searchStreets,
  searchHouses
} from '../../dadata'
import {handleSubmit, Layout, printMessage, Section} from '../helpers'
import {Application} from '../../modules/loan/application/application'
import {Creditor, LoanStatus} from '../../domain'
import {calc} from '../../modules/loan/application/steps'

const defaultProps = {
  supportPhone: '1234567890',
  onGetAddressRegion: searchRegions,
  onGetAddressCity: searchCities,
  onGetAddressStreet: searchStreets,
  onGetAddressHouse: searchHouses,
  fetchApplication: printMessage('On fetch application'),
  fetchCalc: printMessage('On fetch calc'),
  onFieldChange: printMessage('On field change'),
  loan: {
    status: LoanStatus.Incomplete
  },
  isSubmitPending: false,
  isFetchPending: false,
  onSubmit: handleSubmit,
  onClose: printMessage('On close'),
  onCheckLoanStatus: printMessage('On check loan status'),
  needAgreements: Creditor.SmsFinance,
  onShowAgreement: printMessage('On show agreement'),
  currentStep: calc
}

export function LoanParamsApplicationDemo () {
  return (
    <Layout>

      <Section title='Default'>
        <Application
          {...defaultProps}
          calc={{
            minAmount: 1000,
            minTerm: 5,
            maxAmount: 40000,
            maxTerm: 35,
            amountStep: 500,
            termStep: 1,
            rate: 1.4
          }}
          application={{
            profile: {},
            required: [],
            fixed: []
          }}
        />
      </Section>

      <Section title='Preset values'>
        <Application
          {...defaultProps}
          calc={{
            minAmount: 1000,
            minTerm: 5,
            maxAmount: 10000,
            maxTerm: 15,
            amountStep: 100,
            termStep: 1,
            rate: 1.4
          }}
          application={{
            profile: {
              amount: 8000,
              term: 6
            },
            required: [],
            fixed: []
          }}
        />
      </Section>

    </Layout>
  )
}
