import React from 'react'
import {Layout, Section} from '../helpers'
import {ServerError} from '../../modules/server_error'

export function ServerErrorDemo () {
  return (
    <Layout>
      <Section title='Default'>
        <ServerError />
      </Section>
    </Layout>
  )
}
