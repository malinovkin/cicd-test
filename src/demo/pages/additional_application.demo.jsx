import React from 'react'
import {
  searchRegions,
  searchCities,
  searchStreets,
  searchHouses
} from '../../dadata'
import {handleSubmit, Layout, printMessage, Section} from '../helpers'
import {Application} from '../../modules/loan/application/application'
import {LoanStatus} from '../../domain'
import {additional} from '../../modules/loan/application/steps'

const defaultProps = {
  supportPhone: '1234567890',
  onGetAddressRegion: searchRegions,
  onGetAddressCity: searchCities,
  onGetAddressStreet: searchStreets,
  onGetAddressHouse: searchHouses,
  fetchApplication: printMessage('On fetch application'),
  fetchCalc: printMessage('On fetch calc'),
  onFieldChange: printMessage('On field change'),
  application: {
    profile: {},
    required: [],
    fixed: []
  },
  loan: {
    status: LoanStatus.Incomplete
  },
  calc: {},
  isSubmitPending: false,
  isFetchPending: false,
  onSubmit: handleSubmit,
  onClose: printMessage('On close'),
  onCheckLoanStatus: printMessage('On check loan status'),
  onShowAgreement: printMessage('On show agreement'),
  currentStep: additional([])
}

const profile = {
  jobRegion: {
    region: 'Foo'
  },
  jobOrgName: 'Bar',
  jobType: 3,
  jobPosition: 'Директор',
  jobIncome: '12345',
  jobPhone: '9873562423',
  marriageStatus: 1,
  childrenAmount: 5,
  contactName: 'Вася Петя',
  contactType: 1,
  education: 1,
  contactPhone: '9873562423',
  email: 'myemail@email.com'
}

export function AdditionalApplicationDemo () {
  return (
    <Layout>

      <Section title='Default'>
        <Application
          {...defaultProps}
        />
      </Section>

      <Section title='Preset values'>
        <Application
          {...defaultProps}
          application={{
            profile,
            required: [],
            fixed: []
          }}
        />
      </Section>

      <Section title='Fixed fields'>
        <Application
          {...defaultProps}
          currentStep={additional([
            'jobRegion',
            'jobOrgName',
            'jobType',
            'jobPosition',
            'jobIncome',
            'jobPhone',
            'marriageStatus',
            'childrenAmount',
            'contactName',
            'contactType',
            'contactPhone',
            'education'
          ])}
          application={{
            profile,
            required: [],
            fixed: []
          }}
        />
      </Section>

    </Layout>
  )
}
