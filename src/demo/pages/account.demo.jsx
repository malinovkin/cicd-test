import React from 'react'
import * as R from 'ramda'
import {Layout, printMessage, Section} from '../helpers'
import {Account} from '../../modules/account'
import {Container} from '../../base'

export function AccountDemo () {
  return (
    <Layout>
      <Section title='Default' padding>
        <Account
          componentDidMount={printMessage('Did mount')}
          componentWillUnmount={printMessage('Will unmount')}
        >
          <Container>
            {R.range(0, 100).map(el => <div key={el}>{el}</div>)}
          </Container>
        </Account>
      </Section>
    </Layout>
  )
}
