import React from 'react'
import {
  searchRegions,
  searchCities,
  searchStreets,
  searchHouses
} from '../../dadata'
import {handleSubmit, Layout, printMessage, Section} from '../helpers'
import {Application} from '../../modules/loan_request/application/application'
import {Creditor, LoanStatus} from '../../domain'
import {base} from '../../modules/loan_request/application/steps'

const defaultProps = {
  onGetAddressRegion: searchRegions,
  onGetAddressCity: searchCities,
  onGetAddressStreet: searchStreets,
  onGetAddressHouse: searchHouses,
  fetchApplication: printMessage('On fetch application'),
  fetchCalc: printMessage('On fetch calc'),
  onFieldChange: printMessage('On field change'),
  application: {
    profile: {},
    required: [],
    fixed: []
  },
  loan: {
    status: LoanStatus.Incomplete
  },
  calc: {},
  isSubmitPending: false,
  isFetchPending: false,
  onSubmit: handleSubmit,
  onClose: printMessage('On close'),
  onCheckLoanStatus: printMessage('On check loan status'),
  onShowAgreement: printMessage('On show agreement'),
  currentStep: base([])
}

const profile = {
  firstName: 'Пётр',
  middleName: 'Петрович',
  lastName: 'Петров',
  birthDate: '1990-10-10',
  passportIssuedDate: '2000-10-10',
  passportSN: '1234 567890',
  gender: 'female',
  email: 'a@mail.com'
}

export function BaseApplicationDemo () {
  return (
    <Layout>

      <Section title='Default'>
        <Application
          {...defaultProps}
        />
      </Section>

      <Section title='Preset values'>
        <Application
          {...defaultProps}
          application={{
            profile,
            required: [],
            fixed: []
          }}
        />
      </Section>

      <Section title='Agreements'>
        <Application
          {...defaultProps}
          currentStep={{
            ...base([]),
            needAgreements: Creditor.SmsFinance
          }}
          application={{
            profile,
            required: [],
            fixed: []
          }}
        />
      </Section>

      <Section title='Fixed fields'>
        <Application
          {...defaultProps}
          currentStep={base([
            'lastName',
            'firstName',
            'middleName',
            'birthDate',
            'passportSN',
            'passportIssuedDate',
            'gender'
          ])}
          application={{
            profile,
            required: [],
            fixed: [
              'lastName',
              'firstName',
              'middleName',
              'birthDate',
              'passportSN',
              'passportIssuedDate',
              'gender'
            ]
          }}
        />
      </Section>

    </Layout>
  )
}
