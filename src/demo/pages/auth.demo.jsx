import React from 'react'
import {handleSubmit, Layout, printMessage, Section} from '../helpers'
import {Auth} from '../../modules/auth/auth'

const defaultProps = {
  defaultValues: {
    phone: ''
  },
  onClose: printMessage('Close auth.'),
  onAuthorize: handleSubmit,
  onConfirm: handleSubmit,
  onChangePhone: printMessage('On change phone'),
  onUnmount: printMessage('On unmount'),
  onCreated: printMessage('On created'),
  componentDidMount: printMessage('On componentDidMount'),
  componentWillUnmount: printMessage('On componentWillUnmount'),
  onShowAgreement: printMessage('On show agreement'),
  timer: 0,
  form: 'phone',
  isAuthorizePending: false,
  isConfirmPending: false,
  isNewUser: true
}

export function AuthDemo () {
  return (
    <Layout>

      <Section title='Enter phone, new user'>
        <Auth
          {...defaultProps}
        />
      </Section>

      <Section title='Enter phone, new user, set loan amount/term'>
        <Auth
          {...defaultProps}
          loanParams={{
            amount: 5000,
            term: 10
          }}
        />
      </Section>

      <Section title='Enter code, new user'>
        <Auth
          {...defaultProps}
          defaultValues={{
            phone: '1234567890'
          }}
          timer={20}
          form='code'
        />
      </Section>

      <Section title='Enter code, new user, code is sent'>
        <Auth
          {...defaultProps}
          defaultValues={{
            phone: '1234567890'
          }}
          form='code'
        />
      </Section>

      <Section title='Enter phone, old user, code is sent'>
        <Auth
          {...defaultProps}
          defaultValues={{
            phone: '1234567890'
          }}
          timer={20}
          isNewUser={false}
        />
      </Section>

    </Layout>
  )
}
