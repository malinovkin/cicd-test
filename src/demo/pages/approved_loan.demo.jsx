import React from 'react'
import moment from 'moment'
import {Layout, printMessage, Section} from '../helpers'
import {ApprovedLoan} from '../../modules/loan/approved_loan'
import {Creditor} from '../../domain'

export function ApprovedLoanDemo () {
  return (
    <Layout>

      <Section title='Default'>
        <ApprovedLoan
          onClose={printMessage('On close')}
          onGetMoney={printMessage('On get money')}
          loan={{
            id: '0',
            term: 30,
            amount: 30500,
            rate: 1.2,
            creditor: Creditor.Kviku,
            decisionDate: moment().toISOString(),
            expirationDate: moment().add(118, 'hours').toISOString(),
            overpayment: 1000,
            creditorUrl: 'www.google.com'
          }}
          supportPhone='1234567890'
        />
      </Section>

      <Section title='Default'>
        <ApprovedLoan
          onClose={printMessage('On close')}
          onGetMoney={printMessage('On get money')}
          loan={{
            id: '0',
            term: 30,
            amount: 30500,
            rate: 1.2,
            creditor: Creditor.PayPS,
            decisionDate: moment().toISOString(),
            expirationDate: moment().add(25, 'hours').toISOString(),
            overpayment: 1000,
            creditorUrl: 'www.google.com',
            isCounteroffer: true
          }}
          supportPhone='1234567890'
        />
      </Section>

    </Layout>
  )
}
