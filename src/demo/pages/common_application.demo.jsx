import React from 'react'
import {
  searchRegions,
  searchCities,
  searchStreets,
  searchHouses
} from '../../dadata'
import {handleSubmit, Layout, printMessage, Section} from '../helpers'
import {Application} from '../../modules/loan/application/application'
import {LoanStatus} from '../../domain'
import {common} from '../../modules/loan/application/steps'

require('../../api')

const defaultProps = {
  supportPhone: '1234567890',
  onGetAddressRegion: searchRegions,
  onGetAddressCity: searchCities,
  onGetAddressStreet: searchStreets,
  onGetAddressHouse: searchHouses,
  fetchApplication: printMessage('On fetch application'),
  fetchCalc: printMessage('On fetch calc'),
  onFieldChange: printMessage('On field change'),
  application: {
    profile: {},
    required: [],
    fixed: []
  },
  loan: {
    status: LoanStatus.Incomplete
  },
  calc: {},
  isSubmitPending: false,
  isFetchPending: false,
  onSubmit: handleSubmit,
  onClose: printMessage('On close'),
  onCheckLoanStatus: printMessage('On check loan status'),
  onShowAgreement: printMessage('On show agreement'),
  currentStep: common([])
}

const profile = {
  gender: 'female',
  birthPlace: 'село Горохово',
  passportIssuedBy: 'Надлежащими организациями',
  departmentCode: '111-222',
  registrationAddress: {
    region: 'Foo',
    city: 'Bar',
    street: 'Qux',
    house: '1',
    withoutFlat: true
  },
  registrationCoincidesWithTheFact: true,
  email: 'myemail@email.com'
}

export function CommonApplicationDemo () {
  return (
    <Layout>

      <Section title='Default'>
        <Application
          {...defaultProps}
        />
      </Section>

      <Section title='Preset values'>
        <Application
          {...defaultProps}
          application={{
            profile,
            required: [],
            fixed: []
          }}
        />
      </Section>

      <Section title='Fixed fields'>
        <Application
          {...defaultProps}
          currentStep={common([
            'gender',
            'birthPlace',
            'passportIssuedBy',
            'departmentCode',
            'registrationCoincidesWithTheFact'
          ])}
          application={{
            profile,
            required: [],
            fixed: []
          }}
        />
      </Section>

    </Layout>
  )
}
