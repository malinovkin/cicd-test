import React from 'react'
import moment from 'moment'
import {Layout, printMessage, Section} from '../helpers'
import {OverdueLoan} from '../../modules/loan/overdue_loan'
import {Creditor} from '../../domain'

export function OverdueLoanDemo () {
  return (
    <Layout>

      <Section title='Default'>
        <OverdueLoan
          onClose={printMessage('On close')}
          onGetPaymentMethods={printMessage('On get payment methods')}
          loan={{
            id: '0',
            term: 2,
            amount: 30500,
            rate: 1.2,
            creditor: Creditor.DoZarplaty,
            decisionDate: moment().subtract(5, 'days').toISOString(),
            moneyIssuedDate: moment().toISOString(),
            deadline: moment().toISOString(),
            overpayment: 1000
          }}
          supportPhone='1234567890'
        />
      </Section>

    </Layout>
  )
}
