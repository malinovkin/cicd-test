import React from 'react'
import moment from 'moment'
import {Layout, printMessage, Section} from '../helpers'
import {ServicedLoan} from '../../modules/loan/serviced_loan'
import {Creditor} from '../../domain'

export function ServicedLoanDemo () {
  return (
    <Layout>

      <Section title='Default'>
        <ServicedLoan
          onClose={printMessage('On close')}
          onGetPaymentMethods={printMessage('On get payment methods')}
          loan={{
            id: '0',
            term: 2,
            amount: 30500,
            decisionDate: moment().toISOString(),
            rate: 1.2,
            creditor: Creditor.PayPS,
            deadline: moment().add(2, 'days').toISOString(),
            overpayment: 1000
          }}
          supportPhone='1234567890'
        />
      </Section>

    </Layout>
  )
}
