import * as R from 'ramda'
import {createReducer} from '../../helpers/redux_helpers'

const Types = {
  OPEN_MODAL: 'modals/OPEN_MODAL',
  CLOSE_MODAL: 'modals/CLOSE_MODAL'
}

export const ModalsActions = {
  openModal (modalId, data) {
    return {
      type: Types.OPEN_MODAL,
      payload: {
        data,
        id: modalId
      }
    }
  },

  closeModal (modalId) {
    return {
      type: Types.CLOSE_MODAL,
      payload: {
        id: modalId
      }
    }
  }
}

const initialState = {}

export const modalsReducer = createReducer(initialState, {
  [Types.OPEN_MODAL] (state, action) {
    return R.merge(state, {
      [action.payload.id]: {
        open: true,
        data: action.payload.data
      }
    })
  },

  [Types.CLOSE_MODAL] (state, action) {
    return R.merge(state, {
      [action.payload.id]: {
        open: false,
        data: null
      }
    })
  }
})
