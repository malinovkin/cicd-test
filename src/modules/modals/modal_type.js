export const ModalType = {
  ServerError: 'serverError',
  CloseApplication: 'closeApplication',
  AutoLogout: 'autoLogout',
  Agreement: 'agreement',
  LoanCalculator: 'loanCalculator',
  RedirectToCreditor: 'redirectToCreditor',
  PassportChangeHelp: 'passportChangeHelp'
}
