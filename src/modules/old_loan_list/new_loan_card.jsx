import React from 'react'
import {Card, Col, Row, Title} from '../../base'
import {LoanCalculator} from '../shared/loan_calculator'

export function NewLoanCard (props) {
  return (
    <Card borderType='accent'>

      <Row className='u-mt-2 u-mb-4'>
        <Col md={8} offsetMd={2}>
          <Title size={3} sm={2} center>
            Заявка на новый займ
          </Title>
        </Col>
      </Row>

      <Row className='u-mb-2'>
        <Col md={8} offsetMd={2}>
          <LoanCalculator {...props} />
        </Col>
      </Row>

    </Card>
  )
}
