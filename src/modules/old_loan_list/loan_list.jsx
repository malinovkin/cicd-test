import React from 'react'
import P from 'prop-types'
import {
  Button,
  Col,
  Container,
  ContentBeforeFooter,
  ContentWithFooter,
  ExpansionPanel,
  Footer,
  Icon,
  Row,
  Text,
  Title,
  Link
} from '../../base'
import {ApprovedLoanCard} from './approved_loan_card'
import {ArchiveRejectedLoanCard} from './archive_rejected_loan_card'
import {ArchiveRepaidLoanCard} from './archive_repaid_loan_card'
import {ArchiveExpiredLoanCard} from './archive_expired_loan_card'
import {IncompleteLoanCard} from './incomplete_loan_card'
import {NewLoanCard} from './new_loan_card'
import {OverdueLoanCard} from './overdue_loan_card'
import {ProcessedLoanCard} from './processed_loan_card'
import {AlternativeRejectedLoanCard} from './alternative_rejected_loan_card'
import {ServicedLoanCard} from './serviced_loan_card'
import {NewLoanBlockingCard} from './new_loan_blocking_card'
import {MoneyTransferLoanCard} from './money_transfer_loan_card'
import {ListLoader} from './list_loader'
import {LoanStatus, splitOpenLoans} from '../../domain'
import {LoanProps} from '../shared/loan_props'
import {AccountNavbar} from '../shared/account_navbar'
import {formatPhone} from '../../helpers/format_helpers'
import {scrollUp} from '../../helpers/dom_helpers'

export class LoanList extends React.Component {
  static propTypes = {
    componentDidMount: P.func.isRequired,
    onGetLoanDetails: P.func.isRequired,
    onGetMoreArchiveLoans: P.func.isRequired,
    hasMoreArchiveLoans: P.bool.isRequired,
    componentWillUnmount: P.func.isRequired,
    onCreateLoan: P.func.isRequired,
    fetchInitialLoanConditions: P.func.isRequired,
    fetchOpenLoans: P.func.isRequired,
    fetchArchiveLoans: P.func.isRequired,
    fetchPermissions: P.func.isRequired,

    calc: P.object,
    openLoans: P.arrayOf(LoanProps),
    archiveLoans: P.arrayOf(LoanProps),
    permissions: P.object,
    isCreateLoanPending: P.bool,
    isGetMoreArchiveLoansPending: P.bool,
    onGetAgreements: P.func
  }

  static defaultProps = {
    calc: null,
    openLoans: null,
    archiveLoans: null,
    permissions: null,
    isCreateLoanPending: false,
    isGetMoreArchiveLoansPending: false
  }

  static isFirstMount = true

  componentDidMount () {
    this.props.componentDidMount()
    scrollUp()

    if (!this.props.calc) {
      this.props.fetchInitialLoanConditions()
    }
    if (!this.props.openLoans) {
      this.props.fetchOpenLoans()
    }
    if (!this.props.permissions ||
      !LoanList.isFirstMount) {
      this.props.fetchPermissions()
    }

    LoanList.isFirstMount = false
  }

  componentWillUnmount () {
    this.props.componentWillUnmount()
  }

  getOpenLoans () {
    return this.props.openLoans || []
  }

  getArchiveLoans () {
    return this.props.archiveLoans || []
  }

  isVisibleDataLoading () {
    return !this.props.openLoans || !this.props.calc || !this.props.permissions
  }

  handleToggleArchive = () => {
    if (!this.props.archiveLoans) {
      this.props.fetchArchiveLoans()
    }
  }

  renderOpenLoan (loan) {
    const cardProps = {
      loan,
      onGetLoanDetails: this.props.onGetLoanDetails
    }

    switch (loan.status) {
      case LoanStatus.Incomplete:
        return <IncompleteLoanCard {...cardProps} />
      case LoanStatus.Approved:
        return <ApprovedLoanCard {...cardProps} />
      case LoanStatus.Rejected: {
        return <AlternativeRejectedLoanCard {...cardProps} />
      }
      case LoanStatus.Overdue:
        return <OverdueLoanCard {...cardProps} />
      case LoanStatus.Processed:
        return <ProcessedLoanCard {...cardProps} />
      case LoanStatus.Serviced:
        return <ServicedLoanCard {...cardProps} />
      case LoanStatus.MoneyTransfer:
        return <MoneyTransferLoanCard {...cardProps} />
      default:
        return null
    }
  }

  renderArchiveLoan (loan) {
    const cardProps = {
      loan,
      onGetLoanDetails: this.props.onGetLoanDetails
    }

    switch (loan.status) {
      case LoanStatus.Expired:
        return <ArchiveExpiredLoanCard {...cardProps} />
      case LoanStatus.Rejected:
        return <ArchiveRejectedLoanCard {...cardProps} />
      case LoanStatus.Repaid:
        return <ArchiveRepaidLoanCard {...cardProps} />
      default:
        return null
    }
  }

  renderNewLoan () {
    if (this.isVisibleDataLoading()) {
      return null
    }

    if (this.props.permissions.canCreateLoan) {
      return (
        <div className='u-mb-6'>
          <NewLoanCard
            {...this.props.calc}
            onSubmit={this.props.onCreateLoan}
            isSubmitPending={this.props.isCreateLoanPending}
          />
        </div>
      )
    }

    return null
  }

  renderOpenLoans () {
    if (this.isVisibleDataLoading()) {
      return null
    }

    const [active, other] = splitOpenLoans(this.getOpenLoans())

    if (this.getOpenLoans().length > 0) {
      return (
        <div>
          {active.length > 0
            ? <div>
              <div className='u-mb-6'>
                <NewLoanBlockingCard />
              </div>
              <div className='u-mb-6'>
                <Row className='u-mb-2'>
                  <Col className='u-text-align-center u-text-align-left-md'>
                    <Title size={5}>Действующие займы</Title>
                  </Col>
                </Row>
                {active.map(el => (
                  <Row key={el.id} className='u-mb-1'>
                    <Col>
                      {this.renderOpenLoan(el)}
                    </Col>
                  </Row>
                ))}
              </div>
            </div>
            : null
          }

          {other.length > 0
            ? <div className='u-mb-6'>
              {other.map(el => (
                <Row key={el.id} className='u-mb-1'>
                  <Col>
                    {this.renderOpenLoan(el)}
                  </Col>
                </Row>
              ))}
            </div>
            : null
          }
        </div>
      )
    }

    return null
  }

  renderArchiveLoans () {
    if (!this.props.permissions ||
      this.props.permissions.emptyArchiveLoans !== false ||
      this.isVisibleDataLoading()
    ) {
      return null
    }

    const {hasMoreArchiveLoans} = this.props

    return (
      <ExpansionPanel
        caption='История займов'
        defaultClosed
        captionCenter
        captionLeftMd
        onToggle={this.handleToggleArchive}
      >
        <div className={hasMoreArchiveLoans ? 'u-mb-4' : 'u-mb-6'}>
          {this.getArchiveLoans().length > 0
            ? this.getArchiveLoans().map(el => (
              <Row key={el.id} className='u-mb-1'>
                <Col>
                  {this.renderArchiveLoan(el)}
                </Col>
              </Row>
            ))
            : <div className='u-text-align-center'>Загрузка...</div>
          }
        </div>
        {hasMoreArchiveLoans
          ? <Row className='u-mb-6'>
            <Col offsetMd={4} md={4} className='u-text-align-center'>
              <Button
                onClick={this.props.onGetMoreArchiveLoans}
                color='secondary'
                maxWidth
                isPending={this.props.isGetMoreArchiveLoansPending}
              >
                Показать еще
              </Button>
            </Col>
          </Row>
          : null
        }
      </ExpansionPanel>
    )
  }

  renderFooterContent () {
    return (
      <Container className='u-pt-2 u-pt-4-md u-pb-4'>
        <Row>
          <Col>
            <Icon name='finspin-logo-icon' />
          </Col>
        </Row>
        <Row>
          <Col md={4} className='u-pt-2'>
            <Text color='secondary' type='caption'>
              Подбираем микрозаймы на любые цели. До 30 000 рублей за 10 минут.
              Заявка онлайн. Выдача 24 часа в сутки.
            </Text>
          </Col>
          <Col md={8} className='u-pt-2'>
            <Text type='body-1' bold>
              <Link href={`tel:8${process.env.SUPPORT_PHONE}`}>
                {formatPhone(process.env.SUPPORT_PHONE, {firstDigit: '8'})}
              </Link>
            </Text>
            <Text color='secondary' type='caption'>
              С 9:00 до 18:00 по московскому времени звонок по России бесплатный
            </Text>
            <br />
            <Text color='secondary' type='caption'>
              Email: <Link href='mailto:support@finspin.ru'>support@finspin.ru</Link>
            </Text>
          </Col>
        </Row>
        <Row className='u-pt-2'>
          <Col>
            <Text color='secondary' type='caption'>
              Общество с ограниченной ответственностью «Финспин».
              Юридический адрес: 620075, г. Екатеринбург, ул. Мамина-Сибиряка, д. 101, офис 4.06
              ОГРН 1146670012444, ИНН 6670424655.
              ООО «Финспин» сотрудничает только с надлежащим образом
              зарегистрированными МФО, состоящими в государственном реестре
              микрофинансовых организаций ЦБ РФ.
            </Text>
            <Text color='secondary' type='caption' className='u-mt-1'>
              <Link onClick={this.props.onGetAgreements}>
                Соглашения и документы
              </Link>
            </Text>
            <Text color='secondary' type='caption' className='u-pt-2'>
              © 2016-{(new Date()).getFullYear()} ООО «Финспин»
            </Text>
          </Col>
        </Row>
      </Container>
    )
  }

  render () {
    return (
      <ContentWithFooter className='g-workspace-bg'>
        <ContentBeforeFooter>
          <AccountNavbar />
          <Container className='u-pt-4 u-pb-2'>
            {this.renderNewLoan()}
            {this.renderOpenLoans()}
            {this.renderArchiveLoans()}
            {this.isVisibleDataLoading() ? <ListLoader /> : null}
          </Container>
        </ContentBeforeFooter>
        <Footer className='u-border-top'>
          {this.renderFooterContent()}
        </Footer>
      </ContentWithFooter>
    )
  }
}
