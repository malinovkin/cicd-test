import React from 'react'
import moment from 'moment'
import {Col, Container, Icon, Row, Text, Title} from '../../base'
import {formatDayCount, formatMoney} from '../../helpers/format_helpers'
import {CardTemplate, LoanCardProps, LoanParam} from './shared'

export function ArchiveRepaidLoanCard (props) {
  const {loan} = props

  return (
    <CardTemplate

      block1={
        <div>
          <Row className='u-mb-1'>
            <Col>
              <Text color='secondary'>Статус</Text>
            </Col>
          </Row>

          <Row>
            <Col>
              <Title size={3}>Заявка закрыта</Title>
            </Col>
          </Row>
        </div>
      }

      block2={
        <LoanParam
          caption='Сумма займа'
          value={<Title size={1} regular>{formatMoney(loan.amount)}</Title>}
        />
      }

      block3={
        <Container>
          <Row>
            <Col size='auto' className='u-mr-4 u-mb-2 u-p-0'>
              <LoanParam
                caption='Срок займа'
                value={
                  <Title size={3} regular>
                    {loan.term}{' '}
                    {formatDayCount(loan.term)}
                  </Title>
                }
              />
            </Col>

            <Col size='auto' className='u-mr-4 u-mb-2 u-p-0'>
              <LoanParam
                caption='Кредитор'
                value={
                  <Icon name={`${loan.creditor}-logo-icon`} />
                }
              />
            </Col>

            <Col size='auto' className='u-mb-2 u-p-0'>
              <LoanParam
                caption='Займ закрыт'
                value={
                  <Title size={3} regular>
                    {moment(loan.closeDate).format('DD.MM.YYYY')}
                  </Title>
                }
              />
            </Col>
          </Row>
        </Container>
      }

      onAction={() => props.onGetLoanDetails(loan.id)}
      buttonCaption='Подробнее'
      cardBorderType='neutral'
    />
  )
}

ArchiveRepaidLoanCard.propTypes = LoanCardProps
