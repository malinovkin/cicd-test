import React from 'react'
import {Card, Col, Icon, Row, Text, Title} from '../../base'

export function NewLoanBlockingCard () {
  const title = (
    <Title
      size={4}
      className={
        'u-mb-3 u-mb-1-md u-mt-2 ' +
        'u-text-align-center u-text-align-left-md'
      }
    >
      Вы скоро сможете взять новый займ
    </Title>
  )

  return (
    <Card>
      <Row className='u-hidden-mt-md'>
        <Col>
          {title}
        </Col>
      </Row>

      <Row>
        <Col
          md={2}
          className={
            'u-mb-3 u-my-1-md ' +
            'u-text-align-center'
          }
        >
          <Icon name='smile-warning-icon' />
        </Col>
        <Col md={9}>
          <Row className='u-hidden-lt-md'>
            <Col>
              {title}
            </Col>
          </Row>

          <Row className='u-mb-1'>
            <Col>
              <Text center leftMd>
                У вас уже есть действующий займ, оформленный через Finspin.{' '}
                Чтобы взять следующий займ, необходимо погасить действующий,{' '}
                поэтому напоминаем вам о важности своевременного погашения займа.
              </Text>
            </Col>
          </Row>

        </Col>
      </Row>
    </Card>
  )
}
