import React, {useContext} from 'react'
// deprecated
import {LoanListActions} from './loan_list_data'
import {SessionActions} from '../session_data'
import {LoanList} from './loan_list'
import {StoreContext, RouterContext} from '../../context'

export function LoanListContainer () {
  const {state: {loanList, session}, dispatch} = useContext(StoreContext)
  const {history} = useContext(RouterContext)

  return (
    <LoanList
      permissions={session.permissions}
      calc={session.calc}
      openLoans={loanList.openLoans}
      archiveLoans={loanList.archiveLoans}
      hasMoreArchiveLoans={loanList.hasMoreArchiveLoans}
      isCreateLoanPending={loanList.isCreateLoanPending}
      isGetMoreArchiveLoansPending={loanList.isGetMoreArchiveLoansPending}

      fetchPermissions={() => dispatch(SessionActions.fetchUser())}
      fetchOpenLoans={() => dispatch(LoanListActions.fetchOpenLoans())}
      fetchArchiveLoans={() => dispatch(LoanListActions.fetchArchiveLoans())}
      componentWillUnmount={() => dispatch(LoanListActions.unmount())}
      onGetLoanDetails={(id) => history.push(`/loans/${id}`)}
      onGetAgreements={() => history.push(`/agreements`)}
      onGetMoreArchiveLoans={() => dispatch(LoanListActions.getMoreArchiveLoans())}
      onCreateLoan={({amount, term}) => dispatch(LoanListActions.createLoan(amount, term, {history}))}
      fetchInitialLoanConditions={() => dispatch(SessionActions.fetchInitialLoanConditions())}
    />
  )
}
