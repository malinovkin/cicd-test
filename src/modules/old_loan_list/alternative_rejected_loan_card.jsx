import React from 'react'
import {Col, Icon, Row, Text, Title} from '../../base'
import {CardTemplate, LoanCardProps} from './shared'

export function AlternativeRejectedLoanCard (props) {
  const {loan} = props

  return (
    <CardTemplate
      block1={
        <div>
          <Row className='u-mb-1'>
            <Col>
              <Title size={3}>Подходящего займа пока не нашлось</Title>
            </Col>
          </Row>
          <Row>
            <Col>
              <Text>
                Нам не удалось найти для вас подходящий займ{' '}
                среди подключенных к нам МФО.
              </Text>
            </Col>
          </Row>
        </div>
      }

      block2={
        <div>
          <div className='u-flex-md u-mb-1 u-text-align-center u-text-align-left-md'>
            <div className='u-mr-3-md'>
              <Icon name='smile-idea-icon' />
            </div>
            <div>
              <Text inline='block' color='secondary' className='u-mb-2'>
                Рекомендуем
              </Text>
              <Title size={4}>
                Вы можете отправить заявку на займ у наших партнеров
              </Title>
            </div>
          </div>
        </div>
      }

      onAction={() => props.onGetLoanDetails(loan.id)}
      buttonCaption='Подробнее'
      cardBorderType='none'
    />
  )
}

AlternativeRejectedLoanCard.propTypes = LoanCardProps
