import React from 'react'
import moment from 'moment'
import {Col, Container, Icon, Row, Text, Title} from '../../base'
import {formatMoney} from '../../helpers/format_helpers'
import {calcTotalLoanAmount} from '../../domain'
import {CardTemplate, LoanCardProps, LoanParam} from './shared'

export function ServicedLoanCard (props) {
  const {loan} = props

  return (
    <CardTemplate

      block1={
        <div>
          <Row className='u-mb-1'>
            <Col>
              <Text color='secondary'>Статус</Text>
            </Col>
          </Row>

          <Row className='u-mb-1'>
            <Col>
              <Title size={3}>Займ обслуживается</Title>
            </Col>
          </Row>
        </div>
      }

      block2={
        <LoanParam
          caption='К возврату'
          value={
            <Title size={1} regular>
              {formatMoney(calcTotalLoanAmount(loan.amount, loan.overpayment))}
            </Title>
          }
        />
      }

      block3={
        <Container>
          <Row>
            <Col size='auto' className='u-mr-4 u-mb-2 u-p-0'>
              <LoanParam
                caption='Вернуть до'
                value={
                  <Title size={3} regular>
                    {moment(loan.deadline).format('DD.MM.YYYY')}
                  </Title>
                }
              />
            </Col>

            <Col size='auto' className='u-mr-4 u-mb-2 u-p-0'>
              <LoanParam
                caption='Кредитор'
                value={
                  <Icon name={`${loan.creditor}-logo-icon`} />
                }
              />
            </Col>

            <Col size='auto' className='u-mb-2 u-p-0'>
              <LoanParam
                caption='Сумма займа'
                value={
                  <Title size={3} regular>
                    {formatMoney(loan.amount)}
                  </Title>
                }
              />
            </Col>
          </Row>
        </Container>
      }

      onAction={() => props.onGetLoanDetails(loan.id)}
      buttonCaption='Подробнее'
      cardBorderType='success'
    />
  )
}

ServicedLoanCard.propTypes = LoanCardProps
