import React from 'react'
import {Col, Container, Icon, Row, Text, Title} from '../../base'
import {formatDayCount, formatMoney} from '../../helpers/format_helpers'
import {calcTotalLoanAmount} from '../../domain'
import {CardTemplate, LoanCardProps, LoanParam} from './shared'

export function ApprovedLoanCard (props) {
  const {loan} = props

  return (
    <CardTemplate

      block1={
        <div>
          <Row className='u-mb-1'>
            <Col>
              <Text color='secondary'>Статус</Text>
            </Col>
          </Row>

          <Row>
            <Col>
              <Title size={3}>Займ одобрен!</Title>
            </Col>
          </Row>
        </div>
      }

      block2={
        <LoanParam
          caption='Сумма займа'
          value={<Title size={1} regular>{formatMoney(loan.amount)}</Title>}
        />
      }

      block3={
        <Container>
          <Row>
            <Col size='auto' className='u-mr-4 u-mb-2 u-p-0'>
              <LoanParam
                caption='Срок займа'
                value={
                  <Title size={3} regular>
                    {loan.term} {formatDayCount(loan.term)}
                  </Title>
                }
              />
            </Col>

            <Col size='auto' className='u-mr-4 u-mb-2 u-p-0'>
              <LoanParam
                caption='Кредитор'
                value={
                  <Icon name={`${loan.creditor}-logo-icon`} />
                }
              />
            </Col>

            <Col size='auto' className='u-mb-2 u-p-0'>
              <LoanParam
                caption='К возврату'
                value={
                  <Title size={3} regular>
                    {formatMoney(
                      calcTotalLoanAmount(loan.amount, loan.overpayment))}
                  </Title>
                }
              />
            </Col>
          </Row>
        </Container>
      }

      onAction={() => props.onGetLoanDetails(loan.id)}
      buttonCaption='Получить деньги'
      buttonColor='primary'
      cardBorderType='success'
    />
  )
}

ApprovedLoanCard.propTypes = LoanCardProps
