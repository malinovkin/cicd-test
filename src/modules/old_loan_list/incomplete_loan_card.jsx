import * as R from 'ramda'
import React from 'react'
import {Col, Row, Text, Title} from '../../base'
import {formatDayCount, formatMoney} from '../../helpers/format_helpers'
import {calcTotalLoanAmount} from '../../domain'
import {CardTemplate, LoanCardProps, LoanParam} from './shared'

export function IncompleteLoanCard (props) {
  const {loan} = props

  return (
    <CardTemplate

      block1={
        <div>
          <Row className='u-mb-1'>
            <Col>
              <Text color='secondary'>Статус</Text>
            </Col>
          </Row>

          <Row className='u-mb-1'>
            <Col>
              <Title size={3}>Новая заявка</Title>
            </Col>
          </Row>

          <Row>
            <Col>
              <Text>
                Осталось заполнить всего несколько полей.
              </Text>
            </Col>
          </Row>
        </div>
      }

      block2={
        R.isNil(loan.amount) ? ''
          : <LoanParam
            caption='Сумма займа'
            value={<Title size={1} regular>{formatMoney(loan.amount)}</Title>}
          />
      }

      block3={
        <Row>
          {R.isNil(loan.term) ? null
            : <Col size='auto' className='u-mr-4 u-mb-2'>
              <LoanParam
                caption='Срок займа'
                value={
                  <Title size={3} regular>
                    {loan.term} {formatDayCount(loan.term)}
                  </Title>
                }
              />
            </Col>
          }

          {R.isNil(loan.rate) ? null
            : <Col size='auto' className='u-mb-2'>
              <LoanParam
                caption='К возврату'
                value={
                  <Title size={3} regular>
                    {formatMoney(
                      calcTotalLoanAmount(loan.amount, loan.overpayment))}
                  </Title>
                }
              />
            </Col>
          }
        </Row>
      }

      onAction={() => props.onGetLoanDetails(loan.id)}
      buttonColor='primary'
      buttonCaption='Продолжить'
    />
  )
}

IncompleteLoanCard.propTypes = LoanCardProps
