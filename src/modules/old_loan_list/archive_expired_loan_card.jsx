import * as R from 'ramda'
import React from 'react'
import {Col, Container, Icon, Row, Text, Title} from '../../base'
import {formatDayCount, formatMoney} from '../../helpers/format_helpers'
import {CardTemplate, LoanCardProps, LoanParam} from './shared'
import {calcTotalLoanAmount} from '../../domain'

export function ArchiveExpiredLoanCard (props) {
  const {loan} = props

  return (
    <CardTemplate

      block1={
        <div>
          <Row className='u-mb-1'>
            <Col>
              <Text color='secondary'>Статус</Text>
            </Col>
          </Row>

          <Row>
            <Col>
              <Title size={3}>Срок одобрения истек</Title>
            </Col>
          </Row>
        </div>
      }

      block2={
        <LoanParam
          caption='Сумма займа'
          value={<Title size={1} regular>{formatMoney(loan.amount)}</Title>}
        />
      }

      block3={
        <Container>
          <Row>
            <Col size='auto' className='u-mr-4 u-mb-2 u-p-0'>
              <LoanParam
                caption='Срок займа'
                value={
                  <Title size={3} regular>
                    {loan.term}{' '}
                    {formatDayCount(loan.term)}
                  </Title>
                }
              />
            </Col>

            {R.isNil(loan.creditor) ? null
              : <Col size='auto' className='u-mr-4 u-mb-2 u-p-0'>
                <LoanParam
                  caption='Кредитор'
                  value={
                    <Icon name={`${loan.creditor}-logo-icon`} />
                  }
                />
              </Col>
            }

            {R.isNil(loan.overpayment) ? null
              : <Col size='auto' className='u-mb-2 u-p-0'>
                <LoanParam
                  caption='К возврату'
                  value={
                    <Title size={3} regular>
                      {formatMoney(
                        calcTotalLoanAmount(loan.amount, loan.overpayment))}
                    </Title>
                  }
                />
              </Col>
            }
          </Row>
        </Container>
      }

      onAction={() => props.onGetLoanDetails(loan.id)}
      buttonCaption='Подробнее'
      cardBorderType='neutral'
    />
  )
}

ArchiveExpiredLoanCard.propTypes = LoanCardProps
