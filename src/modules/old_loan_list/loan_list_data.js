import * as R from 'ramda'
import {createReducer} from '../../helpers/redux_helpers'
import {ApplicationActions} from '../loan/application/application_data'

const Types = {
  FETCH_OPEN_LOANS: 'loanList/FETCH_OPEN_LOANS',
  FETCH_ARCHIVE_LOANS: 'loanList/FETCH_ARCHIVE_LOANS',
  FETCH_MORE_ARCHIVE_LOANS: 'loanList/FETCH_MORE_ARCHIVE_LOANS',
  CREATE_LOAN: 'loanList/CREATE_LOAN',
  UNMOUNT: 'loanList/UNMOUNT_VIEW'
}

const ARCHIVE_LOANS_LIMIT = 10

export const LoanListActions = {
  fetchOpenLoans: () => (dispatch, getState, api) =>
    dispatch({
      type: Types.FETCH_OPEN_LOANS,
      payload: api.fetchOpenLoans()
    }),

  fetchArchiveLoans: () => (dispatch, getState, api) =>
    dispatch({
      type: Types.FETCH_ARCHIVE_LOANS,
      payload: api.fetchArchiveLoans(0, ARCHIVE_LOANS_LIMIT)
    }),

  getMoreArchiveLoans: () => (dispatch, getState, api) => {
    const archive = getState().loanList.archiveLoans

    return dispatch({
      type: Types.FETCH_MORE_ARCHIVE_LOANS,
      payload: api.fetchArchiveLoans(archive.length, ARCHIVE_LOANS_LIMIT)
    })
  },

  createLoan: (amount, term, ctx) => (dispatch, getState, api) => {
    dispatch(ApplicationActions.addPrefilled({amount, term}))

    return dispatch({
      type: Types.CREATE_LOAN,
      payload: api.createLoanRequest()
    })
      .then((action) => {
        ctx.history.push(`/loans/${action.value.id}`)
      })
  },

  unmount () {
    return {
      type: Types.UNMOUNT
    }
  }
}

const initialState = {
  openLoans: null,
  archiveLoans: null,
  hasMoreArchiveLoans: false,
  isGetMoreArchiveLoansPending: false,
  isCreateLoanPending: false
}

export const loanListReducer = createReducer(initialState, {
  [Types.FETCH_OPEN_LOANS]: {
    fulfilled (state, {payload}) {
      return R.merge(state, {
        openLoans: R.uniqBy(R.path(['id']),
          R.concat(state.openLoans || [], payload.items))
      })
    }
  },

  [Types.FETCH_ARCHIVE_LOANS]: {
    fulfilled (state, {payload}) {
      const archiveLoans = R.uniqBy(R.path(['id']),
        R.concat(state.archiveLoans || [], payload.items))

      return R.merge(state, {
        archiveLoans,
        hasMoreArchiveLoans: archiveLoans.length < payload.count
      })
    }
  },

  [Types.FETCH_MORE_ARCHIVE_LOANS]: {
    pending (state) {
      return R.merge(state, {
        isGetMoreArchiveLoansPending: true
      })
    },
    fulfilled (state, {payload}) {
      const archiveLoans = R.uniqBy(R.path(['id']),
        R.concat(state.archiveLoans || [], payload.items))

      return R.merge(state, {
        archiveLoans,
        hasMoreArchiveLoans: archiveLoans.length < payload.items.length,
        isGetMoreArchiveLoansPending: false
      })
    }
  },

  [Types.CREATE_LOAN]: {
    pending (state) {
      return R.merge(state, {
        isCreateLoanPending: true
      })
    },
    fulfilled (state) {
      return R.merge(state, {
        isCreateLoanPending: false
      })
    }
  },

  [Types.UNMOUNT] () {
    return initialState
  }
})
