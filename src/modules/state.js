import * as R from 'ramda'
// import moment from 'moment'
import {applyMiddleware, createStore} from 'redux'
import thunkMiddleware from 'redux-thunk'
import promiseMiddleware from 'redux-promise-middleware'
import {createLogger} from 'redux-logger'

import {API} from '../api'
import {createReducer} from '../helpers/redux_helpers'
import {searchAddresses} from '../dadata'
import {clearCookie} from './shared/filkos_pixel'

const Events = {
  // Запросить смс с код для авторизации
  REQUEST_SMS: 'REQUEST_SMS',

  // Поменять номер телефона с которым хотим войти
  REENTER_AUTH_PHONE: 'REENTER_AUTH_PHONE',

  // Авторизоваться с кодом из смс (signup или signin без разницы)
  AUTHORIZE: 'AUTHORIZE',

  AUTHORIZE_PROCESSING: 'AUTHORIZE_PROCESSING',

  // Обновить таймер обратного отсчета до возмножности повторого запроса смс кода
  UPDATE_SMS_TIMER: 'UPDATE_SMS_TIMER',

  LOGOUT: 'LOGOUT',

  // Установить телефон пользователя (например из url)
  SET_USER_PHONE: 'SET_USER_PHONE',

  // Установить желаемые параметры займа (сумма и срок)
  SET_WANTED_LOAN_CONDITIONS: 'SET_WANTED_LOAN_CONDITIONS',

  // Загрузить данные о пользователе
  FETCH_USER: 'FETCH_USER',

  // Загрузить условия нового займа (например для анкеты на шаге выбора суммы и срока)
  FETCH_NEW_LOAN_OPTIONS: 'FETCH_NEW_LOAN_OPTIONS',

  // Загрузить общую статистику по выданным займам
  FETCH_LOAN_STATISTICS: 'FETCH_LOAN_STATISTICS',

  // Проверить, есть ли кука авторизации
  CHECK_AUTH_COOKIE: 'CHECK_AUTH_COOKIE',

  CREATE_LOAN_REQUEST: 'CREATE_LOAN_REQUEST',

  RESET_AUTH: 'RESET_AUTH',

  OPEN_MODAL: 'OPEN_MODAL',

  CLOSE_MODAL: 'CLOSE_MODAL',

  FETCH_LOAN_REQUEST: 'FETCH_LOAN_REQUEST',

  CHECK_LOAN_REQUEST_STATUS: 'CHECK_LOAN_REQUEST_STATUS',

  FETCH_ACTIVE_LOAN_REQUEST: 'FETCH_ACTIVE_LOAN_REQUEST',

  FETCH_ARCHIVE_LOAN_REQUEST: 'FETCH_ARCHIVE_LOAN_REQUEST',

  FETCH_LOAN_REQUEST_SUGGESTIONS: 'FETCH_LOAN_REQUEST_SUGGESTIONS',

  FETCH_ACTIVE_LOAN_REQUEST_SUGGESTIONS: 'FETCH_ACTIVE_LOAN_REQUEST_SUGGESTIONS',

  FETCH_ALTERNATIVE_OFFERS: 'FETCH_ALTERNATIVE_OFFERS',

  RESET_LOAN_REQUEST: 'RESET_LOAN_REQUEST',

  FETCH_PROFILE: 'FETCH_PROFILE',

  APPLICATION_UNMOUNT: 'APPLICATION_UNMOUNT',

  LOAN_REQUEST_LIST_UNMOUNT: 'LOAN_REQUEST_LIST_UNMOUNT',

  LOAN_REQUEST_UNMOUNT: 'LOAN_REQUEST_UNMOUNT',

  // Отправить анкету
  SEND_APPLICATION: 'SEND_APPLICATION',

  ADD_LEAD_PROVIDER_PARAMS: 'ADD_LEAD_PROVIDER_PARAMS',

  REMOVE_LEAD_PROVIDER_PARAMS: 'REMOVE_LEAD_PROVIDER_PARAMS',

  // Перейти на первый шаг.
  // На первый шаг можно попасть только с шага калькулятора, с API всегда приходит 0 шаг вместо 1-го.
  GO_TO_FIRST_STEP: 'GO_TO_FIRST_STEP',

  // Переключение между списками предложений и анкетой
  GO_TO_APPLICATION: 'GO_TO_APPLICATION',
  GO_TO_LIST: 'GO_TO_LIST'
}

export const Actions = {
  fetchLoanStatistics () {
    return {
      type: Events.FETCH_LOAN_STATISTICS,
      payload: API.fetchLoanStatistics()
    }
  },

  fetchNewLoanOptions () {
    return {
      type: Events.FETCH_NEW_LOAN_OPTIONS,
      payload: API.fetchNewLoanOptions()
    }
  },

  setWantedLoanConditions (amount, term) {
    return {
      type: Events.SET_WANTED_LOAN_CONDITIONS,
      payload: {amount, term}
    }
  },

  // Принимает object, где key это название куки, а value значение куки
  checkAuthCookie (cookies) {
    return {
      type: Events.CHECK_AUTH_COOKIE,
      payload: R.has('_identity-frontend', cookies)
    }
  },

  requestSMS (phone) {
    return (dispatch, getState) => {
      dispatch({
        type: Events.REQUEST_SMS,
        payload: {
          promise: API.requestSMS(phone),
          data: {phone}
        }
      })
        .then(() => {
          // Запускаем таймер обратного отсчета для разблокирования возможности
          // перезапроса смс
          const id = setInterval(() => {
            if (getState().auth.timer === 0) {
              clearInterval(id)
            } else {
              dispatch({type: Events.UPDATE_SMS_TIMER})
            }
          }, 1000)
        })
    }
  },

  authorize (code, router) {
    // 1) Авторизуемся.
    // 2) Если пользователь первичный (впервые на сайте) и он указал параметры займа на лендинге, то
    //    создаем новую заявку, и отправляем его на url с заявкой.
    // 3) Иначе отправляем его на список заявок.
    return (dispatch, getState) => {
      dispatch(Actions.setAuthorizeProcessing(true))
      return dispatch({
        type: Events.AUTHORIZE,
        payload: API.authorize(code, getState().primaryUser)
      })
        .then(() => {
          const {primaryUser} = getState()
          if (!primaryUser) {
            return dispatch(Actions.fetchActiveLoanRequest())
          }
        })
        .then(() => {
          const {loanRequest, loanRequestList, leadProviderParams} = getState()

          if (loanRequestList.activeLoanRequest.id == null && loanRequest.amount != null && loanRequest.term != null) {
            return dispatch(Actions.createLoanRequest(leadProviderParams.common))
          }
        })
        .then(() => {
          const {loanRequest} = getState()
          // Проверяем id на null/undefined потому что там может быть 0
          if (loanRequest.id != null) {
            // Сохраняем на бекенде данные через одно место
            // Нет проверки на amount и term потому что loanRequest.id может быть только
            // когда заявка создалась. Без amount и term это невозможно
            API.saveLoanConditions(loanRequest.id, loanRequest.amount, loanRequest.term)
            router.history.push(`/loan-requests/${loanRequest.id}`)
          } else {
            router.history.push('/loan-requests')
          }
        })
        .then(() => dispatch(Actions.setAuthorizeProcessing(false)))
    }
  },

  setAuthorizeProcessing (flag) {
    return {
      type: Events.AUTHORIZE_PROCESSING,
      payload: flag
    }
  },

  reenterAuthPhone () {
    return {type: Events.REENTER_AUTH_PHONE}
  },

  addLeadProviderParams (leadProviderParams) {
    return (dispatch, getState) => {
      dispatch({
        type: Events.ADD_LEAD_PROVIDER_PARAMS,
        payload: leadProviderParams
      })
    }
  },

  removeLeadProviderParams (provider) {
    return (dispatch, getState) => {
      dispatch({
        type: Events.REMOVE_LEAD_PROVIDER_PARAMS,
        payload: {provider}
      })
    }
  },

  fetchUser () {
    return {
      type: Events.FETCH_USER,
      payload: API.fetchUser()
    }
  },

  createLoanRequest (commonLeadProviderParams) {
    return {
      type: Events.CREATE_LOAN_REQUEST,
      payload: API.createLoanRequest(commonLeadProviderParams)
    }
  },

  setUserPhone (phone) {
    return {
      type: Events.SET_USER_PHONE,
      payload: phone
    }
  },

  resetAuth () {
    return {type: Events.RESET_AUTH}
  },

  openModal (modalId, data) {
    return {
      type: Events.OPEN_MODAL,
      payload: {
        data,
        id: modalId
      }
    }
  },

  closeModal (modalId) {
    return {
      type: Events.CLOSE_MODAL,
      payload: {
        id: modalId
      }
    }
  },

  fetchLoanRequest (loanRequestId) {
    return {
      type: Events.FETCH_LOAN_REQUEST,
      payload: API.fetchLoanRequest(loanRequestId)
    }
  },

  checkLoanRequestStatus (loanRequestId) {
    return {
      type: Events.CHECK_LOAN_REQUEST_STATUS,
      payload: API.fetchLoanRequest(loanRequestId)
    }
  },

  fetchActiveLoanRequest () {
    return {
      type: Events.FETCH_ACTIVE_LOAN_REQUEST,
      payload: API.fetchActiveLoanRequest()
    }
  },

  fetchLoanRequestSuggestion (loanRequestId) {
    return {
      type: Events.FETCH_LOAN_REQUEST_SUGGESTIONS,
      payload: API.fetchLoanRequestSuggestion(loanRequestId)
    }
  },

  fetchActiveLoanRequestSuggestion (loanRequestId) {
    return {
      type: Events.FETCH_ACTIVE_LOAN_REQUEST_SUGGESTIONS,
      payload: API.fetchLoanRequestSuggestion(loanRequestId)
    }
  },

  fetchArchiveLoanRequest (limit, offset) {
    return {
      type: Events.FETCH_ARCHIVE_LOAN_REQUEST,
      payload: API.fetchArchiveLoanRequest(limit, offset)
    }
  },

  fetchAlternativeOffers (loanRequestId) {
    return {
      type: Events.FETCH_ALTERNATIVE_OFFERS,
      payload: API.fetchAlternativeOffers(loanRequestId)
    }
  },

  resetLoanRequest () {
    return {type: Events.RESET_LOAN_REQUEST}
  },

  fetchProfile (loanRequestId) {
    return {
      type: Events.FETCH_PROFILE,
      payload: API.fetchProfile(loanRequestId)
    }
  },

  sendApplication (application) {
    return (dispatch, getState) => {
      // Если в адресах повторников не хватает ОКАТО, дозапрашиваем данные с Дадаты
      // (для некоторых http запросы это слишком сложно и долго, поэтому этим занимается клиент,
      // хотя если делать это грамотно, то данная неконсистентность данных бекенда должна устраняться
      // на бекенде)
      function checkOkato () {
        const promises = []
        const {registrationAddress, factAddress, jobAddress} = application
        if (registrationAddress && !registrationAddress.okato) {
          promises[0] = (searchAddresses(registrationAddress.unrestrictedValue, {count: 1}).then(res => res[0].okato))
        }
        if (factAddress && !factAddress.okato) {
          promises[1] = (searchAddresses(factAddress.unrestrictedValue, {count: 1}).then(res => res[0].okato))
        }
        if (jobAddress && !jobAddress.okato) {
          promises[2] = (searchAddresses(jobAddress.unrestrictedValue, {count: 1}).then(res => res[0].okato))
        }

        return Promise.all(promises)
          .then(([regAddrOkato, factAddrOkato, jobAddrOkato]) => {
            return R.mergeRight(
              application,
              {
                // Если окато коды есть, дополняем адреса
                registrationAddress: regAddrOkato
                  ? R.assoc('okato', regAddrOkato, application.registrationAddress)
                  : application.registrationAddress,
                factAddress: factAddrOkato
                  ? R.assoc('okato', factAddrOkato, application.factAddress)
                  : application.factAddress,
                jobAddress: jobAddrOkato
                  ? R.assoc('okato', jobAddrOkato, application.jobAddress)
                  : application.jobAddress
              })
          })
      }

      const state = getState()

      return checkOkato()
        .then(app => {
          // Если отправляем первый шаг, берем amount и term и отправляем их вместе с анкетой в API,
          // при этом amount и term не сохраняем в profile, тк эти поля хранятся в loanRequest.
          // Данное обстоятельство вызвано отвратительным дизайном backend api.
          const app2 = R.has('firstName', app)
            ? R.mergeRight(app, {
              amount: state.loanRequest.amount,
              term: state.loanRequest.term
            })
            : app
          return dispatch({
            type: Events.SEND_APPLICATION,
            payload: {
              promise: API.sendApplication(state.loanRequest.id, app2),
              // Здесь передаем оригинальный application без amount и term (причину смотри чуть выше)
              data: app
            }
          })
            .then(() => dispatch(Actions.fetchLoanRequest(state.loanRequest.id)))
            .then(({value}) => {
              if (value.status !== 'needInfo') {
                dispatch(Actions.goToList())
              }
              if (value.status === 'needInfo') {
                return dispatch(Actions.fetchProfile(state.loanRequest.id))
              }
            })
        })
    }
  },

  logout () {
    return (dispatch, getState) => {
      clearCookie()
      return dispatch({
        type: Events.LOGOUT,
        payload: API.logout()
      })
    }
  },

  applicationUnmount () {
    return {type: Events.APPLICATION_UNMOUNT}
  },

  loanRequestListUnmount () {
    return {type: Events.LOAN_REQUEST_LIST_UNMOUNT}
  },

  loanRequestUnmount () {
    return {type: Events.LOAN_REQUEST_UNMOUNT}
  },

  goToFirstStep () {
    return {type: Events.GO_TO_FIRST_STEP}
  },

  goToApplication () {
    return {type: Events.GO_TO_APPLICATION}
  },
  goToList () {
    return {type: Events.GO_TO_LIST}
  }
}

const defaultStates = {
  auth: {
    phone: '',
    form: 'phone',
    timer: 0
  },

  userPhone: null,

  loanRequest: {
    amount: null,
    term: null,
    onApplication: false,
    id: null,
    alternativeOffers: null,
    suggestions: []
  },

  loanRequestList: {
    activeLoanRequest: {
      id: null,
      requestedAmount: null,
      requestedTerm: null
    },
    archiveLoanRequest: null
  }
}

const initialState = {
  // Данные калькулятора для новой заявки
  newLoanOptions: null,

  // Статистика по выданным займам, используется на лендинге
  loanStatistics: null,

  // Данные о заявке
  loanRequest: defaultStates.loanRequest,

  loanRequestList: defaultStates.loanRequestList,

  auth: defaultStates.auth,

  // Флаги разрашений пользователя, что может что нет
  permissions: null,

  // Профиль пользователя, используется в анкете
  profile: null,

  // Авторизован ли пользователь
  authorized: false,

  // Новый пользователь на сайте или нет
  primaryUser: false,

  // Filkos
  leadProviderParams: {common: null, filkos: null},

  // Модальные окна
  modals: {},

  pendings: {
    requestSMS: false,
    authorize: false,
    fetchUser: false,
    createLoanRequest: false,
    fetchLoanRequest: false,
    fetchActiveLoanRequest: false,
    fetchActiveLoanRequestSuggestion: false,
    fetchArchiveLoanRequest: false,
    fetchProfile: false,
    fetchAlternativeOffers: false,
    sendApplication: false,
    checkLoanRequest: false
  }
}

const reducer = createReducer(initialState, {
  [Events.FETCH_NEW_LOAN_OPTIONS]: {
    fulfilled (state, {payload}) {
      return R.mergeRight(state, {
        newLoanOptions: payload
      })
    }
  },

  [Events.FETCH_LOAN_STATISTICS]: {
    fulfilled (state, {payload}) {
      return R.mergeRight(state, {
        loanStatistics: payload
      })
    }
  },

  [Events.SET_WANTED_LOAN_CONDITIONS] (state, {payload}) {
    return R.mergeDeepRight(state, {
      loanRequest: {
        amount: payload.amount,
        term: payload.term
      }
    })
  },

  [Events.REQUEST_SMS]: {
    pending (state, {payload}) {
      return R.mergeDeepRight(state, {
        auth: {
          phone: payload.phone
        },
        pendings: {requestSMS: true}
      })
    },
    fulfilled (state, {payload}) {
      return R.mergeDeepRight(state, {
        primaryUser: payload.primaryUser,
        auth: {
          timer: payload.resendTimeout,
          form: 'code'
        },
        pendings: {requestSMS: false}
      })
    },
    rejected (state) {
      return R.mergeDeepRight(state, {
        pendings: {requestSMS: false}
      })
    }
  },

  [Events.ADD_LEAD_PROVIDER_PARAMS] (state, {payload}) {
    return R.mergeDeepRight(state, {
      leadProviderParams: R.pick(['common', 'filkos'], payload)
    })
  },

  [Events.REMOVE_LEAD_PROVIDER_PARAMS] (state, {payload}) {
    return R.mergeDeepRight(state, {
      leadProviderParams: {[payload.provider]: null}
    })
  },

  [Events.AUTHORIZE]: {
    pending (state) {
      return R.mergeDeepRight(state, {
        pendings: {authorize: true}
      })
    },
    fulfilled (state) {
      return R.mergeDeepRight(state, {
        authorized: true,
        pendings: {authorize: false}
      })
    },
    rejected (state) {
      return R.mergeDeepRight(state, {
        pendings: {authorize: false}
      })
    }
  },

  [Events.AUTHORIZE_PROCESSING] (state, {payload}) {
    return R.mergeRight(state, {
      authorizeProcessing: payload
    })
  },

  [Events.UPDATE_SMS_TIMER] (state) {
    return R.mergeDeepRight(state, {
      auth: {timer: state.auth.timer - 1}
    })
  },

  [Events.REENTER_AUTH_PHONE] (state) {
    return R.mergeDeepRight(state, {
      auth: {form: 'phone'}
    })
  },

  [Events.FETCH_USER]: {
    pending (state) {
      return R.mergeDeepRight(state, {
        pendings: {fetchUser: true}
      })
    },
    fulfilled (state, {payload}) {
      return R.mergeDeepRight(state, {
        permissions: payload.permissions,
        userPhone: payload.userPhone,
        pendings: {fetchUser: false}
      })
    },
    rejected (state) {
      return R.mergeDeepRight(state, {
        pendings: {fetchUser: false}
      })
    }
  },

  [Events.CREATE_LOAN_REQUEST]: {
    pending (state) {
      return R.mergeDeepRight(state, {
        pendings: {createLoanRequest: true}
      })
    },
    fulfilled (state, {payload}) {
      return R.mergeDeepRight(state, {
        pendings: {createLoanRequest: false},
        loanRequest: {id: payload.id}
      })
    },
    rejected (state) {
      return R.mergeDeepRight(state, {
        pendings: {createLoanRequest: false}
      })
    }
  },

  [Events.SET_USER_PHONE] (state, {payload}) {
    return R.mergeDeepRight(state, {
      auth: {phone: payload}
    })
  },

  [Events.RESET_AUTH] (state) {
    return R.mergeRight(state, {
      auth: defaultStates.auth
    })
  },

  [Events.OPEN_MODAL] (state, action) {
    return R.mergeDeepRight(state, {
      modals: {
        [action.payload.id]: {
          open: true,
          data: action.payload.data
        }
      }
    })
  },

  [Events.CLOSE_MODAL] (state, action) {
    return R.mergeDeepRight(state, {
      modals: {
        [action.payload.id]: {
          open: false,
          data: null
        }
      }
    })
  },

  [Events.FETCH_LOAN_REQUEST]: {
    pending (state) {
      return R.mergeDeepRight(state, {
        pendings: {fetchLoanRequest: true}
      })
    },
    fulfilled (state, {payload}) {
      const onApp = payload.step != null && state.loanRequest.onApplication
      if (payload.amount == null && state.loanRequest.amount) {
        return R.mergeDeepRight(state, {
          loanRequest: {
            id: payload.id,
            status: payload.status,
            step: payload.step,
            creationDate: payload.creationDate,
            expirationDate: payload.expirationDate,
            term: state.loanRequest.term,
            amount: state.loanRequest.amount,
            isLongProcess: payload.isLongProcess,
            countLoanRequestItem: payload.countLoanRequestItem,
            onApplication: onApp
          },
          pendings: {fetchLoanRequest: false}
        })
      }
      return R.mergeDeepRight(state, {
        loanRequest: {...payload, onApplication: onApp},
        pendings: {fetchLoanRequest: false}
      })
    },
    rejected (state) {
      return R.mergeDeepRight(state, {
        pendings: {fetchLoanRequest: false}
      })
    }
  },
  [Events.CHECK_LOAN_REQUEST_STATUS]: {
    pending (state) {
      return R.mergeDeepRight(state, {
        pendings: {checkLoanRequest: true}
      })
    },
    fulfilled (state, {payload}) {
      return R.mergeDeepRight(state, {
        loanRequest: {
          countLoanRequestItem: payload.countLoanRequestItem,
          status: payload.status
        },
        pendings: {checkLoanRequest: false}
      })
    },
    rejected (state) {
      return R.mergeDeepRight(state, {
        pendings: {checkLoanRequest: false}
      })
    }
  },
  [Events.FETCH_ACTIVE_LOAN_REQUEST]: {
    pending (state) {
      return R.mergeDeepRight(state, {
        pendings: {fetchActiveLoanRequest: true}
      })
    },
    fulfilled (state, {payload}) {
      return R.mergeDeepRight(state, {
        loanRequestList: payload,
        pendings: {fetchActiveLoanRequest: false}
      })
    },
    rejected (state) {
      return R.mergeDeepRight(state, {
        pendings: {fetchActiveLoanRequest: false}
      })
    }
  },

  [Events.FETCH_ARCHIVE_LOAN_REQUEST]: {
    pending (state) {
      return R.mergeDeepRight(state, {
        pendings: {fetchArchiveLoanRequest: true}
      })
    },
    fulfilled (state, {payload}) {
      return R.mergeDeepRight(state, {
        loanRequestList: payload,
        pendings: {fetchArchiveLoanRequest: false}
      })
    },
    rejected (state) {
      return R.mergeDeepRight(state, {
        pendings: {fetchArchiveLoanRequest: false}
      })
    }
  },

  [Events.FETCH_LOAN_REQUEST_SUGGESTIONS]: {
    pending (state) {
      return R.mergeDeepRight(state, {
        pendings: {fetchLoanRequestSuggestions: true}
      })
    },
    fulfilled (state, {payload}) {
      return R.mergeDeepRight(state, {
        loanRequest: {suggestions: payload},
        pendings: {fetchLoanRequestSuggestions: false}
      })
    },
    rejected (state) {
      return R.mergeDeepRight(state, {
        pendings: {fetchLoanRequestSuggestions: false}
      })
    }
  },

  [Events.FETCH_ACTIVE_LOAN_REQUEST_SUGGESTIONS]: {
    pending (state) {
      return R.mergeDeepRight(state, {
        pendings: {fetchActiveLoanRequestSuggestion: true}
      })
    },
    fulfilled (state, {payload}) {
      return R.mergeDeepRight(state, {
        loanRequestList: {activeLoanRequest: {suggestions: payload}},
        pendings: {fetchActiveLoanRequestSuggestions: false}
      })
    },
    rejected (state) {
      return R.mergeDeepRight(state, {
        pendings: {fetchActiveLoanRequestSuggestions: false}
      })
    }
  },

  [Events.FETCH_ALTERNATIVE_OFFERS]: {
    pending (state) {
      return R.mergeDeepRight(state, {
        pendings: {fetchAlternativeOffers: true}
      })
    },
    fulfilled (state, {payload}) {
      return R.mergeDeepRight(state, {
        loanRequest: {alternativeOffers: payload},
        pendings: {fetchAlternativeOffers: false}
      })
    },
    rejected (state) {
      return R.mergeDeepRight(state, {
        pendings: {fetchAlternativeOffers: false}
      })
    }
  },

  [Events.RESET_LOAN_REQUEST] (state) {
    return R.mergeRight(state, {
      loanRequest: defaultStates.loanRequest
    })
  },

  [Events.FETCH_PROFILE]: {
    pending (state) {
      return R.mergeDeepRight(state, {
        pendings: {fetchProfile: true}
      })
    },
    fulfilled (state, {payload}) {
      return R.mergeDeepRight(state, {
        profile: payload,
        pendings: {fetchProfile: false}
      })
    },
    rejected (state) {
      return R.mergeDeepRight(state, {
        pendings: {fetchProfile: false}
      })
    }
  },

  [Events.SEND_APPLICATION]: {
    pending (state, {payload}) {
      return R.mergeDeepRight(state, {
        pendings: {sendApplication: true},
        profile: payload
      })
    },
    fulfilled (state) {
      return R.mergeDeepRight(state, {
        pendings: {sendApplication: false}
      })
    },
    rejected (state) {
      return R.mergeDeepRight(state, {
        pendings: {sendApplication: false}
      })
    }
  },

  // [Events.LOGOUT] (state) {
  //   return R.mergeDeepRight(state, {
  //     authorized: initialState.authorized
  //   })
  // },
  [Events.LOGOUT]: {
    fulfilled (state) {
      return initialState
    }
  },

  [Events.APPLICATION_UNMOUNT] (state) {
    return R.mergeDeepRight(state, {
      profile: initialState.profile
    })
  },

  [Events.LOAN_REQUEST_LIST_UNMOUNT] (state) {
    return R.mergeDeepRight(state, {
      loanRequestList: initialState.loanRequestList
    })
  },

  [Events.LOAN_REQUEST_UNMOUNT] (state) {
    return R.mergeDeepRight(state, {
      profile: initialState.profile,
      loanRequest: initialState.loanRequest
    })
  },

  [Events.GO_TO_FIRST_STEP] (state) {
    return R.mergeDeepRight(state, {
      loanRequest: {
        step: 1,
        onApplication: true
      }
    })
  },

  [Events.GO_TO_APPLICATION] (state) {
    return R.mergeDeepRight(state, {
      loanRequest: {onApplication: true}
    })
  },

  [Events.GO_TO_LIST] (state) {
    return R.mergeDeepRight(state, {
      loanRequest: {onApplication: false}
    })
  },

  [Events.CHECK_AUTH_COOKIE] (state, {payload}) {
    return R.mergeRight(state, {
      authorized: payload
    })
  }
})

export function configureStore (initial) {
  const middlewares = [thunkMiddleware, promiseMiddleware()]

  if (process.env.REDUX_LOG) {
    middlewares.push(createLogger({collapsed: true}))
  }

  return createStore(reducer, initial, applyMiddleware(...middlewares))
}
