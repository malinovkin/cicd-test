import React, { useContext, useEffect, useState } from 'react'
import moment from 'moment'

import {
  Button,
  Col,
  Container,
  Form,
  Icon,
  IconButton,
  Link,
  Row,
  Text,
  TextField,
  Title
} from '../../base'
import { isConfirmationCode, isPhone } from '../../validation'
import * as Format from '../../helpers/format_helpers'
import { AgreementName, AgreementVendor } from '../../domain'
import { scrollUp } from '../../helpers/dom_helpers'
import { StoreContext, RouterContext } from '../../context'
import { changePage } from '../../metrics'
import { ModalType } from '../modals/modal_type'
import { Actions } from '../state'

function AuthTitle () {
  const [eng] = useState(process.env.THEME === 'eng')

  const { state } = useContext(StoreContext)
  const title = eng
    ? state.loanRequest.amount && state.loanRequest.term
      ? `Borrow ${Format.formatMoney(state.loanRequest.amount)} ` +
        `for ${state.loanRequest.term} ${Format.formatDayCount(
          state.loanRequest.term
        )}`
      : 'Microloan for any purpose'
    : state.loanRequest.amount && state.loanRequest.term
      ? `Займ ${Format.formatMoney(state.loanRequest.amount)} ` +
      `на ${state.loanRequest.term} ${Format.formatDayCount(
        state.loanRequest.term
      )}`
      : 'Микрозайм на любые цели'

  return (
    <Col offsetMd={1} md={5}>
      <div className='u-text-align-center'>
        <div className='u-hidden-lt-sm u-mb-6 u-mt-2 get-money-icon' />
      </div>
      <Title className='u-mb-2' size={4} sm={3} center>
        {title}
      </Title>
      <div className='u-border-bottom u-border-bottom-none-md'>
        <Text type='body-2' sm='subtitle' center className='u-mb-2'>
          {eng
            ? 'We will find the best offer from lenders for free.'
            : 'Мы бесплатно подберём наиболее подходящее предложение среди всех кредиторов.'}
        </Text>
      </div>
    </Col>
  )
}

function PhoneForm () {
  const [eng] = useState(process.env.THEME === 'eng')
  const [formRef, setFormRef] = useState(null)
  const { state, dispatch } = useContext(StoreContext)

  function handleValidate ({ phone }) {
    return { phone: isPhone(phone, { required: true }) }
  }

  function handlePreSubmit ({ phone }) {
    return { phone: Format.parsePhone(phone) }
  }

  function handleSubmit ({ phone }) {
    formRef.form.invalidate(dispatch(Actions.requestSMS(phone)))
  }

  return (
    <div className='u-pl-1-md'>
      <Title className='u-my-2' size={3} sm={2} center leftMd>
        {eng ? 'Phone Verification' : 'Подтверждение телефона'}
      </Title>
      <Text className='u-mb-2'>
        {eng
          ? 'Enter your mobile number. We will send a free SMS message with a verification code.'
          : 'Укажите ваш номер мобильного телефона. Мы вышлем на него бесплатное SMS-сообщение с кодом для подтверждения.'}
      </Text>
      <Form
        preSubmit={handlePreSubmit}
        onSubmit={handleSubmit}
        validate={handleValidate}
        defaultValues={{ phone: Format.formatPhone(state.auth.phone) }}
        ref={setFormRef}
      >
        <TextField
          type='tel'
          label={eng ? 'Mobile number' : 'Мобильный телефон'}
          field='phone'
          mask='+7 (999) 999-99-99'
          dataAttrs={{ stat: '1', id: 'phone' }}
        />
        {state.auth.timer > 0 ? (
          <div className='u-mt-1'>
            <Text type='body-2' color='secondary' inline>
              {eng
                ? 'An SMS confirmation code has been sent. Resend verification code.'
                : 'SMS с кодом подтверждения отправлено. Продолжить можно через:'}{' '}
            </Text>
            <Text type='body-2' inline>
              {Format.formatTimer(state.auth.timer)}
            </Text>
          </div>
        ) : null}
        <div className='u-my-4'>
          <Button
            maxWidth
            isPending={state.pendings.requestSMS}
            disabled={state.auth.timer > 0}
            type='submit'
            dataAttrs={{ stat: '1', id: 'phoneSubmit' }}
          >
            {eng ? 'Continue' : 'Продолжить'}
          </Button>
        </div>
      </Form>
    </div>
  )
}

function SMSCodeForm () {
  const [eng] = useState(process.env.THEME === 'eng')
  const { state, dispatch } = useContext(StoreContext)
  const router = useContext(RouterContext)
  const [formRef, setFormRef] = useState(null)

  const moscowDate = moment().utcOffset(3)
  const agreementsData = {
    phone: Format.formatPhone(state.auth.phone),
    date: moscowDate.format('DD.MM.YYYY'),
    time: moscowDate.format('HH:mm')
  }

  function handlePreSubmit ({ confirmationCode }) {
    return { confirmationCode: Format.parseConfirmationCode(confirmationCode) }
  }

  function handleSubmit (values) {
    return dispatch(Actions.authorize(values.confirmationCode, router))
  }

  function handleValidate ({ confirmationCode }) {
    return {
      confirmationCode: isConfirmationCode(confirmationCode, {
        required: true
      })
    }
  }

  function onShowAgreement (vendor, agreement, data) {
    dispatch(
      Actions.openModal(ModalType.Agreement, { vendor, agreement, data })
    )
  }

  function handleChangePhone () {
    dispatch(Actions.reenterAuthPhone())
  }

  function handleResendCode () {
    formRef.form.invalidate(dispatch(Actions.requestSMS(state.auth.phone)))
  }

  return (
    <div className='u-pl-1-md'>
      <Title className='u-my-2' size={3} sm={2} center leftMd>
        {eng ? 'Phone Verification' : 'Подтверждение телефона.'}
      </Title>
      <Form
        preSubmit={handlePreSubmit}
        onSubmit={handleSubmit}
        validate={handleValidate}
        defaultValues={{ phone: Format.formatPhone(state.auth.phone) }}
        ref={setFormRef}
      >
        {state.primaryUser ? (
          <Text className='u-mb-2' type='body-2'>
            {eng
              ? ' By registering, you accept the '
              : 'Регистрируясь, вы принимаете '}
            <Link
              onClick={() =>
                onShowAgreement(
                  AgreementVendor.Finspin,
                  AgreementName.PersonalSignUse
                )
              }
              dataAttrs={{ stat: '1' }}
            >
              {eng
                ? 'Terms of Use for the analogue of a handwritten signature.'
                : ' Условия использования аналога собственноручной подписи.'}
            </Link>
          </Text>
        ) : null}
        <TextField
          type='tel'
          label={eng ? 'Mobile number' : 'Мобильный телефон'}
          field='phone'
          mask='+7 (999) 999-99-99'
          disabled
        />
        <Text className='u-mb-2 u-mt-1' type='body-2'>
          <Link onClick={handleChangePhone} dataAttrs={{ stat: '1' }}>
            {eng ? 'Change number' : 'Изменить номер'}
          </Link>
        </Text>
        <TextField
          label={eng ? 'Confirmation code' : 'Код подтверждения'}
          field='confirmationCode' // code
          mask='9 9 9 9'
          dataAttrs={{ stat: '1', id: 'code' }}
        />
        {state.auth.timer > 0 ? (
          <div className='u-mt-1'>
            <Text type='body-2' color='secondary' inline>
              {eng
                ? 'An SMS confirmation code has been sent. Resend verification code. '
                : 'SMS с кодом подтверждения отправлено. Запросить новый код можно через: '}
            </Text>
            <Text type='body-2' inline>
              {Format.formatTimer(state.auth.timer)}
            </Text>
          </div>
        ) : (
          <div className='u-mt-1'>
            <Text type='body-2' color='secondary' inline>
              {eng
                ? 'An SMS confirmation code has been sent '
                : 'SMS с кодом подтверждения отправлено. '}
            </Text>
            <Text type='body-2' inline>
              <Link onClick={handleResendCode}>
                {' '}
                {eng ? 'Resend verification code' : 'Выслать код повторно'}
              </Link>
            </Text>
          </div>
        )}

        <Button
          className='u-my-4'
          maxWidth
          isPending={state.pendings.authorize}
          type='submit'
          dataAttrs={{ stat: '1', id: 'confirmationCodeSubmit' }}
        >
          {eng ? 'Confirm' : 'Подтвердить'}
        </Button>
        {state.primaryUser ? (
          <Text className='u-mb-2' type='body-2'>
            {eng
              ? 'By entering the code received from SMS, you consent '
              : 'Указывая полученный из SMS код, вы соглашаетесь с '}
            <Link
              onClick={() =>
                onShowAgreement(
                  AgreementVendor.Finspin,
                  AgreementName.ProcessingPersonalData,
                  agreementsData
                )
              }
              dataAttrs={{ stat: '1' }}
            >
              {eng
                ? 'to the processing of your data'
                : 'Соглашением на обработку персональных данных'}
            </Link>
            ,{' '}
            <Link
              onClick={() =>
                onShowAgreement(
                  AgreementVendor.Finspin,
                  AgreementName.InfoReceiving,
                  agreementsData
                )
              }
              dataAttrs={{ stat: '1' }}
            >
              {eng
                ? 'to receive information'
                : 'Соглашением на получение информации'}
            </Link>
            ,{' '}
            <Link
              onClick={() =>
                onShowAgreement(
                  AgreementVendor.Finspin,
                  AgreementName.CreditInfoDisclosure,
                  agreementsData
                )
              }
              dataAttrs={{ stat: '1' }}
            >
              {eng
                ? 'to grant access to your credit history'
                : 'Соглашением на получение доступа к кредитной истории'}
            </Link>
            .
          </Text>
        ) : null}
      </Form>
    </div>
  )
}

function AuthForm () {
  const { state } = useContext(StoreContext)

  switch (state.auth.form) {
    case 'code':
      return <SMSCodeForm />
    case 'phone':
      return <PhoneForm />
    default:
      return null
  }
}

export function Auth () {
  const { dispatch } = useContext(StoreContext)
  const { history } = useContext(RouterContext)

  useEffect(() => {
    changePage('auth')
    scrollUp()
    return () => dispatch(Actions.resetAuth())
  }, [])

  return (
    <Container>
      <Row hBetween className='u-my-3 u-my-6-md'>
        <Col center>
          <Icon name='finspin-logo-icon' />
        </Col>
        <Col center>
          <IconButton
            name='close'
            onClick={() => history.push('/')}
            className='u-align-right'
            dataAttrs={{ stat: '1' }}
          />
        </Col>
      </Row>
      <Row className='u-mb-2'>
        <AuthTitle />
        <Col offsetMd={1} md={4} className='u-border-left-md'>
          <AuthForm />
        </Col>
      </Row>
    </Container>
  )
}
