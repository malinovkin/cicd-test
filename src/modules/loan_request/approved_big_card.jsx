import React from 'react'
// import P from 'prop-types'

import { Col, Container, Icon, Button, Card, Row, Text, Title } from '../../base'
import {
  formatDayCount,
  formatMoney,
  formatISODateToTextFieldWithMonth
} from '../../helpers/format_helpers'
import { calcTotalLoanAmount } from '../../domain'
import { LoanParam } from './shared'

export function ApprovedBigCard ({
  suggestion,
  onGetSuggestionDetails,
  onGetMoney,
  loanRequestId,
  suggestionId
}) {
  return (
    <Card borderType='success' simple className='u-mb-2'>
      <Row className='u-px-4-md u-mt-2 u-mb-3'>
        <Col md={4} className='u-mb-0-md u-mb-2'>
          <LoanParam
            caption='Сумма займа'
            value={
              <Title sm={1} size={3}>
                {formatMoney(suggestion.amount)}
              </Title>
            }
          />
        </Col>
        <Col md={8}>
          <Container>
            <Row>
              <Col size={12} sm='auto' className='u-mr-4 u-mb-2 u-p-0'>
                <LoanParam
                  caption='Кредитор'
                  value={
                    <Icon
                      name={`${suggestion.mfiAlias}-logo-icon`}
                      pos='left'
                    />
                  }
                />
              </Col>
              <Col size='auto' className='u-mr-4 u-mb-2 u-p-0'>
                <LoanParam
                  caption='Срок займа'
                  value={
                    <Title size={4}>
                      {suggestion.term} {formatDayCount(suggestion.term)}
                    </Title>
                  }
                />
              </Col>
              <Col size='auto' className='u-mb-2 u-p-0'>
                <LoanParam
                  caption='К возврату'
                  value={
                    <Title size={4}>
                      {formatMoney(
                        calcTotalLoanAmount(
                          suggestion.amount,
                          suggestion.overpayment
                        )
                      )}
                    </Title>
                  }
                />
              </Col>
            </Row>
          </Container>
        </Col>
      </Row>
      <Row className='u-px-4-md'>
        <Col md={4}>
          <Button
            onClick={() => onGetMoney(suggestion.mfiAlias, suggestion.mfiUrl)}
            maxWidth
            className='u-mb-2'
          >
            Получить деньги
          </Button>
        </Col>
        <Col md={4}>
          <Button
            color='secondary'
            onClick={() => onGetSuggestionDetails(loanRequestId, suggestionId)}
            maxWidth
            className='u-mb-2'
          >
            Подробнее
          </Button>
        </Col>
        <Col
          md={4}
          className='u-text-align-center u-text-align-left-md u-mt-0 u-mt-2-md u-mb-2 u-mb-0-md'
        >
          <div className='bullet-block u-flex-center u-flex-left-md'>
            <span className='bullet green big u-mr-1' />
            <Text bold centerSm inline className='u-pl-1' color='secondary'>
              Одобрено до{' '}
              {formatISODateToTextFieldWithMonth(suggestion.expirationDate)}
            </Text>
          </div>
        </Col>
      </Row>
    </Card>
  )
}

// ApprovedCard.propTypes = {
//   onGetMoney: P.func.isRequired,
//   onGetSuggestionDetails: P.func.isRequired
// }
