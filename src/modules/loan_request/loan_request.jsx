import React, { useContext, useEffect } from 'react'

import * as Format from '../../helpers/format_helpers'
import {
  Container,
  ExpansionPanel,
  Row,
  Col,
  Icon,
  Title,
  Text,
  Link,
  IconButton,
  ContentWithFooter,
  ContentBeforeFooter,
  Footer
} from '../../base'
import { ApprovedBigCard } from './approved_big_card'
import { NotActiveCard } from './not_active_card'
import { AlternativeRejectedLoan } from './alternative_rejected_loan'
import { LoadingDots } from '../loan_request_list/loadingDots'
import { StoreContext, RouterContext } from '../../context'
import { Actions } from '../../modules/state'
import { Application } from './application/application'
import { ListLoader } from '../loan_request_list/list_loader'
import { ModalType } from '../modals/modal_type'
import { AccountNavbar } from '../shared/account_navbar'
import { FilkosPixel } from '../shared/filkos_pixel'
import { Support } from '../shared/support'
import { NewLoanRequestCard } from '../loan_request_list/new_loan_request_card'
import { API } from '../../api'
import { scrollUp } from '../../helpers/dom_helpers'
import { changePage } from '../../metrics'

export function FinishedList ({
  suggestions,
  onGetSuggestionDetails,
  onGetMoney,
  id: loanRequestId,
  onClose
}) {
  useEffect(() => {
    changePage('loanRequestActive')
    scrollUp()
  }, [])

  function renderSuggestion () {
    const approvedSuggestion = suggestions.filter(
      suggestion => suggestion.status === 'available'
    )

    const notActiveSuggestion = suggestions.filter(
      suggestion => suggestion.status !== 'available'
    )

    return (
      <React.Fragment>
        <ExpansionPanel
          caption='Можно воспользоваться'
          captionCenter
          captionLeftMd
        >
          {approvedSuggestion.map(suggestion => (
            <ApprovedBigCard
              key={suggestion.id}
              suggestion={suggestion}
              onGetSuggestionDetails={onGetSuggestionDetails}
              onGetMoney={onGetMoney}
              loanRequestId={loanRequestId}
              suggestionId={suggestion.id}
            />
          ))}
        </ExpansionPanel>

        {notActiveSuggestion.length > 0 ? (
          <ExpansionPanel
            className='u-mt-6'
            caption='Нельзя воспользоваться'
            captionCenter
            captionLeftMd
          >
            {notActiveSuggestion.map(suggestion => (
              <NotActiveCard
                key={suggestion.id}
                suggestion={suggestion}
                onGetSuggestionDetails={onGetSuggestionDetails}
                loanRequestId={loanRequestId}
                suggestionId={suggestion.id}
              />
            ))}
          </ExpansionPanel>
        ) : null}
      </React.Fragment>
    )
  }

  return (
    <Container className='u-pt-4 u-pb-2'>
      <Row className='u-mt-3 u-mt-4-sm u-mb-2'>
        <Col offset={2} center className='u-text-align-center'>
          <Icon name='smile-glad-icon' />
        </Col>
        <Col size={2} start className='u-text-align-center'>
          <IconButton
            className='u-align-right'
            size='md'
            name='close'
            onClick={() => onClose()}
          />
        </Col>
      </Row>

      <Row className='u-mb-2'>
        <Col>
          <Title size={3} sm={2} center>
            Подобранные займы
          </Title>
        </Col>
      </Row>

      <Row className='u-mb-4 u-mb-6-sm'>
        <Col center>
          <Title size={4} regular center>
            Вы можете воспользоваться одним или несколькими займами.
          </Title>
        </Col>
      </Row>

      {renderSuggestion()}
    </Container>
  )
}

export function ProcessingList ({
  suggestions,
  onGetSuggestionDetails,
  onGetMoney,
  isStartProcessing,
  isProcessing,
  isLongProcess,
  countLoanRequestItem,
  id: loanRequestId,
  onClose,
  returnToApplication,
  onCheckLoanRequestStatus
}) {
  useEffect(() => {
    scrollUp()
    changePage('loanLoading')
    let intervalId = window.setInterval(onCheckLoanRequestStatus, 5000)
    return () => window.clearInterval(intervalId)
  }, [])

  function renderNotification () {
    if (isStartProcessing) {
      return (
        <Title size={4} regular center>
          Мы уже подбираем для вас займы. Заполните{' '}
          <Link onClick={returnToApplication}>еще один шаг формы</Link>, чтобы
          получить больше займов
        </Title>
      )
    }
    if (isProcessing && isLongProcess && countLoanRequestItem === 0) {
      return (
        <React.Fragment>
          <Title size={4} regular center className='u-mb-2'>
            Мы продолжаем подбирать подходящие займы. Это может занять еще 20-30
            минут. Найденные займы будут появляться прямо на этой странице.
          </Title>
          <Title size={4} regular center>
            Не волнуйтесь, как только все будет готово, мы отправим вам
            уведомление по SMS.
          </Title>
        </React.Fragment>
      )
    }
    if (isProcessing && countLoanRequestItem === 0) {
      return (
        <Title size={4} regular center>
          Пожалуйста, подождите. Подбираем для вас подходящие займы. Найденные
          займы будут появляться прямо на этой странице.
        </Title>
      )
    }

    return (
      <Title size={4} regular center>
        Мы продолжаем подбирать подходящие займы. Это может занять еще 20-30
        минут.
      </Title>
    )
  }

  return (
    <Container className='u-pt-4 u-pb-2'>
      <Row className='u-mb-2'>
        <Col>
          <Container>
            <Row className='u-mb-2 u-mb-3-md'>
              <Col size={3} start className='u-text-align-center'>
                {isStartProcessing ? (
                  <div onClick={returnToApplication}>
                    <IconButton
                      className='u-align-left'
                      size='md'
                      name='arrow-left'
                    />
                    <Text
                      style={{
                        paddingTop: '6px',
                        minWidth: '73px',
                        cursor: 'pointer'
                      }}
                    >
                      Вернуться к форме
                    </Text>
                  </div>
                ) : (
                  <Col size={3} />
                )}
              </Col>
              <Col size={6}>
                {isProcessing ? <LoadingDots /> : null}
                {/* <LoadingDots /> */}
              </Col>
              <Col size={3} start className='u-text-align-center'>
                <IconButton
                  className='u-align-right'
                  size='md'
                  name='close'
                  onClick={() => onClose()}
                />
              </Col>
            </Row>
            <Row className='u-mb-2'>
              <Col md={8} offsetMd={2}>
                <Title
                  center
                  size={3}
                  md={2}
                  className='u-text-align-center u-text-align-left-md'
                >
                  Подбор займов
                </Title>
              </Col>
            </Row>
            <Row className='u-mb-6'>
              <Col md={6} offsetMd={3}>
                {renderNotification()}
              </Col>
            </Row>
            {countLoanRequestItem !== 0 ? (
              <Row className='u-mb-3'>
                <Col>
                  <Title center size={3}>
                    {Format.formatPickedUpSuggestion(countLoanRequestItem)}
                  </Title>
                </Col>
              </Row>
            ) : null}
          </Container>
        </Col>
      </Row>
      {suggestions.map(suggestion => (
        <ApprovedBigCard
          key={suggestion.id}
          suggestion={suggestion}
          onGetSuggestionDetails={onGetSuggestionDetails}
          onGetMoney={onGetMoney}
          loanRequestId={loanRequestId}
          suggestionId={suggestion.id}
        />
      ))}
    </Container>
  )
}

export function ArchiveList ({
  suggestions,
  creationDate,
  onGetSuggestionDetails,
  onGetMoney,
  countLoanRequestItem,
  id: loanRequestId,
  onClose
}) {
  useEffect(() => {
    changePage('loanRequestErlier')
    scrollUp()
  }, [])

  return (
    <Container className='u-pt-4 u-pb-2'>
      <Row className='u-mt-3 u-mt-4-sm u-mb-2'>
        <Col offset={2} center className='u-text-align-center'>
          <Icon name='smile-neutral-icon' />
        </Col>
        <Col size={2} start className='u-text-align-center'>
          <IconButton
            className='u-align-right'
            size='md'
            name='close'
            onClick={() => onClose()}
          />
        </Col>
      </Row>
      <Row className='u-mb-6'>
        <Col>
          <Title size={3} sm={2} center>
            {countLoanRequestItem === 1
              ? 'Подобранный займ'
              : 'Подобранные займы'}
          </Title>
        </Col>
      </Row>
      {countLoanRequestItem <= 1 ? null : (
        <Row className='u-mb-4 u-mb-6-sm'>
          <Col center md={6} offsetMd={3}>
            <Title size={4} regular center>
              {Format.formatISODateToTextField(creationDate)} вам было одобрено{' '}
              <span style={{ fontWeight: 'bold' }}>
                {countLoanRequestItem}{' '}
                {Format.formatSuggestionCount(countLoanRequestItem)}
              </span>
              . Вы можете ознакомиться с условиями каждого из них.
            </Title>
          </Col>
        </Row>
      )}
      {suggestions.map(suggestion => (
        <NotActiveCard
          key={suggestion.id}
          suggestion={suggestion}
          onGetSuggestionDetails={onGetSuggestionDetails}
          onGetMoney={onGetMoney}
          loanRequestId={loanRequestId}
          suggestionId={suggestion.id}
        />
      ))}
    </Container>
  )
}

export function LoanRequest ({ match }) {
  const { state, dispatch } = useContext(StoreContext)
  const { history } = useContext(RouterContext)
  const {
    id,
    isLongProcess,
    status,
    onApplication,
    countLoanRequestItem,
    step
  } = state.loanRequest
  let { suggestions } = state.loanRequest
  suggestions = suggestions || []

  useEffect(() => {
    scrollUp()
    dispatch(Actions.fetchLoanRequest(match.id))

    if (!suggestions && (step > 1 || !step)) {
      dispatch(Actions.fetchLoanRequestSuggestion(match.id))
    }
    if (!state.permission) {
      dispatch(Actions.fetchUser())
    }
    return () => dispatch(Actions.loanRequestUnmount())
  }, [])

  useEffect(
    () => {
      if (countLoanRequestItem) {
        dispatch(Actions.fetchLoanRequestSuggestion(match.id))
      }
    },
    [countLoanRequestItem]
  )

  function isVisibleDataLoading () {
    return !state.loanRequest.id || !state.permissions
  }

  function getRequestStatus () {
    if (
      (status === 'needInfo' && step === 1) ||
      (countLoanRequestItem === 0 && status === 'needInfo') ||
      (onApplication && (status === 'needInfo' || status === 'finished'))
    ) {
      return 'notStarted'
    }

    if (status === 'needInfo' && (step === 2 || step === 3) && !onApplication) {
      return 'startProcessing'
    }

    if (status === 'inProgress') {
      return 'inProgress'
    }

    if (status === 'finished' && countLoanRequestItem !== 0) {
      return 'finished'
    }

    if (
      (status === 'finished' || status === 'expired') &&
      countLoanRequestItem === 0
    ) {
      return 'alternateOffers'
    }

    if (status === 'expired') {
      return 'expired'
    }
  }

  function onCheckLoanRequestStatus () {
    return dispatch(Actions.checkLoanRequestStatus(match.id))
  }

  function returnToApplication () {
    dispatch(Actions.goToApplication())
  }
  function returnToSuggestionList () {
    dispatch(Actions.goToList())
  }

  function onGetSuggestionDetails (loanRequestId, suggestionId) {
    return history.push(`/loan-requests/${loanRequestId}/${suggestionId}`)
  }

  function onClose () {
    return history.push('/loanRequest')
  }

  function onGetMoney (creditor, creditorUrl) {
    dispatch(
      Actions.openModal(ModalType.RedirectToCreditor, {
        creditor,
        url: creditorUrl
      })
    )
  }

  function renderCalc () {
    if (
      isVisibleDataLoading() ||
      (state.loanRequest.amount && state.loanRequest.term)
    ) {
      return null
    }

    const withoutTerm =
      (!state.loanRequest.amount || !state.loanRequest.term) &&
      !state.newLoanOptions

    if (withoutTerm) {
      dispatch(Actions.fetchNewLoanOptions())
    }

    function onSaveCondition ({ amount, term }) {
      dispatch(Actions.setWantedLoanConditions(amount, term))
      API.saveLoanConditions(id, amount, term)
    }

    return !withoutTerm ? (
      <Container className='u-pt-4 u-pb-2'>
        <NewLoanRequestCard
          {...state.newLoanOptions}
          onSubmit={onSaveCondition}
          isSubmitPending={state.pendings.createLoanRequest}
        />
      </Container>
    ) : (
      <ListLoader />
    )
  }

  function renderLoanRequest () {
    if (
      isVisibleDataLoading() ||
      (!state.loanRequest.amount || !state.loanRequest.term)
    ) {
      return null
    }

    switch (getRequestStatus()) {
      case 'notStarted':
        return (
          <Application
            onCheckLoanRequestStatus={onCheckLoanRequestStatus}
            returnToSuggestionList={returnToSuggestionList}
          />
        )
      case 'startProcessing':
        return (
          <React.Fragment>
            <FilkosPixel id={state.loanRequest.id} />
            <ProcessingList
              {...state.loanRequest}
              isStartProcessing
              onGetSuggestionDetails={onGetSuggestionDetails}
              onGetMoney={onGetMoney}
              returnToApplication={returnToApplication}
              onClose={onClose}
              onCheckLoanRequestStatus={onCheckLoanRequestStatus}
            />
          </React.Fragment>
        )
      case 'inProgress':
        return (
          <React.Fragment>
            <FilkosPixel id={state.loanRequest.id} />
            <ProcessingList
              isLongProcess={isLongProcess}
              {...state.loanRequest}
              isProcessing
              onGetSuggestionDetails={onGetSuggestionDetails}
              onGetMoney={onGetMoney}
              onClose={onClose}
              onCheckLoanRequestStatus={onCheckLoanRequestStatus}
            />
          </React.Fragment>
        )
      case 'finished':
        return (
          <FinishedList
            {...state.loanRequest}
            onGetSuggestionDetails={onGetSuggestionDetails}
            onGetMoney={onGetMoney}
            onClose={onClose}
          />
        )
      case 'alternateOffers':
        return (
          <AlternativeRejectedLoan {...state.loanRequest} onClose={onClose} />
        )
      case 'expired':
        return (
          <ArchiveList
            {...state.loanRequest}
            onGetSuggestionDetails={onGetSuggestionDetails}
            onGetMoney={onGetMoney}
            onClose={onClose}
          />
        )
      default:
        return null
    }
  }

  return (
    <ContentWithFooter>
      <ContentBeforeFooter>
        <AccountNavbar />
        {renderCalc()}
        {renderLoanRequest()}
        {isVisibleDataLoading() ? <ListLoader /> : null}
      </ContentBeforeFooter>
      <Footer>
        <Container>
          <Row className='u-mb-4 u-pt-4'>
            <Col offsetMd={3} md={6}>
              <Support phone={process.env.SUPPORT_PHONE} />
            </Col>
          </Row>
        </Container>
      </Footer>
    </ContentWithFooter>
  )
}
