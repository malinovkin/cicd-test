import React, { useEffect, useContext } from 'react'
import P from 'prop-types'
import * as R from 'ramda'
import { API } from '../../api'
import { StoreContext, RouterContext } from '../../context'
import { Actions } from '../../modules/state'
import {
  Col,
  Container,
  Icon,
  IconButton,
  Button,
  Row,
  Text,
  Title,
  ExpansionPanel,
  Card,
  Link
} from '../../base'
import { LoanParam } from './shared'
import { formatDayCount, formatMoney } from '../../helpers/format_helpers'
import { NewLoanRequestCard } from '../loan_request_list/new_loan_request_card'
import { ListLoader } from '../loan_request_list/list_loader' // ПОменять на SHared
import { scrollUp } from '../../helpers/dom_helpers'
import { changePage } from '../../metrics'

function Offer ({ info, className }) {
  let sumTitle

  if (R.isNil(info.newSumFrom)) {
    sumTitle = formatMoney(info.newSumTo)
  } else if (R.isNil(info.newSumTo)) {
    sumTitle = formatMoney(info.newSumFrom)
  } else {
    sumTitle =
      `${formatMoney(info.newSumFrom, { withoutCurrency: true })} - ` +
      `${formatMoney(info.newSumTo)}`
  }

  function goToMFI () {
    window.open(info.trackingLink, '_blank')
  }

  function isPreApproved (link) {
    const regex = /^(http|https):\/\//
    return !regex.test(link)
  }

  return (
    <Card
      borderType={isPreApproved(info.logo) ? 'accentLeft' : 'none'}
      className={className}
      simple
    >
      <Row className='u-pl-2-md u-pr-4-md'>
        <Col md={4}>
          <Row className='u-mt-2 u-mb-3'>
            <Col>
              <Text inline='block' className='u-mb-1' color='secondary'>
                Кредитор
              </Text>
              <div>
                {isPreApproved(info.logo) ? (
                  <Icon name={`${info.logo}-large-logo-icon`} />
                ) : (
                  <img src={info.logo} alt='Логотип МФО' />
                )}
              </div>
            </Col>
          </Row>
          <Row className='u-hidden-lt-md'>
            <Col md={11}>
              <Button
                color='primary'
                onClick={goToMFI}
                maxWidth
                className='u-mb-2'
              >
                Подать заявку
              </Button>
            </Col>
          </Row>
        </Col>

        <Col md={8}>
          <Row className='u-mb-0 u-mb-3-md'>
            <Col>
              <Container className='u-mt-2-md'>
                <Row>
                  <Col size='auto' className='u-mr-4 u-mb-2 u-p-0'>
                    <LoanParam
                      caption='Сумма займа'
                      value={<Title size={4}>{sumTitle}</Title>}
                    />
                  </Col>

                  <Col size='auto' className='u-mr-4 u-mb-2 u-p-0'>
                    <LoanParam
                      caption='Срок займа до'
                      value={
                        <Title size={4}>
                          {info.newTermTo} {formatDayCount(info.newTermTo)}
                        </Title>
                      }
                    />
                  </Col>

                  <Col size='auto' className='u-mb-2 u-p-0'>
                    <LoanParam
                      caption='Ставка в день'
                      value={<Title size={4}>{info.newPercentFrom} %</Title>}
                    />
                  </Col>
                </Row>
              </Container>
            </Col>
          </Row>
          {isPreApproved(info.logo) ? (
            <Row>
              <Col className='u-text-align-left u-mt-0 u-mt-2-md u-mb-3 u-mb-0-md'>
                <div className='bullet-block'>
                  <span className='bullet accent big u-mr-1' />
                  <Text className='u-pl-1 u-inline-block'>
                    Вы прошли проверку на предварительное одобрение
                  </Text>
                </div>
              </Col>
            </Row>
          ) : null}
        </Col>

        <Col className='u-hidden-mt-md'>
          <Button color='primary' onClick={goToMFI} maxWidth className='u-mb-2'>
            Подать заявку
          </Button>
        </Col>
      </Row>
    </Card>
  )
}

Offer.propTypes = {
  info: P.shape({
    newTermTo: P.number.isRequired,
    newPercentFrom: P.number.isRequired,
    logo: P.string.isRequired,
    trackingLink: P.string.isRequired,
    newSumFrom: P.number,
    newSumTo: P.number
  }).isRequired,
  className: P.string
}

Offer.defaultProps = {
  className: null,
  newSumFrom: null,
  newSumTo: null
}

export function AlternativeRejectedLoan ({ id, onClose }) {
  const { state, dispatch } = useContext(StoreContext)
  const { history } = useContext(RouterContext)
  useEffect(() => {
    scrollUp()
    dispatch(Actions.fetchAlternativeOffers(id))
    changePage('rejected')

    if (!state.newLoanOptions) {
      dispatch(Actions.fetchNewLoanOptions())
    }

    if (!state.permission) {
      dispatch(Actions.fetchUser())
    }
  }, [])

  function onCreateLoanRequest ({ amount, term }) {
    dispatch(Actions.setWantedLoanConditions(amount, term))
    dispatch(Actions.createLoanRequest(state.leadProviderParams.common)).then(
      ({ value }) => {
        API.saveLoanConditions(value.id, amount, term)
        dispatch(Actions.fetchLoanRequest(value.id))
        history.push(`/loan-requests/${value.id}`)
      }
    )
  }

  function isVisibleDataLoading () {
    return (
      !state.newLoanOptions ||
      !state.loanRequest.alternativeOffers ||
      !state.permissions ||
      !state.loanRequest
    )
  }

  function renderNewLoanRequest () {
    if (isVisibleDataLoading()) {
      return null
    }

    if (state.permissions.newLoan && state.loanRequest.status === 'expired') {
      return (
        <NewLoanRequestCard
          {...state.newLoanOptions}
          caption='Заявка на новый займ'
          onSubmit={onCreateLoanRequest}
          isSubmitPending={
            state.pendings.createLoanRequest || state.pendings.fetchLoanRequest
          }
        />
      )
    }
  }

  function renderAlternativeRejectedLoan () {
    if (isVisibleDataLoading()) {
      return null
    }

    if (state.loanRequest.status === 'finished') {
      return (
        <React.Fragment>
          <Row className='u-mb-4'>
            <Col offset={2} size={8}>
              <Title size={4} sm={3} center>
                Вы можете отправить заявку на займ у наших партнеров
              </Title>
            </Col>
          </Row>
          {R.keys(offers).map(title => (
            <ExpansionPanel
              caption={title.slice(0, title.length - 1)}
              key={title}
              className='u-mb-4'
            >
              {offers[title].map((offer, i) => (
                <Offer info={offer} key={i} className='u-mb-2' />
              ))}
            </ExpansionPanel>
          ))}
        </React.Fragment>
      )
    }
  }

  const offers = state.loanRequest.alternativeOffers

  return (
    <Container>
      <Row className='u-mt-3 u-mt-4-sm u-mb-2'>
        <Col offset={2} center className='u-text-align-center'>
          <Icon name='smile-warning-icon' />
        </Col>
        <Col size={2} start className='u-text-align-center'>
          <IconButton
            className='u-align-right'
            size='md'
            name='close'
            onClick={() => onClose()}
          />
        </Col>
      </Row>

      <Row className='u-mb-4'>
        <Col>
          <Title size={4} sm={3} center>
            {state.permissions.newLoan && state.loanRequest.status === 'expired'
              ? 'Вы можете отправить заявку на займ еще раз'
              : 'Подходящего займа пока не нашлось'}
          </Title>
        </Col>
      </Row>

      {state.permissions.newLoan && state.loanRequest.status !== 'expired' ? (
        <Row className='u-mb-4'>
          <Col offset={2} size={8}>
            <Text center>
              Нам не удалось найти для вас подходящий займ среди подключенных к
              нам МФО.
            </Text>
          </Col>
        </Row>
      ) : null}

      {renderAlternativeRejectedLoan()}
      {renderNewLoanRequest()}
      {isVisibleDataLoading() ? <ListLoader /> : null}

      <Row className='u-mt-2 u-mt-4-sm u-mb-4'>
        <Col center className='u-text-align-center'>
          <Title size={3} center>
            Рекомендуем
          </Title>
        </Col>
      </Row>

      <Card simple className='u-mb-6'>
        <Row>
          <Col size={4} sm={3} md={2} className='u-text-align-center'>
            <Icon name='credit-rating-icon' />
          </Col>
          <Col size={8} sm={9} md={7}>
            <Text type='subtitle' bold left className='u-hidden-lt-sm'>
              Проверьте свой кредитный рейтинг
            </Text>
            <Text type='body-1' bold left className='u-hidden-mt-sm'>
              Проверьте свой кредитный рейтинг
            </Text>
            <Text left className='u-mt-1 u-hidden-lt-sm'>
              Возможная причина отказов — низкий кредитный рейтинг.
            </Text>
            <Text left className='u-hidden-lt-sm'>
              Проверьте его и получите 4 рекомендации по его улучшению
            </Text>
          </Col>
          <Col sm={12} md={3} className='u-mt-2 u-mt-0-md'>
            <Link
              onClick={() =>
                window.open(
                  'http://ki.mycreditinfo.ru/rating?utm_source=finspin.ru&utm_medium=referral&utm_campaign=Rejected_page_suggestion',
                  '_blank'
                )
              }
              className='u-border-bottom-none'
            >
              <Button maxWidth color='secondary' size='sm'>
                Подробнее
              </Button>
            </Link>
            <Text center className='u-mt-2 u-hidden-lt-md'>
              <Link
                onClick={() =>
                  window.open(
                    'https://mycreditinfo.ru/Report/ReportSample?serviceId=6',
                    '_blank'
                  )
                }
              >
                Пример отчета
              </Link>
            </Text>
          </Col>
        </Row>
      </Card>
    </Container>
  )
}

AlternativeRejectedLoan.propTypes = {
  onClose: P.func.isRequired,
  offers: P.object
}

AlternativeRejectedLoan.defaultProps = {
  offers: null
}
