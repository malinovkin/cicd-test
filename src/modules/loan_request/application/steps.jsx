import * as R from 'ramda'

import {Fields} from './fields'

function configure (step, fixedSteps, config) {
  return config.filter(x => !R.isNil(x))
    .map(x => {
      const [name, section] = x
      const field = Fields[name]
      const isFixed = R.includes(name, fixedSteps)
      return R.mergeRight(field, {
        fixed: field.fixable && isFixed,
        section,
        name
      })
    })
}

export const steps = {
  1: (fixedSteps) => ({
    title: {percent: 0, plusPercent: 30, stepName: 'первый'},
    fields: (values) => configure(1, fixedSteps,
      [
        ['lastName', 'Данные паспорта'],
        ['firstName'],
        ['middleName'],
        ['birthDate'],
        ['passportSN'],
        ['passportIssuedDate'],
        ['gender'],
        ['email', 'Электронная почта']
      ])
  }),

  2: (fixedSteps) => ({
    title: {percent: 30, plusPercent: 14, stepName: 'следующий'},
    fields: (values) => configure(2, fixedSteps,
      [
        ['birthPlace', 'Данные паспорта'],
        ['passportIssuedBy'],
        ['departmentCode'],
        ['registrationAddress', 'Адрес регистрации'],
        ['registrationCoincidesWithTheFact', 'Адрес проживания'],
        values.registrationCoincidesWithTheFact ? null : ['factAddress']
      ])
  }),

  3: (fixedSteps) => ({
    title: {percent: 44, plusPercent: 23, stepName: 'последний'},
    fields: (values) => configure(3, fixedSteps,
      [
        ['jobType', 'Работа'],
        ['jobPosition'],
        ['jobIncome'],
        ['jobPhone'],
        ['jobExperience'],
        ['jobOrgName', 'Информация о работе'],
        ['jobEmployees'],
        ['jobAddress'],
        ['education', 'Образование'],
        ['marriageStatus', 'Семья'],
        ['amountOfChildren'],
        ['contactName', 'Доверенное лицо'],
        ['contactType'],
        ['contactPhone']
      ])
  })
}
