import React from 'react'
import P from 'prop-types'
import {Col, Container, Row, Title} from '../../../base/index'
import {LoanCalculator} from '../../shared/loan_calculator'

export const DefaultValues = {
  amount: P.number,
  term: P.number
}

export class LoanParams extends React.Component {
  static propTypes = {
    minTerm: P.number.isRequired,
    maxTerm: P.number.isRequired,
    minAmount: P.number.isRequired,
    maxAmount: P.number.isRequired,
    amountStep: P.number.isRequired,
    termStep: P.number.isRequired,
    rate: P.number.isRequired,
    defaultValues: P.shape(DefaultValues).isRequired,
    onSubmit: P.func.isRequired,
    onFieldChange: P.func.isRequired
  }

  handleSubmit = (values) => {
    this.props.onSubmit(values)
  }

  render () {
    return (
      <Container>

        <Row className='u-mb-4'>
          <Col>
            <Title size={4} sm={3} center>
              Выберите сумму и срок
            </Title>
          </Col>
        </Row>

        <Row className='u-mb-4'>
          <Col offsetMd={2} md={8}>
            <LoanCalculator
              defaultValues={this.props.defaultValues}
              onSubmit={this.handleSubmit}
              minTerm={this.props.minTerm}
              maxTerm={this.props.maxTerm}
              minAmount={this.props.minAmount}
              maxAmount={this.props.maxAmount}
              amountStep={this.props.amountStep}
              termStep={this.props.termStep}
              rate={this.props.rate}
              isSubmitPending={false}
              onFieldChange={this.props.onFieldChange}
              buttonCaption='Продолжить'
            />
          </Col>
        </Row>

      </Container>
    )
  }
}
