import * as R from 'ramda'
import * as Validation from '../../../validation'
import {
  searchRegions,
  searchCities,
  searchStreets,
  searchHouses,
  searchFirstNames,
  searchLastNames,
  searchMiddleNames
} from '../../../dadata'
import * as Domain from '../../../domain'
import * as Format from '../../../helpers/format_helpers'

function getNameValue (obj) {
  return R.is(Object, obj) ? obj.value : obj
}

function autoselectGender (value, field, rawValue, getValue, setValue) {
  if (rawValue && rawValue.gender) {
    if (getValue('gender') !== rawValue.gender) {
      setValue('gender', rawValue.gender)
    }
  }
}

const textDefaults = {
  type: 'textField',
  parseValue: R.identity,
  formatValue: R.identity,
  widgetOptions: {
    type: 'text'
  }
}

const dateDefaults = {
  type: 'textField',
  parseValue: Format.parseTextFieldDate,
  formatValue: Format.formatISODateToTextField,
  validate: Validation.isTextFieldDate,
  widgetOptions: {
    type: 'text',
    mask: '99.99.9999'
  }
}

const phoneDefaults = {
  type: 'textField',
  parseValue: Format.parsePhone,
  formatValue: Format.formatPhone,
  validate: Validation.isPhone,
  widgetOptions: {
    type: 'tel',
    mask: '+7 (999) 999-99-99'
  }
}

const selectDefaults = {
  type: 'select',
  parseValue: value => (R.isNil(value) ? value : value.value),
  widgetOptions: {
    onGetOptionCaption: o => o.caption,
    onGetValueCaption: o => o.caption,
    onGetOptionId: o => o.value
  }
}

const nameDefaults = {
  type: 'autocomplete',
  formatValue: str => ({ value: str }),
  parseValue: getNameValue,
  widgetOptions: {
    onGetSuggestionCaption: R.prop('value'),
    onGetValueCaption: getNameValue,
    onGetSuggestionId: R.prop('value'),
    allowCustomValue: true,
    delay: 300
  }
}

const addressDefaults = {
  type: 'address',
  parseValue: (value = {}) => ({
    ...value,
    withoutFlat: value.withoutFlat || false
  }),
  formatValue: R.identity,
  validate: (value = {}, options) => ({
    region: Validation.isEmpty(value.region),
    city: Validation.isEmpty(value.city) && Validation.isEmpty(value.settlement),
    street: Validation.isEmpty(value.street),
    house: Validation.isEmpty(value.house),
    flat: value.withoutFlat ? Validation.noError
      : Validation.isFlat(value.flat, options)
  }),
  widgetOptions: {
    searchRegion: searchRegions,
    searchCity: searchCities,
    searchStreet: searchStreets,
    searchHouse: searchHouses
  }
}

export const Fields = {
  lastName: R.mergeDeepRight(nameDefaults, {
    validate: (value, options) =>
      Validation.isLastName(getNameValue(value), options),
    fixable: true,
    onChange: autoselectGender,
    widgetOptions: {
      label: 'Фамилия',
      analyticsLabel: 'Фамилия',
      onGetSuggestions: searchLastNames
    }
  }),

  firstName: R.mergeDeepRight(nameDefaults, {
    validate: (value, options) =>
      Validation.isFirstName(getNameValue(value), options),
    fixable: true,
    onChange: autoselectGender,
    widgetOptions: {
      label: 'Имя',
      analyticsLabel: 'Имя',
      onGetSuggestions: searchFirstNames
    }
  }),

  middleName: R.mergeDeepRight(nameDefaults, {
    validate: (value, options) =>
      Validation.isMiddleName(getNameValue(value), options),
    fixable: true,
    onChange: autoselectGender,
    widgetOptions: {
      label: 'Отчество',
      analyticsLabel: 'Отчество',
      onGetSuggestions: searchMiddleNames
    }
  }),

  birthDate: R.mergeDeepRight(dateDefaults, {
    fixable: true,
    widgetOptions: {
      label: 'Дата рождения',
      analyticsLabel: 'Дата рождения',
      isNarrow: true
    }
  }),

  passportSN: R.mergeDeepRight(textDefaults, {
    validate: Validation.isPassportSN,
    fixable: true,
    widgetOptions: {
      label: 'Серия и номер',
      analyticsLabel: 'Серия и номер паспорта',
      mask: '9999 999999',
      isNarrow: true
    }
  }),

  passportIssuedDate: R.mergeDeepRight(dateDefaults, {
    fixable: true,
    widgetOptions: {
      label: 'Дата выдачи',
      analyticsLabel: 'Дата выдачи паспорта',
      isNarrow: true
    }
  }),

  email: R.mergeDeepRight(textDefaults, {
    validate: Validation.isEmail,
    optional: true,
    widgetOptions: {
      type: 'email',
      label: 'Электронная почта',
      analyticsLabel: 'Email',
      placeholder: 'ivanov@mail.com'
    },
    bonus: {
      value: 4,
      caption: 'адрес электронной почты'
    }
  }),

  gender: {
    type: 'radioButtonGroup',
    defaultValue: 'male',
    parseValue: R.identity,
    formatValue: R.identity,
    fixable: true,
    widgetOptions: {
      label: 'Пол',
      inline: true,
      items: [
        { value: 'male', caption: 'Мужчина' },
        { value: 'female', caption: 'Женщина' }
      ]
    }
  },

  birthPlace: R.mergeDeepRight(textDefaults, {
    fixable: true,
    widgetOptions: {
      isMultiline: true,
      label: 'Место рождения',
      analyticsLabel: 'Место рождения'
    }
  }),

  passportIssuedBy: R.mergeDeepRight(textDefaults, {
    fixable: true,
    widgetOptions: {
      isMultiline: true,
      label: 'Кем выдан',
      analyticsLabel: 'Кем выдан',
      hint: 'Напишите так же, как в паспорте',
      hintAlways: true
    }
  }),

  departmentCode: R.mergeDeepRight(textDefaults, {
    validate: Validation.isDepartmentCode,
    fixable: true,
    widgetOptions: {
      label: 'Код подразделения',
      analyticsLabel: 'Код подразд.',
      mask: '999-999',
      isNarrow: true
    }
  }),

  registrationAddress: R.mergeDeepRight(addressDefaults, {
    widgetOptions: {
      analyticsLabel: 'Адрес регистрации'
    }
  }),

  registrationCoincidesWithTheFact: {
    type: 'checkbox',
    defaultValue: true,
    parseValue: R.identity,
    formatValue: R.identity,
    widgetOptions: {
      caption: 'Совпадает с адресом регистрации',
      analyticsLabel: 'Адр. рег. = факт. адр.'
    }
  },

  factAddress: R.mergeDeepRight(addressDefaults, {
    widgetOptions: {
      analyticsLabel: 'Факт. адрес'
    }
  }),

  jobOrgName: R.mergeDeepRight(textDefaults, {
    widgetOptions: {
      label: 'Название компании',
      analyticsLabel: 'Название компании'
    }
  }),

  jobType: R.mergeDeepRight(selectDefaults, {
    formatValue: value => R.find(R.whereEq({ value }), Domain.JobTypes),
    widgetOptions: {
      label: 'Сфера деятельности',
      items: Domain.JobTypes
    }
  }),

  jobPosition: R.mergeDeepRight(textDefaults, {
    widgetOptions: {
      label: 'Ваша должность',
      analyticsLabel: 'Ваша должность'
    }
  }),

  jobPhone: R.mergeDeepRight(phoneDefaults, {
    widgetOptions: {
      label: 'Рабочий телефон',
      analyticsLabel: 'Рабочий тел.'
    }
  }),

  jobIncome: R.mergeDeepRight(textDefaults, {
    widgetOptions: {
      label: 'Уровень зарплаты (в рублях)',
      analyticsLabel: 'Уровень зп'
    }
  }),

  jobAddress: R.mergeDeepRight(addressDefaults, {
    validate: (value = {}, options) => ({
      region: Validation.isEmpty(value.region),
      city:
        Validation.isEmpty(value.city) && Validation.isEmpty(value.settlement),
      street: Validation.isEmpty(value.street),
      house: Validation.isEmpty(value.house),
      flat: value.withoutFlat
        ? Validation.noError
        : Validation.isOffice(value.flat, options)
    }),
    widgetOptions: {
      analyticsLabel: 'Адрес работы',
      labels: {
        flat: 'Офис',
        withoutFlat: 'Номер офиса отсутсвует'
      }
    }
  }),

  jobExperience: R.mergeDeepRight(selectDefaults, {
    formatValue: value => R.find(R.whereEq({ value }), Domain.JobExperience),
    widgetOptions: {
      label: 'Стаж работы',
      analyticsLabel: 'Стаж работы',
      items: Domain.JobExperience
    }
  }),

  jobEmployees: R.mergeDeepRight(selectDefaults, {
    formatValue: value => R.find(R.whereEq({ value }), Domain.JobEmployees),
    widgetOptions: {
      label: 'Количество сотрудников',
      analyticsLabel: 'Количество сотрудников',
      items: Domain.JobEmployees
    }
  }),

  marriageStatus: R.mergeDeepRight(selectDefaults, {
    formatValue: value => R.find(R.whereEq({ value }), Domain.MarriageStatuses),
    widgetOptions: {
      label: 'Семейное положение',
      analyticsLabel: 'Семейное положение',
      items: Domain.MarriageStatuses
    }
  }),

  amountOfChildren: R.mergeDeepRight(selectDefaults, {
    formatValue: value => R.find(R.whereEq({ value }), Domain.ChildrenAmounts),
    widgetOptions: {
      label: 'Количество детей',
      analyticsLabel: 'Количество детей',
      items: Domain.ChildrenAmounts
    }
  }),

  contactName: R.mergeDeepRight(textDefaults, {
    validate: Validation.isFirstNameLastName,
    widgetOptions: {
      label: 'Фамилия и имя',
      analyticsLabel: 'Фамилия и имя довер. лица'
    }
  }),

  contactType: R.mergeDeepRight(selectDefaults, {
    formatValue: value => R.find(R.whereEq({ value }), Domain.ContactType),
    widgetOptions: {
      label: 'Кем приходится',
      analyticsLabel: 'Кем приходится довер. лицо',
      items: Domain.ContactType
    }
  }),

  contactPhone: R.mergeDeepRight(phoneDefaults, {
    widgetOptions: {
      label: 'Контактный телефон',
      analyticsLabel: 'Контактный тел. довер. лица'
    }
  }),

  education: R.mergeDeepRight(selectDefaults, {
    formatValue: value => R.find(R.whereEq({ value }), Domain.Education),
    widgetOptions: {
      label: 'Образование',
      analyticsLabel: 'Образование',
      items: Domain.Education
    }
  }),

  inn: R.mergeDeepRight(textDefaults, {
    validate: Validation.isINN,
    widgetOptions: {
      label: 'ИНН',
      analyticsLabel: 'ИНН',
      mask: '999999999999'
    }
  })
}
