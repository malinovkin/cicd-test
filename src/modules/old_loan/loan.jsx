import * as R from 'ramda'
import React from 'react'
import P from 'prop-types'
import {
  Col,
  Container,
  ContentBeforeFooter,
  ContentWithFooter,
  Footer,
  Row
} from '../../base'
import {ApprovedLoan} from './approved_loan'
import {OverdueLoan} from './overdue_loan'
import {AlternativeRejectedLoan} from './alternative_rejected_loan'
import {ServicedLoan} from './serviced_loan'
import {RepaidLoan} from './repaid_loan'
import {ExpiredLoan} from './expired_loan'
import {MoneyTransferLoan} from './money_transfer_loan'
import {ApplicationContainer} from './application/application_container'
import {LoanStatus} from '../../domain'
import {LoanProps} from '../shared/loan_props'
import {Support} from '../shared/support'
import {AccountNavbar} from '../shared/account_navbar'
import {FilkosPixel} from '../shared/filkos_pixel'
import {scrollUp} from '../../helpers/dom_helpers'
import {Processing} from './processing'

export class Loan extends React.Component {
  static propTypes = {
    componentDidMount: P.func.isRequired,
    componentWillUnmount: P.func.isRequired,
    onClose: P.func.isRequired,
    fetchLoan: P.func.isRequired,
    fetchPermissions: P.func.isRequired,
    fetchAlternativeOffers: P.func.isRequired,
    onShowAgreement: P.func.isRequired,
    id: P.string.isRequired,
    userPhone: P.string,
    loan: LoanProps,
    permissions: P.object,
    onGetPaymentMethods: P.func,
    onGetMoney: P.func,
    onCheckLoanStatus: P.func,
    createLoan: P.func,
    isCreateLoanPending: P.bool,
    alternativeOffers: P.object
  }

  static defaultProps = {
    userPhone: null,
    loan: null,
    permissions: null,
    onGetCreditHistory: null,
    onGetPaymentMethods: null,
    onGetMoney: null,
    onCheckLoanStatus: null,
    createLoan: null,
    isCreateLoanPending: false,
    alternativeOffers: null
  }

  componentDidMount () {
    this.props.componentDidMount()
    scrollUp()
    if (!this.props.loan) {
      this.props.fetchLoan(this.props.id)
    }
    this.props.fetchPermissions()
  }

  componentWillReceiveProps (nextProps) {
    if (this.props.id !== nextProps.id) {
      this.props.fetchLoan(nextProps.id)
    }
  }

  componentWillUnmount () {
    this.props.componentWillUnmount()
  }

  renderLoan () {
    if (!this.props.loan || !this.props.permissions) {
      return null
    }

    const defaultProps = {
      loan: this.props.loan,
      onClose: this.props.onClose
    }

    switch (this.props.loan.status) {
      case LoanStatus.Approved:
        return (
          <ApprovedLoan
            {...defaultProps}
            userPhone={this.props.userPhone}
            onGetMoney={this.props.onGetMoney}
            onShowAgreement={this.props.onShowAgreement}
          />
        )

      case LoanStatus.Rejected: {
        return (
          <AlternativeRejectedLoan
            {...defaultProps}
            fetchData={this.props.fetchAlternativeOffers}
            offers={this.props.alternativeOffers}
          />
        )
      }

      case LoanStatus.Overdue:
        return (
          <OverdueLoan
            {...defaultProps}
            onGetPaymentMethods={this.props.onGetPaymentMethods}
          />
        )

      case LoanStatus.Serviced:
        return (
          <ServicedLoan
            {...defaultProps}
            onGetPaymentMethods={this.props.onGetPaymentMethods}
          />
        )

      case LoanStatus.Repaid:
        return (
          <RepaidLoan
            {...defaultProps}
            onCreateLoan={this.props.createLoan}
            canCreateLoan={this.props.permissions.canCreateLoan}
            isCreateLoanPending={this.props.isCreateLoanPending}
          />
        )

      case LoanStatus.Expired:
        return (
          <ExpiredLoan
            {...defaultProps}
            onCreateLoan={this.props.createLoan}
            canCreateLoan={this.props.permissions.canCreateLoan}
            isCreateLoanPending={this.props.isCreateLoanPending}
          />
        )

      case LoanStatus.MoneyTransfer:
        return (
          <MoneyTransferLoan
            {...defaultProps}
          />
        )

      case LoanStatus.Processed:
        return (
          <div>
            <FilkosPixel id={this.props.id} />

            <Processing
              {...defaultProps}
              onCheckLoanStatus={this.props.onCheckLoanStatus}
            />
          </div>
        )

      case LoanStatus.Incomplete:
        return <ApplicationContainer id={this.props.id} />

      default:
        throw new Error('Unknown loan status: ', this.props.loan.status,
          ' You can only use these loan statuses: ', R.values(LoanStatus))
    }
  }

  render () {
    return (
      <ContentWithFooter>
        <ContentBeforeFooter>
          <AccountNavbar />
          {this.renderLoan()}
        </ContentBeforeFooter>

        <Footer>
          <Container>
            <Row className='u-mb-4 u-pt-4'>
              <Col offsetMd={3} md={6}>
                <Support phone={process.env.SUPPORT_PHONE} />
              </Col>
            </Row>
          </Container>
        </Footer>
      </ContentWithFooter>
    )
  }
}
