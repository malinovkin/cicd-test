import React from 'react'
import moment from 'moment'
import P from 'prop-types'
import {
  Button,
  Col,
  Container,
  Icon,
  IconButton,
  Panel,
  Row,
  Text,
  Title
} from '../../base'
import {
  formatDayCount,
  formatMoney,
  formatRemainingTime
} from '../../helpers/format_helpers'
import {
  calcRemainingDaysUntilOverdue,
  calcTotalLoanAmount
} from '../../domain'
import {LoanConditions} from './shared'
import {LoanProps} from '../shared/loan_props'

export function ServicedLoan (props) {
  const {loan} = props
  const totalAmount = calcTotalLoanAmount(loan.amount, loan.overpayment)

  return (
    <Container>

      <Row className='u-mt-2 u-mt-4-sm u-mb-4'>
        <Col offset={2} center className='u-text-align-center'>
          <Icon name='smile-glad-icon' />
        </Col>
        <Col size={2} start>
          <IconButton
            name='close'
            onClick={props.onClose}
            className='u-align-right'
            dataAttrs={{
              state: 'ev:click;ctx:Детали serviced займа, кнопка "Закрыть форму"'
            }}
          />
        </Col>
      </Row>

      <Row className='u-mb-2'>
        <Col>
          <Title size={3} sm={2} center>
            Займ обслуживается
          </Title>
        </Col>
      </Row>

      <Row className='u-mb-2'>
        <Col md={6} offsetMd={3}>
          <Panel color='success'>
            <Row className='u-mb-1'>
              <Col>
                <Title size={1} color='inverse' center>
                  {formatMoney(totalAmount)}
                </Title>
              </Col>
            </Row>
            <Row>
              <Col>
                <Title size={3} color='inverse' center>
                  {`Вернуть до ${moment(loan.deadline).format('D MMMM YYYY')}`}
                </Title>
              </Col>
            </Row>
          </Panel>
        </Col>
      </Row>

      <Row className='u-mb-4'>
        <Col md={6} offsetMd={3}>
          <Text center>
            Вы взяли займ {formatMoney(loan.amount)} на {loan.term}&nbsp;
            {formatDayCount(loan.term)}.{' '}
            Сумма к возврату {formatMoney(totalAmount)}{' '}
            до {moment(loan.deadline).format('DD.MM.YYYY')}{' '}
            включительно.{' '}
            <Text capitalize inline='block'>
              {formatRemainingTime(calcRemainingDaysUntilOverdue(loan.deadline))}{', '}
              чтобы погасить задолженность.
            </Text>
          </Text>
        </Col>
      </Row>

      <Row className='u-mb-2'>
        <Col md={6} offsetMd={3}>
          <LoanConditions
            loan={loan}
            visibleParams={['decisionDate', 'deadline']}
          />
        </Col>
      </Row>

      <Row className='u-mb-8'>
        <Col offsetMd={4} md={4}>
          <Button
            onClick={() => props.onGetPaymentMethods(loan.creditor)}
            maxWidth
            dataAttrs={{
              state: 'ev:click;ctx:Детали serviced займа, кнопка "Способы оплаты займа"'
            }}
          >
            Способы оплаты займа
          </Button>
        </Col>
      </Row>

    </Container>
  )
}

ServicedLoan.propTypes = {
  loan: LoanProps.isRequired,
  onClose: P.func.isRequired,
  onGetPaymentMethods: P.func.isRequired
}
