import React from 'react'
import P from 'prop-types'
import {
  Button,
  Col,
  Container,
  Icon,
  IconButton,
  Panel,
  Row,
  Text,
  Title
} from '../../base'
import {formatDayCount, formatMoney} from '../../helpers/format_helpers'
import {LoanConditions} from './shared'
import {LoanProps} from '../shared/loan_props'

export function RepaidLoan (props) {
  const {loan} = props

  return (
    <Container>
      <Row className='u-mt-2 u-mt-4-sm u-mb-4'>
        <Col offset={2} center className='u-text-align-center'>
          <Icon name='smile-neutral-icon' />
        </Col>
        <Col size={2} start>
          <IconButton
            name='close'
            onClick={props.onClose}
            className='u-align-right'
            dataAttrs={{
              state: 'ev:click;ctx:Детали repaid займа, кнопка "Закрыть форму"'
            }}
          />
        </Col>
      </Row>

      <Row className='u-mb-2'>
        <Col>
          <Title size={3} sm={2} center>
            Займ закрыт
          </Title>
        </Col>
      </Row>

      <Row className='u-mb-2'>
        <Col md={6} offsetMd={3}>
          <Panel color='neutral'>
            <Row className='u-mb-1'>
              <Col>
                <Title size={1} color='inverse' center>
                  {formatMoney(loan.amount)}
                </Title>
              </Col>
            </Row>
            <Row>
              <Col>
                <Title size={3} color='inverse' center>
                  на {loan.term}&nbsp;{formatDayCount(loan.term)}
                </Title>
              </Col>
            </Row>
          </Panel>
        </Col>
      </Row>

      <Row className='u-mb-4'>
        <Col md={6} offsetMd={3}>
          <Text center>
            Ваши обязательства перед Кредитором по данном{' '}
            займу исполнены в полном объеме. Задолженность по займу отсутствует.
          </Text>
        </Col>
      </Row>

      <Row className='u-mb-2'>
        <Col md={6} offsetMd={3}>
          <LoanConditions loan={loan} visibleParams={['moneyIssuedDate', 'closeDate']} />
        </Col>
      </Row>

      {props.canCreateLoan
        ? <Row className='u-mb-8'>
          <Col offsetMd={4} md={4}>
            <Button
              onClick={props.onCreateLoan}
              maxWidth
              isPending={props.isCreateLoanPending}
              dataAttrs={{
                stat: 'ev:click;ctx:Детали repaid займа, кнопка "Оформить новый займ"'
              }}
            >
              Оформить новый займ
            </Button>
          </Col>
        </Row>
        : null
      }

    </Container>
  )
}

RepaidLoan.propTypes = {
  loan: LoanProps.isRequired,
  onClose: P.func.isRequired,
  onCreateLoan: P.func.isRequired,
  canCreateLoan: P.bool,
  isCreateLoanPending: P.bool
}

RepaidLoan.defaultProps = {
  canCreateLoan: false,
  isCreateLoanPending: false
}
