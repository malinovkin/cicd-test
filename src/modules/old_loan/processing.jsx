/* eslint-disable react/forbid-prop-types */
import React from 'react'
import P from 'prop-types'
import {
  Col,
  Container,
  Icon,
  IconButton,
  Row,
  Text,
  Title
} from '../../base'

export class Processing extends React.Component {
  static propTypes = {
    loan: P.object.isRequired,
    onClose: P.func.isRequired,
    onCheckLoanStatus: P.func.isRequired
  }

  componentDidMount () {
    this.intervalId = window.setInterval(this.props.onCheckLoanStatus, 15000)
  }

  componentWillUnmount () {
    window.clearInterval(this.intervalId)
  }

  render () {
    const {loan} = this.props

    return (
      <Container>

        <Row className='u-mt-2 u-mt-4-sm u-mb-4 u-mb-8-sm'>
          <Col offset={2} center>
            <Title size={3} sm={2} center>
              Заявка на займ
            </Title>
          </Col>
          <Col size={2} start>
            <IconButton
              name='close'
              onClick={this.props.onClose}
              className='u-align-right'
            />
          </Col>
        </Row>

        <Row className='u-mb-4'>
          <Col className='u-text-align-center'>
            <Icon name='hourglass-icon' />
          </Col>
        </Row>

        {loan.isLongProcess
          ? <div>

            <Row className='u-mb-2'>
              <Col md={6} offsetMd={3}>
                <Text type='subtitle' center>
                  Обработка заявки продолжается. Это может занять еще 20-30 минут.
                </Text>
              </Col>
            </Row>

            <Row className='u-mb-8'>
              <Col md={6} offsetMd={3}>
                <Text type='subtitle' center>
                  Не волнуйтесь, как только все будет готово, мы отправим вам уведомление по SMS.
                </Text>
              </Col>
            </Row>

          </div>

          : <Row className='u-mb-8'>
            <Col md={6} offsetMd={3}>
              <Text type='subtitle' center>
                Пожалуйста, подождите минуту, мы обрабатываем ваши данные
              </Text>
            </Col>
          </Row>
        }

      </Container>
    )
  }
}
