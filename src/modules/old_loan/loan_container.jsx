import React, {useContext} from 'react'
// deprecated
import {LoanActions} from './loan_data'
import {SessionActions} from '../session_data'
import {ModalType} from '../modals/modal_type'
import {Creditor} from '../../domain'
import {Loan} from './loan'
import {StoreContext, RouterContext} from '../../context'
import {Actions} from '../state'

const links = {
  [Creditor.PayPS]: 'https://www.payps.ru/payoff/index',
  [Creditor.SmsFinance]: 'https://www.smsfinance.ru/scheme#hintReturn',
  [Creditor.DoZarplaty]: 'https://dozarplati.com/how-to-pay',
  [Creditor.Kreditech]: 'https://www.kredito24.ru/content/%D0%BA%D0%B0%D0%BA-%D0%BF%D0%BE%D0%B3%D0%B0%D1%81%D0%B8%D1%82%D1%8C-%D0%B7%D0%B0%D0%B5%D0%BC/',
  [Creditor.SlonFinance]: 'https://slonfinance.ru/faq#faq4',
  [Creditor.OneClickMoney]: 'https://oneclickmoney.ru/how-to-repay-loan/',
  [Creditor.Kviku]: 'https://kviku.ru/site/howtopay',
  [Creditor.EcoFinance]: 'https://creditplus.ru/voprosy-i-otvety/#pogashenie-zajma',
  [Creditor.WebBankir]: 'https://webbankir.com/repay',
  [Creditor.MoneyMan]: 'https://moneyman.ru/return-credit/',
  [Creditor.HoneyMoney]: 'https://hm-finance.ru/faq'
}

export function LoanContainer ({match}) {
  const {state: {loan, session}, dispatch} = useContext(StoreContext)
  const {history} = useContext(RouterContext)

  return (
    <Loan
      loan={loan.loan}
      permissions={session.permissions}
      userPhone={session.phone}
      id={match.id}
      alternativeOffers={loan.alternativeOffers}

      fetchLoan={id => dispatch(LoanActions.fetchLoan(id))}
      fetchPermissions={() => dispatch(SessionActions.fetchUser())}
      componentWillUnmount={() => dispatch(LoanActions.unmount())}
      onClose={() => history.push('/loans')}
      onGetCreditHistory={() => {
        window.location = 'http://mycreditinfo.ru/prunto-otkaz-mikrozaim/' +
          '?utm_medium=Referral&utm_source=Finspin&utm_campaign=Credit%20' +
          'History%20Crosspromo%20RejectedClients'
      }}
      onGetPaymentMethods={creditor => {
        window.location = links[creditor]
      }}
      onGetMoney={({creditor, creditorUrl}) => {
        dispatch(Actions.openModal(ModalType.RedirectToCreditor, {
          creditor, url: creditorUrl}))
      }}
      onCheckLoanStatus={() => dispatch(LoanActions.fetchLoan(match.id))}
      createLoan={() => dispatch(LoanActions.createLoan({history}))}
      onShowAgreement={(vendor, agreement) =>
        dispatch(Actions.openModal(ModalType.Agreement, {vendor, agreement}))
      }
      fetchAlternativeOffers={loanId => dispatch(LoanActions.fetchAlternativeOffers(loanId))}
    />
  )
}
