/* eslint-disable react/forbid-prop-types */
import React from 'react'
import P from 'prop-types'
import * as R from 'ramda'
import {
  Col,
  Container,
  Icon,
  IconButton,
  Button,
  Row,
  Text,
  Title,
  ExpansionPanel,
  Card,
  Link
} from '../../base'
import {CardTemplate, LoanParam} from '../loan_list/shared'
import {formatDayCount, formatMoney} from '../../helpers/format_helpers'

function Offer (props) {
  const {info} = props

  let sumTitle

  if (R.isNil(info.newSumFrom)) {
    sumTitle = formatMoney(info.newSumTo)
  } else if (R.isNil(info.newSumTo)) {
    sumTitle = formatMoney(info.newSumFrom)
  } else {
    sumTitle =
      `${formatMoney(info.newSumFrom, {withoutCurrency: true})} - ` +
      `${formatMoney(info.newSumTo)}`
  }

  function goToMFI () {
    window.open(info.trackingLink, '_blank')
  }

  return (
    <CardTemplate
      block1={
        <div>
          <Text inline='block' className='u-mb-1' color='secondary'>
            Кредитор
          </Text>
          <div>
            <img src={info.logo} alt='Логотип МФО' />
          </div>
        </div>
      }
      block3={
        <Container className='u-mt-2-md'>
          <Row>
            <Col size='auto' className='u-mr-4 u-mb-2 u-p-0'>
              <LoanParam
                caption='Сумма займа'
                value={<Title size={4}>{sumTitle}</Title>}
              />
            </Col>

            <Col size='auto' className='u-mr-4 u-mb-2 u-p-0'>
              <LoanParam
                caption='Срок займа до'
                value={
                  <Title size={4}>
                    {info.newTermTo} {formatDayCount(info.newTermTo)}
                  </Title>
                }
              />
            </Col>

            <Col size='auto' className='u-mb-2 u-p-0'>
              <LoanParam
                caption='Ставка в день'
                value={<Title size={4}>{info.newPercentFrom} %</Title>}
              />
            </Col>
          </Row>
        </Container>
      }
      onAction={goToMFI}
      buttonCaption='Подать заявку'
      buttonColor='primary'
      cardBorderType='none'
      cardSimple
      className={props.className}
    />
  )
}

Offer.propTypes = {
  info: P.shape({
    newTermTo: P.number.isRequired,
    newPercentFrom: P.number.isRequired,
    logo: P.string.isRequired,
    trackingLink: P.string.isRequired,
    newSumFrom: P.number,
    newSumTo: P.number
  }).isRequired,
  className: P.string
}

Offer.defaultProps = {
  className: null,
  newSumFrom: null,
  newSumTo: null
}

export class AlternativeRejectedLoan extends React.Component {
  static propTypes = {
    fetchData: P.func.isRequired,
    onClose: P.func.isRequired,
    offers: P.object,
    loan: P.object
  }

  static defaultProps = {
    offers: null
  }

  componentDidMount () {
    if (!this.props.offers) {
      this.props.fetchData(this.props.loan && this.props.loan.id)
    }
  }

  render () {
    const {offers} = this.props

    if (!offers) {
      return null
    }

    return (
      <Container>
        <Row className='u-mt-2 u-mt-4-sm u-mb-2'>
          <Col offset={2} center className='u-text-align-center'>
            <Title sm={2} size={3} center>
              Результат
            </Title>
          </Col>
          <Col size={2} start>
            <IconButton
              name='close'
              onClick={this.props.onClose}
              className='u-align-right'
              dataAttrs={{
                state:
                  'ev:click;ctx:Детали rejected займа, кнопка "Закрыть форму"'
              }}
            />
          </Col>
        </Row>

        <Row className='u-mb-4'>
          <Col offset={2} size={8}>
            <Text center>
              Нам не удалось найти для вас подходящий займ среди подключенных к
              нам МФО.
            </Text>
          </Col>
        </Row>

        <Row className='u-mb-4'>
          <Col offset={2} size={8}>
            <Title size={4} sm={3} center>
              Вы можете отправить заявку на займ у наших партнеров
            </Title>
          </Col>
        </Row>

        {R.keys(offers).map(title => (
          <ExpansionPanel
            caption={title.slice(0, title.length - 1)}
            key={title}
            className='u-mb-4'
          >
            {offers[title].map(offer => (
              <Offer info={offer} key={offer.id} className='u-mb-2' />
            ))}
          </ExpansionPanel>
        ))}

        <Row className='u-mt-2 u-mt-4-sm u-mb-4'>
          <Col center className='u-text-align-center'>
            <Title size={3} center>
              Рекомендуем
            </Title>
          </Col>
        </Row>

        <Card simple className='u-mb-6'>
          <Row>
            <Col size={4} sm={3} md={2} className='u-text-align-center'>
              <Icon name='credit-rating-icon' />
            </Col>
            <Col size={8} sm={9} md={7}>
              <Text type='subtitle' bold left className='u-hidden-lt-sm'>
                Проверьте свой кредитный рейтинг
              </Text>
              <Text type='body-1' bold left className='u-hidden-mt-sm'>
                Проверьте свой кредитный рейтинг
              </Text>
              <Text left className='u-mt-1 u-hidden-lt-sm'>
                Возможная причина отказов — низкий кредитный рейтинг.
              </Text>
              <Text left className='u-hidden-lt-sm'>
                Проверьте его и получите 4 рекомендации по его улучшению
              </Text>
            </Col>
            <Col sm={12} md={3} className='u-mt-2 u-mt-0-md'>
              <Link
                onClick={() =>
                  window.open(
                    'http://ki.mycreditinfo.ru/rating?utm_source=finspin.ru&utm_medium=referral&utm_campaign=Rejected_page_suggestion',
                    '_blank'
                  )
                }
                className='u-border-bottom-none'
              >
                <Button maxWidth color='secondary' size='sm'>
                  Подробнее
                </Button>
              </Link>
              <Text center className='u-mt-2 u-hidden-lt-md'>
                <Link
                  onClick={() =>
                    window.open(
                      'https://mycreditinfo.ru/Report/ReportSample?serviceId=6',
                      '_blank'
                    )
                  }
                >
                  Пример отчета
                </Link>
              </Text>
            </Col>
          </Row>
        </Card>
      </Container>
    )
  }
}
