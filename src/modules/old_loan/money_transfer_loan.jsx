import React from 'react'
import P from 'prop-types'
import {
  Col,
  Container,
  Icon,
  IconButton,
  Panel,
  Row,
  Text,
  Title
} from '../../base'
import {formatDayCount, formatMoney} from '../../helpers/format_helpers'
import {LoanConditions} from './shared'
import {LoanProps} from '../shared/loan_props'

export function MoneyTransferLoan (props) {
  const {loan} = props

  return (
    <Container>

      <Row className='u-mt-2 u-mt-4-sm u-mb-4'>
        <Col offset={2} center className='u-text-align-center'>
          <Icon name='smile-glad-icon' />
        </Col>
        <Col size={2} start>
          <IconButton
            name='close'
            onClick={props.onClose}
            className='u-align-right'
            dataAttrs={{
              state: 'ev:click;ctx:Детали moneyTransfer займа, кнопка "Закрыть форму"'
            }}
          />
        </Col>
      </Row>

      <Row className='u-mb-2'>
        <Col>
          <Title size={3} sm={2} center>
            Ожидается перевод денег
          </Title>
        </Col>
      </Row>

      <Row className='u-mb-2'>
        <Col md={6} offsetMd={3}>
          <Panel color='success'>
            <Row className='u-mb-1'>
              <Col>
                <Title size={1} color='inverse' center>
                  {formatMoney(loan.amount)}
                </Title>
              </Col>
            </Row>
            <Row>
              <Col>
                <Title size={3} color='inverse' center>
                  на {loan.term}&nbsp;{formatDayCount(loan.term)}
                </Title>
              </Col>
            </Row>
          </Panel>
        </Col>
      </Row>

      <Row className='u-mb-4'>
        <Col md={6} offsetMd={3}>
          <Text center>
            По одобренному займу ожидается перевод денег. Это может занять
            какое-то время, в зависимости от выбранного вами способа получения.
          </Text>
        </Col>
      </Row>

      <Row className='u-mb-2'>
        <Col md={6} offsetMd={3}>
          <LoanConditions loan={loan} visibleParams={['decisionDate']} />
        </Col>
      </Row>

      <Row className='u-mb-4'>
        <Col md={6} offsetMd={3}>
          <Text center>
            Если процесс перевода затянется, пожалуйста, обратитесь в нашу
            службу поддержки.
          </Text>
        </Col>
      </Row>

    </Container>
  )
}

MoneyTransferLoan.propTypes = {
  loan: LoanProps.isRequired,
  onClose: P.func.isRequired
}
