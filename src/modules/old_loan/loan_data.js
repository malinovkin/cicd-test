import * as R from 'ramda'
import {createReducer} from '../../helpers/redux_helpers'

const Types = {
  FETCH_LOAN: 'loan/FETCH_LOAN',
  CREATE_LOAN: 'loan/CREATE_LOAN',
  UNMOUNT: 'loan/UNMOUNT',
  FETCH_ALTERNATIVE_OFFERS: 'loan/FETCH_ALTERNATIVE_OFFERS'
}

export const LoanActions = {
  fetchLoan: id => (dispatch, getState, api) =>
    dispatch({
      type: Types.FETCH_LOAN,
      payload: api.fetchLoan(id)
    }),

  unmount () {
    return {
      type: Types.UNMOUNT
    }
  },

  createLoan: ctx => (dispatch, getState, api) =>
    dispatch({
      type: Types.CREATE_LOAN,
      payload: api.createLoanRequest()
    }).then(({value}) => {
      ctx.history.push(`/loans/${value.id}`)
    }),

  fetchAlternativeOffers: loanId => (dispatch, getState, api) =>
    dispatch({
      type: Types.FETCH_ALTERNATIVE_OFFERS,
      payload: api.fetchAlternativeOffers(loanId)
    })
}

const initialState = {
  loan: null,
  isCreateLoanPending: false,
  alternativeOffers: null
}

export const loanReducer = createReducer(initialState, {
  [Types.FETCH_LOAN]: {
    fulfilled (state, action) {
      return R.merge(state, {
        loan: action.payload
      })
    }
  },

  [Types.CHECK_LOAN_STATUS]: {
    fulfilled (state, action) {
      return R.merge(state, {
        loan: action.payload
      })
    }
  },

  [Types.UNMOUNT] () {
    return initialState
  },

  [Types.FETCH_ALTERNATIVE_OFFERS]: {
    fulfilled (state, action) {
      return R.merge(state, {
        alternativeOffers: action.payload
      })
    }
  }
})
