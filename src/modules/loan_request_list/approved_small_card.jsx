import React from 'react'
import { Col, Icon, Row, Text, Title, Card, Button } from '../../base'
import {
  formatDayCount,
  formatMoney,
  formatISODateToTextFieldWithMonth
} from '../../helpers/format_helpers'
import { calcTotalLoanAmount, Creditor } from '../../domain'

const specialCondition = {
  [Creditor.Kviku]: (
    <div className='bullet-block u-flex-center'>
      <span className='bullet green' />
      <Text
        type='body-2'
        color='secondary'
        center
        leftMd
        className='u-text-align-center u-text-align-left-md'
      >
        Виртуальная карта с активируемым лимитом
        <br />
        до 1000 рублей
      </Text>
    </div>
  )
}

export function ApprovedSmallCard ({ suggestion, onGetMoney }) {
  return (
    <Card className='u-mb-2' simple>
      <Row vCenter>
        <Col md={3} className='u-mb-2 u-mb-0-md u-text-align-center'>
          <Icon
            name={`${suggestion.mfiAlias}-large-logo-icon`}
            className='u-mt-1'
          />
        </Col>
        <Col md={6} center className='u-mb-2 u-mb-0-md u-pl-0'>
          <Row className='u-mb-1 u-mb-0-md u-text-align-center u-text-align-left-md'>
            <Col sm={12} md={6}>
              <Title size={4}>
                {formatMoney(suggestion.amount)} на {suggestion.term}{' '}
                {formatDayCount(suggestion.term)}
              </Title>
            </Col>
            <Col sm={12} md={6} className='u-mb-1'>
              <Title size={4} center leftMd>
                К возврату{' '}
                {formatMoney(
                  calcTotalLoanAmount(suggestion.amount, suggestion.overpayment)
                )}
              </Title>
            </Col>
          </Row>
          <Row className='u-hidden-lt-md'>
            <Col>
              <div className='bullet-block'>
                <span className='bullet green u-mr-1' />
                <Text
                  type='body-2'
                  color='secondary'
                  center
                  leftMd
                  className='u-text-align-center u-text-align-left-md'
                >
                  Одобрено до{' '}
                  {formatISODateToTextFieldWithMonth(suggestion.expirationDate)}
                </Text>
              </div>
            </Col>
            {specialCondition[suggestion.mfiAlias] ? (
              <Col>{specialCondition[suggestion.mfiAlias]}</Col>
            ) : null}
          </Row>
        </Col>
        <Col md={3} className='u-mb-2 u-mb-0-md'>
          <Button
            onClick={() => onGetMoney(suggestion.mfiAlias, suggestion.mfiUrl)}
            maxWidth
            size='sm'
            className='u-my-0 u-my-1-md'
          >
            Получить деньги
          </Button>
        </Col>
      </Row>
      <Row className='u-hidden-mt-md u-mb-1'>
        <Col>
          <div className='bullet-block u-flex-center'>
            <span className='bullet green u-mr-1' />
            <Text
              type='body-2'
              color='secondary'
              center
              leftMd
              className='u-text-align-center u-text-align-left-md'
            >
              Одобрено до{' '}
              {formatISODateToTextFieldWithMonth(suggestion.expirationDate)}
            </Text>
          </div>
        </Col>
      </Row>
      {specialCondition[suggestion.mfiAlias] ? (
        <Row className='u-hidden-mt-md u-mb-1'>
          <Col>{specialCondition[suggestion.mfiAlias]}</Col>
        </Row>
      ) : null}
    </Card>
  )
}
