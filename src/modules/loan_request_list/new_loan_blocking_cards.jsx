import React from 'react'
import { Col, Icon, Button, Card, Row, Text, Title } from '../../base'
import { formatDayCount } from '../../helpers/format_helpers'
import { calcRemainingDaysUntilExpiration } from '../../domain'

export function NewLoanBlockingCards ({
  onGetLoanRequestDetails,
  id,
  expirationDate
}) {
  const title = (
    <Title
      size={3}
      className={
        'u-mb-3 u-mb-1-md u-mt-2 ' + 'u-text-align-center u-text-align-left-md'
      }
    >
      Оформить новую заявку вы сможете через{' '}
      {calcRemainingDaysUntilExpiration(expirationDate)}{' '}
      {formatDayCount(calcRemainingDaysUntilExpiration(expirationDate))}
    </Title>
  )

  return (
    <React.Fragment>
      <Card>
        <Row className='u-hidden-mt-md'>
          <Col>{title}</Col>
        </Row>

        <Row>
          <Col md={2} className={'u-mb-3 u-my-1-md ' + 'u-text-align-center'}>
            <Icon name='smile-warning-icon' />
          </Col>
          <Col md={9}>
            <Row className='u-hidden-lt-md'>
              <Col>{title}</Col>
            </Row>

            <Row className='u-mb-1'>
              <Col>
                <Text center leftMd>
                  Чтобы оформить следующую заявку на подбор займов, необходимо
                  дождаться, когда истечет срок последнего предложения в заявке.
                </Text>
              </Col>
            </Row>
          </Col>
        </Row>
      </Card>
      <Card borderType='none' className='u-mt-2 u-py-4-md u-px-5'>
        <Row className=''>
          <Col md={5} className='u-pr-8-md u-pr-none'>
            <Row className='u-mb-3 u-mb-4-md'>
              <Col>
                <Row className='u-mb-1'>
                  <Col>
                    <Title size={3} className='u-text-align-center u-text-align-left-md'>Подходящего займа пока не нашлось</Title>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Text center leftMd>
                      Нам не удалось найти для вас подходящий займ среди
                      подключенных к нам МФО.
                    </Text>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>

          <Col md={7} className='u-border-left-md u-border-left-none u-pl-5-md'>
            <Row className=''>
              <Col>
                <div className='u-flex-md u-mb-1 u-text-align-center u-text-align-left-md'>
                  <div className='u-mr-3-md'>
                    <Icon name='smile-idea-icon' />
                  </div>
                  <div>
                    <Text inline='block' color='secondary' className='u-mb-2'>
                      Рекомендуем
                    </Text>
                    <Title size={4}>
                      Вы можете отправить заявку на займ у наших партнеров
                    </Title>
                  </div>
                </div>
              </Col>
            </Row>
            <Row className='u-mt-1'>
              <Col md={8}>
                <Button
                  color='secondary'
                  onClick={() => onGetLoanRequestDetails(id)}
                  maxWidth
                  className='u-mb-2'
                >
                  Подробнее
                </Button>
              </Col>
            </Row>
          </Col>
        </Row>
      </Card>
    </React.Fragment>
  )
}
