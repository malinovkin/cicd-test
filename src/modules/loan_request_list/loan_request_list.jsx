import React, { useContext, useEffect } from 'react'

import P from 'prop-types'
import { ApprovedSmallCard } from './approved_small_card'
import {
  Container,
  Row,
  Col,
  Button,
  Icon,
  Title,
  Text,
  Card,
  ExpansionPanel,
  ContentBeforeFooter,
  ContentWithFooter,
  Footer,
  Link
} from '../../base'
import {
  formatMoney,
  formatDayCount,
  formatISODateToTextField,
  formatLoanCount,
  formatSuggestionCount,
  formatPhone
} from '../../helpers/format_helpers'
import { ListLoader } from './list_loader'
import { NewLoanBlockingCards } from './new_loan_blocking_cards'
import { AccountNavbar } from '../shared/account_navbar'
import { StoreContext, RouterContext } from '../../context'
import { Actions } from '../../modules/state'
import { ModalType } from '../modals/modal_type'
import { NewLoanRequestCard } from './new_loan_request_card'
// import { LoadingDots } from './loadingDots'
import { API } from '../../api'
import { scrollUp } from '../../helpers/dom_helpers'
import { changePage } from '../../metrics'

function ActiveLoanRequest ({
  status,
  step,
  createdDate,
  requestedAmount,
  requestedTerm,
  id,
  onGetLoanRequestDetails
}) {
  const { state, dispatch } = useContext(StoreContext)
  const { history } = useContext(RouterContext)
  useEffect(() => {
    dispatch(Actions.fetchActiveLoanRequestSuggestion(id))
  }, [])

  let { suggestions } = state.loanRequestList.activeLoanRequest
  suggestions = suggestions || []

  const activeSuggestions = suggestions.filter(
    suggestion => suggestion.status === 'available'
  )
  let activeSuggestionsCount = String(activeSuggestions.length) || null
  const isSuggestionLoaded = !!state.loanRequestList.activeLoanRequest
    .suggestions

  const condition = {
    isStart: status === 'needInfo' && (step === 0 || step === 1),
    isStartProcessing: status === 'needInfo' && (step === 2 || step === 3),
    isProcessing: status === 'inProgress',
    isFinish: status === 'finished'
  }

  function onGetMoney (creditor, creditorUrl) {
    dispatch(
      Actions.openModal(ModalType.RedirectToCreditor, {
        creditor,
        url: creditorUrl
      })
    )
  }

  function renderHeaderContent () {
    return (
      <Row className={condition.isStart ? '' : 'u-mb-4'} vCenter>
        <Col md={2} className='u-text-align-center'>
          <Icon name='smile-glad-icon' />
        </Col>
        <Col
          md={10}
          className={`u-mt-3S u-text-align-center u-text-align-left-md ${
            condition.isStart ? 'u-mt-1-mdS' : 'u-mt-2-mdS'
          }`}
        >
          <Container>
            {condition.isStart ? (
              <Row vCenter>
                <Col md={8}>
                  {requestedAmount && requestedTerm ? (
                    <React.Fragment>
                      <Text color='secondary'>
                        Новая заявка от {formatISODateToTextField(createdDate)}
                      </Text>

                      <Title
                        size={3}
                        className='u-text-align-center u-text-align-left-md u-mt-1'
                      >
                        {formatMoney(requestedAmount)} на {requestedTerm}{' '}
                        {formatDayCount(requestedTerm)}
                      </Title>

                      <Text className='u-mt-2 u-mt-1-md'>
                        Осталось заполнить всего несколько полей
                      </Text>
                    </React.Fragment>
                  ) : (
                    <React.Fragment>
                      <Text color='secondary'>
                        {formatISODateToTextField(createdDate)}
                      </Text>

                      <Title
                        size={3}
                        className='u-text-align-center u-text-align-left-md u-mt-1'
                      >
                        Ваша заявка на займ
                      </Title>

                      <Text className='u-mt-2 u-mt-1-md'>
                        Осталось заполнить всего несколько полей
                      </Text>
                    </React.Fragment>
                  )}
                </Col>
                <Col md={4} className='u-text-align-right-md'>
                  <Button
                    onClick={() => onGetLoanRequestDetails(id)}
                    className='u-mt-3 u-mt-0-md'
                    size='md'
                    maxWidth
                  >
                    Продолжить
                  </Button>
                </Col>
              </Row>
            ) : (
              <Row vCenter>
                <Col md={4}>
                  <Text color='secondary'>
                    Ваша заявка от {formatISODateToTextField(createdDate)}
                  </Text>
                  <Title
                    size={3}
                    className={
                      'u-mb-2 u-mb-1-md u-mt-1 ' +
                      'u-text-align-center u-text-align-left-md'
                    }
                  >
                    {formatMoney(requestedAmount)} на {requestedTerm}{' '}
                    {formatDayCount(requestedTerm)}
                  </Title>
                </Col>
                {isSuggestionLoaded && activeSuggestionsCount !== '0' ? (
                  <Col md={4}>
                    <Text color='secondary'>Можно воспользоваться</Text>
                    <Title
                      size={3}
                      className={
                        'u-mb-1-md u-mt-1 ' +
                        'u-text-align-center u-text-align-left-md'
                      }
                    >
                      {activeSuggestionsCount}{' '}
                      {formatLoanCount(activeSuggestionsCount)}
                    </Title>
                  </Col>
                ) : (
                  <Col md={4} />
                )
                // (<Text
                //     className={
                //       'u-mb-1-md u-mt-1 ' +
                //       'u-text-align-center u-text-align-left-md'
                //     }
                //   >
                //     Идет подбор ...
                //   </Text>)
                }

                <Col md={4}>
                  <Button
                    onClick={() => {
                      dispatch(Actions.goToList())
                      onGetLoanRequestDetails(id)
                    }}
                    color='secondary'
                    className='u-mt-3 u-mt-0-md'
                    maxWidth
                  >
                    Подробнее
                  </Button>
                </Col>
              </Row>
            )}
          </Container>
        </Col>
      </Row>
    )
  }

  function renderProcessingBlock () {
    if (condition.isStartProcessing || condition.isProcessing) {
      return (
        <Row>
          <Col>
            <Container>
              {/* <Row className='u-mb-2 u-mb-3-md'>
                <Col md={8} offsetMd={2}>
                  <LoadingDots />
                </Col>
              </Row> */}
              <Row className='u-mb-2'>
                <Col md={8} offsetMd={2}>
                  <Title
                    center
                    size={3}
                    md={2}
                    className='u-text-align-center u-text-align-left-md'
                  >
                    Подбор займов
                  </Title>
                </Col>
              </Row>
              <Row className='u-mb-6'>
                <Col md={8} offsetMd={2}>
                  {condition.isProcessing ? (
                    <Text center size={18}>
                      Мы продолжаем подбирать подходящие займы. Это может занять
                      еще 20-30 минут.
                    </Text>
                  ) : (
                    <Text center size={18}>
                      {/* Мы подбираем для вас займы. Заполните{' '} */}
                      Заполните{' '}
                      <Link
                        onClick={() => {
                          dispatch(Actions.goToApplication())
                          return history.push(`loan-requests/${id}`)
                        }}
                      >
                        еще один шаг формы
                      </Link>
                      {/* , чтобы получить больше займов */}, чтобы получить
                      займ
                    </Text>
                  )}
                </Col>
              </Row>
            </Container>
          </Col>
        </Row>
      )
    }
    return null
  }

  function renderSuggestionsList () {
    if (!condition.isStart) {
      return (
        <Row className='u-pl-2 u-pr-2'>
          <Col>
            {isSuggestionLoaded ? (
              activeSuggestions.map(suggestion => {
                return (
                  <ApprovedSmallCard
                    suggestion={suggestion}
                    key={suggestion.id}
                    onGetMoney={onGetMoney}
                  />
                )
              })
            ) : (
              <div className='u-text-align-center'>Загрузка...</div>
            )}
          </Col>
        </Row>
      )
    }
    return null
  }

  return (
    <Container
      className={`${
        condition.isStart ? 'u-pt-4 u-pb-4' : 'u-pt-4 u-pb-6'
      } 'u-px-0 u-px-2-sm'`}
    >
      <Card className='u-px-0 u-px-3-sm u-py-4'>
        {renderHeaderContent()}
        {renderProcessingBlock()}
        {renderSuggestionsList()}
      </Card>
    </Container>
  )
}

function ArchiveLoanRequest ({ onGetLoanRequestDetails }) {
  const { state, dispatch } = useContext(StoreContext)

  function handleToggleArchive () {
    if (!state.loanRequestList.archiveLoanRequest) {
      dispatch(Actions.fetchArchiveLoanRequest())
    }
  }

  let { archiveLoanRequest } = state.loanRequestList
  archiveLoanRequest = archiveLoanRequest || []

  return (
    <Container>
      <ExpansionPanel
        className=' u-mt-6 u-mb-6'
        caption='История заявок'
        captionCenter
        captionLeftMd
        defaultClosed
        onToggle={handleToggleArchive}
      >
        {archiveLoanRequest.length > 0
          ? archiveLoanRequest.map(
            (
              {
                countLoanRequestItem,
                createdDate,
                requestedAmount,
                requestedTerm,
                id
              },
              i
            ) => {
              return (
                <Card className='u-mb-2 u-px-0 u-px-3-sm u-py-4' key={i}>
                  <Row vCenter>
                    <Col md={2} className='u-text-align-center'>
                      <Icon name='smile-neutral-icon' />
                    </Col>
                    <Col
                      md={10}
                      className='u-text-align-center u-text-align-left-md'
                    >
                      <Container>
                        <Row vCenter>
                          <Col md={4}>
                            <Text color='secondary'>
                                Ваша заявка от{' '}
                              {formatISODateToTextField(createdDate)}
                            </Text>
                            <Title
                              size={3}
                              className={
                                'u-mb-2 u-mb-0-md u-mt-1 ' +
                                  'u-text-align-center u-text-align-left-md'
                              }
                            >
                              {formatMoney(requestedAmount)} на{' '}
                              {requestedTerm} {formatDayCount(requestedTerm)}
                            </Title>
                          </Col>
                          <Col md={4}>
                            <Text color='secondary'>Было одобрено</Text>
                            <Title
                              size={3}
                              className={
                                'u-mb-0-md u-mt-1 ' +
                                  'u-text-align-center u-text-align-left-md'
                              }
                            >
                              {countLoanRequestItem}{' '}
                              {formatSuggestionCount(countLoanRequestItem)}
                            </Title>
                          </Col>
                          <Col md={4}>
                            <Button
                              onClick={() => onGetLoanRequestDetails(id)}
                              color='secondary'
                              className='u-mt-3 u-my-1-md'
                              maxWidth
                            >
                                Подробнее
                            </Button>
                          </Col>
                        </Row>
                      </Container>
                    </Col>
                  </Row>
                </Card>
              )
            }
          )
          : null}
      </ExpansionPanel>
    </Container>
  )
}

export function LoanRequestList () {
  const { state, dispatch } = useContext(StoreContext)
  const { history } = useContext(RouterContext)
  useEffect(() => {
    scrollUp()
    changePage('loanRequests')
    if (!state.newLoanOptions) {
      dispatch(Actions.fetchNewLoanOptions())
    }
    if (!state.loanRequestList.activeLoanRequest.id) {
      dispatch(Actions.fetchActiveLoanRequest())
    }
    if (!state.permission) {
      dispatch(Actions.fetchUser())
    }
    return componentWillUnmount
  }, [])

  function isVisibleDataLoading () {
    return (
      !state.newLoanOptions ||
      !state.loanRequestList.activeLoanRequest ||
      !state.permissions
    )
  }

  function renderNewLoanRequest () {
    if (isVisibleDataLoading()) {
      return null
    }
    if (state.permissions.newLoan) {
      return (
        <NewLoanRequestCard
          {...state.newLoanOptions}
          onSubmit={onCreateLoanRequest}
          isSubmitPending={state.pendings.createLoanRequest}
        />
      )
    }
  }

  function renderActiveLoanRequest () {
    if (isVisibleDataLoading()) {
      return null
    }
    if (state.loanRequestList.activeLoanRequest.id) {
      if (
        state.loanRequestList.activeLoanRequest.countLoanRequestItem === 0 &&
        state.loanRequestList.activeLoanRequest.status === 'finished'
      ) {
        return (
          <NewLoanBlockingCards
            onGetLoanRequestDetails={onGetLoanRequestDetails}
            {...state.loanRequestList.activeLoanRequest}
          />
        )
      }
      return (
        <ActiveLoanRequest
          {...state.loanRequestList.activeLoanRequest}
          onGetLoanRequestDetails={onGetLoanRequestDetails}
        />
      )
    }
  }

  function renderArchiveLoanRequest () {
    if (isVisibleDataLoading()) {
      return null
    }
    if (!state.permissions.emptyArchiveLoans) {
      return (
        <ArchiveLoanRequest
          {...state.newLoanOptions}
          onGetLoanRequestDetails={onGetLoanRequestDetails}
        />
      )
    }
  }

  function onCreateLoanRequest ({ amount, term }) {
    dispatch(Actions.setWantedLoanConditions(amount, term))
    dispatch(Actions.createLoanRequest(state.leadProviderParams.common)).then(
      ({ value }) => {
        API.saveLoanConditions(value.id, amount, term)
        history.push(`/loan-requests/${value.id}`)
      }
    )
  }

  function onGetLoanRequestDetails (LoanRequestId) {
    return history.push(`/loan-requests/${LoanRequestId}`)
  }

  function componentWillUnmount () {
    return dispatch(Actions.loanRequestListUnmount())
  }

  function onGetAgreements () {
    return history.push(`/agreements`)
  }

  function renderFooterContent () {
    return (
      <Container className='u-pt-2 u-pt-4-md u-pb-4'>
        <Row>
          <Col>
            <Icon name='finspin-logo-icon' />
          </Col>
        </Row>
        <Row>
          <Col md={4} className='u-pt-2'>
            <Text color='secondary' type='caption'>
              Подбираем микрозаймы на любые цели. До 30 000 рублей за 10 минут.
              Заявка онлайн. Выдача 24 часа в сутки.
            </Text>
          </Col>
          <Col md={8} className='u-pt-2'>
            <Text type='body-1' bold>
              <Link href={`tel:8${process.env.SUPPORT_PHONE}`}>
                {formatPhone(process.env.SUPPORT_PHONE, { firstDigit: '8' })}
              </Link>
            </Text>
            <Text color='secondary' type='caption'>
              С 9:00 до 18:00 по московскому времени звонок по России бесплатный
            </Text>
            <br />
            <Text color='secondary' type='caption'>
              Email:{' '}
              <Link href='mailto:support@finspin.ru'>support@finspin.ru</Link>
            </Text>
          </Col>
        </Row>
        <Row className='u-pt-2'>
          <Col>
            <Text color='secondary' type='caption'>
              Общество с ограниченной ответственностью «Финспин». Юридический
              адрес: 620075, г. Екатеринбург, ул. Мамина-Сибиряка, д. 101, офис
              4.06 ОГРН 1146670012444, ИНН 6670424655. ООО «Финспин»
              сотрудничает только с надлежащим образом зарегистрированными МФО,
              состоящими в государственном реестре микрофинансовых организаций
              ЦБ РФ.
            </Text>
            <Text color='secondary' type='caption' className='u-mt-1'>
              <Link onClick={() => onGetAgreements()}>
                Соглашения и документы
              </Link>
            </Text>
            <Text color='secondary' type='caption' className='u-pt-2'>
              © 2016-{new Date().getFullYear()} ООО «Финспин»
            </Text>
          </Col>
        </Row>
      </Container>
    )
  }

  return (
    <div>
      <ContentWithFooter className='g-workspace-bg'>
        <ContentBeforeFooter>
          <AccountNavbar />
          <Container className='u-pt-4 u-pb-2 u-px-0'>
            {renderNewLoanRequest()}
            {renderActiveLoanRequest()}
            {renderArchiveLoanRequest()}
            {isVisibleDataLoading() ? <ListLoader /> : null}
          </Container>
        </ContentBeforeFooter>
        <Footer className='u-border-top'>{renderFooterContent()}</Footer>
      </ContentWithFooter>
    </div>
  )
}

ActiveLoanRequest.propTypes = {
  countLoanRequestItem: P.number.isRequired,
  createdDate: P.string.isRequired,
  expirationDate: P.string,
  id: P.number.isRequired,
  isLongProcess: P.bool,
  onGetLoanRequestDetails: P.func.isRequired,
  requestedAmount: P.number,
  requestedTerm: P.number,
  status: P.string.isRequired,
  step: P.number,
  suggestions: P.arrayOf(
    P.shape({
      amount: P.number,
      approvedDate: P.string,
      expirationDate: P.string,
      id: P.number,
      mfiAlias: P.string,
      mfiUrl: P.string,
      overpayment: P.number,
      rate: P.number,
      status: P.string,
      term: P.number
    })
  )
}
