import React, {useEffect, useContext} from 'react'
import {Redirect, Switch, Route} from 'react-router-dom'

import {Modals} from './modals/modals'
import {ModalType} from './modals/modal_type'
import {StoreContext, RouterContext} from '../context'
import {IdleMonitor} from '../idle_monitor'
import {Auth} from '../modules/auth/auth'
import {Actions} from '../modules/state'
import {LoanRequest} from '../modules/loan_request/loan_request'
import {LoanRequestList} from '../modules/loan_request_list/loan_request_list'
import {Agreements} from '../modules/agreements/agreements'
import {Loan} from '../modules/loan/loan'
// import { setServerErrorHandler } from '../api'

const Landing = require(`../modules/landing/${process.env.THEME}`).Landing

function Account ({children}) {
  const {dispatch} = useContext(StoreContext)
  const {history} = useContext(RouterContext)

  // setServerErrorHandler((error) => {
  //   if (error.status >= 400 && error.status < 500) {
  //     history.push('/auth')
  //   }
  // })

  useEffect(() => {
    IdleMonitor.start({
      maxIdleSeconds: 30 * 24 * 60 * 60, // 30 days
      finalCountdownSeconds: 60,

      onFinalCountdownTick (secondsLeft) {
        dispatch(Actions.openModal(ModalType.AutoLogout, {secondsLeft}))
      },

      onExpire () {
        dispatch(Actions.closeModal(ModalType.AutoLogout))
        dispatch(Actions.logout())
          .then(() => history.push('/'))
      }
    })

    return IdleMonitor.stop
  }, [])

  return children
}

function PublicRoute ({path, component, exact, computedMatch: {params}}) {
  const {state} = useContext(StoreContext)
  return state.authorized && !state.authorizeProcessing
    ? <Redirect to='/loan-requests' />
    : <Route exact={exact} path={path} render={() => React.createElement(component, {match: params})} />
}

function PrivateRoute ({path, component, exact, location, computedMatch: {params}}) {
  return useContext(StoreContext).state.authorized
    ? (
      <Route
        exact={exact}
        path={path}
        render={() => (
          <Account>{React.createElement(component, {match: params})}</Account>
        )}
      />
    ) : <Redirect to={{pathname: '/auth', state: {from: location}}} />
}

function Routes () {
  return (
    <Switch>
      <PublicRoute path='/auth' component={Auth} />
      <PrivateRoute path='/loan-requests/:id/:itemId' component={Loan} />
      <PrivateRoute path='/loan-requests/:id' component={LoanRequest} />
      <PrivateRoute exact path='/loan-requests' component={LoanRequestList} />
      <PrivateRoute path='/agreements' component={Agreements} />
      <PublicRoute exact path='/' component={Landing} />
      <Redirect to='/loan-requests' />
    </Switch>
  )
}

export function App () {
  return (
    <React.Fragment>
      <Routes />
      <Modals />
    </React.Fragment>
  )
}
