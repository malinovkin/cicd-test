import React, {useContext} from 'react'

import {Col, Container, Icon, Link, Menu, Navbar, Row} from '../../base'
// import {SessionActions} from '../session_data'
import {StoreContext, RouterContext} from '../../context'
// ДОБАВИТЬ ФИЛКОСЫ
import { Actions } from '../../modules/state'

export function AccountNavbar () {
  const {dispatch} = useContext(StoreContext)
  const {history} = useContext(RouterContext)

  function onLogout () {
    dispatch(Actions.logout())
    // history.push('/')
  }

  function onNavToLoanList () {
    history.push('/loan-requests')
  }

  function handleMenuSelect (id) {
    switch (id) {
      case 'loan-requests': onNavToLoanList(); break
      case 'logout': onLogout(); break
    }
  }

  return (
    <Navbar>
      <Container>
        <Row hBetween>
          <Col center>
            <Link onClick={onNavToLoanList} className='account-navbar--logo'>
              <Icon name='finspin-logo-icon' />
            </Link>
          </Col>
          <Col className='u-text-align-right'>
            <Menu
              items={[
                {
                  id: 'loan-requests',
                  caption: 'Мои займы',
                  iconName: 'ruble-icon',
                  dataAttrs: {
                    stat: 'ev:click;ctx:Навигация, пункт меню "Мои займы"'
                  }
                },
                {
                  id: 'logout',
                  caption: 'Выйти',
                  iconName: 'exit-icon',
                  dataAttrs: {
                    stat: 'ev:click;ctx:Навигация, пункт меню "Выйти"'
                  }
                }
              ]}
              onSelect={handleMenuSelect}
              caption='Профиль'
              captionIconName='person-icon'
              dataAttrs={{
                stat: 'ev:click;ctx:Навигация, кнопка "Открыть меню навигации"'
              }}
            />
          </Col>
        </Row>
      </Container>
    </Navbar>
  )
}
