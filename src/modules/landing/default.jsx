import React, { useContext, useEffect } from 'react'
import P from 'prop-types'
import { random } from 'lodash/number'
import cn from 'classnames'
import acc from 'accounting'
import ReactSwipe from 'react-swipe'
import moment from 'moment'

import { Icon, Link, CalcPlaceholder, Menu, Text } from '../../base'
import { LoanCalculator } from '../shared/loan_calculator'
import { scrollTo } from '../../helpers/dom_helpers'
import { formatPhone } from '../../helpers/format_helpers'
import { StoreContext, RouterContext } from '../../context'
import { ModalType } from '../../modules/modals/modal_type'
import { changePage } from '../../metrics'
import { AgreementVendor, AgreementName } from '../../domain'
import { Actions } from '../state'

moment.locale('ru-RU')

function Safety ({ text }) {
  return (
    <div className='safety'>
      <div className='safety-icon'>
        <Icon name='lock-icon' />
      </div>
      <div className='safety-text'>{text}</div>
    </div>
  )
}

function MenuItems () {
  function scroll (ev) {
    scrollTo(ev.currentTarget.getAttribute('data-scrollto'))
  }

  return (
    <React.Fragment>
      <div
        className='menu-item'
        data-scrollto='#how-it-work'
        data-stat="click:Главная, меню 'Как это работает'"
        onClick={scroll}
      >
        Как это работает
      </div>
      <div
        className='menu-item'
        data-scrollto='#programs'
        data-stat="click:Главная, меню 'Спецпрограммы'"
        onClick={scroll}
      >
        Спецпрограммы
      </div>
      <div
        className='menu-item'
        data-scrollto='#why-we'
        data-stat="click:Главная, меню 'Почему мы'"
        onClick={scroll}
      >
        Почему мы
      </div>
      {/* <div
        className='menu-item'
        data-scrollto='#faq'
        data-stat="click:Главная, меню 'Вопросы и ответы'"
        onClick={scroll}
      >
        Вопросы и ответы
      </div> */}
    </React.Fragment>
  )
}

function goToService (id) {
  if (id === 'credits') {
    window.location = `${window.location.origin}/bank-cards/`
  }
}

class ExpandedInfo extends React.Component {
  static propTypes = {
    caption: P.string.isRequired,
    children: P.any.isRequired
  }

  state = {
    open: false
  }

  handleClick = () => {
    this.setState(prevState => ({ open: !prevState.open }))
  }

  render () {
    return (
      <div className='expanded-info'>
        <div
          role='presentation'
          className='expanded-info__caption'
          onClick={this.handleClick}
        >
          {this.props.caption}
        </div>
        {this.state.open ? (
          <div className='expanded-info__content'>{this.props.children}</div>
        ) : null}
      </div>
    )
  }
}

function Header ({ onLogin }) {
  return (
    <div className='l-content-pad'>
      <div className='header landing-navbar'>
        <div className='landing-navbar--section landing-mobile-menu-container'>
          <Menu
            items={[
              {
                id: 'credits',
                caption: 'Кредиты и карты',
                iconName: 'wallet-icon'
              }
            ]}
            onSelect={goToService}
            captionIconName='hamburger-icon'
          />
        </div>
        <div className='landing-navbar--section'>
          <div className='landing-navbar--logo' />
          <div className='landing-navbar--delimiter' />
          <div className='landing-navbar--products-menu'>
            <Menu
              items={[
                {
                  id: 'credits',
                  caption: 'Кредиты и карты',
                  iconName: 'wallet-icon'
                }
              ]}
              onSelect={goToService}
              caption='Микрозаймы'
            />
          </div>
        </div>
        <div>
          <div className='menu-old'>
            <MenuItems />
          </div>
          <button
            className='btn btn-outline btn-sm'
            onClick={onLogin}
            data-stat="click:Главная, кнопка 'Войти'"
          >
            Войти
          </button>

          <a
            href={
              process.env.ENV !== 'production'
                ? 'https://test.en.finspin.ru/'
                : 'https://en.finspin.ru/'
            }
          >
            <button className='btn btn-link'>RU/EN</button>
          </a>
        </div>
      </div>
    </div>
  )
}

class LoanForm extends React.Component {
  static propTypes = {
    onSubmit: P.func.isRequired,
    calc: P.object
  }

  static defaultProps = {
    calc: null
  }

  render () {
    return (
      <div className='l-base-loan'>
        <div className='base-loan-title'>
          <div className='title-1'>Подберем микрозайм на любые цели</div>
        </div>
        <div className='base-loan-description'>
          <div className='base-loan-points'>
            <div className='base-loan-point'>
              <div className='base-loan-point-icon'>
                <Icon name='complete-icon' />
              </div>
              <div className='base-loan-point-text'>
                <b>Скажите, сколько денег вам нужно</b> и на какой срок
              </div>
            </div>
            <div className='base-loan-point'>
              <div className='base-loan-point-icon'>
                <Icon name='complete-icon' />
              </div>
              <div className='base-loan-point-text'>
                <b>Finspin бесплатно сравнит предложения</b> нескольких
                кредиторов, и подберет займ конкретно для вас
              </div>
            </div>
            <div className='base-loan-point'>
              <div className='base-loan-point-icon'>
                <Icon name='complete-icon' />
              </div>
              <div className='base-loan-point-text'>
                <b>Вы получите деньги</b> на самых выгодных условиях
              </div>
            </div>
          </div>
          <div className='safety-padding'>
            <Safety text='Мы работаем в строгом соответствии с законодательством РФ. Здесь и далее ваши данные надежно защищены.' />
          </div>
        </div>
        <div className='base-loan-form' id='base-loan'>
          {this.props.calc ? (
            <LoanCalculator
              onSubmit={this.props.onSubmit}
              {...this.props.calc}
            />
          ) : (
            <CalcPlaceholder />
          )}
        </div>
      </div>
    )
  }
}

function HowItWorks () {
  return (
    <div className='l-work'>
      <div className='l-content-pad l-content-line'>
        <div className='work-hand'>
          <Icon name='like-big-icon' />
        </div>

        <div className='work-description' id='how-it-work'>
          <div className='work-title'>
            <div className='title-2'>Микрозайм за 10 минут</div>
            <div className='subtitle-2'>Как это работает</div>
          </div>

          <div className='work-point'>
            <div className='work-point-icon'>
              <Icon name='like-icon' />
            </div>
            <div className='work-point-text'>
              <div className='title-3'>Заполните анкету онлайн</div>
              <p>
                Всего несколько простых полей. Нужен только паспорт и 8–10 минут
                вашего времени.
              </p>
            </div>
          </div>

          <div className='work-point'>
            <div className='work-point-icon'>
              <Icon name='like-icon' />
            </div>
            <div className='work-point-text'>
              <div className='title-3'>
                Получите выгодное предложение от Finspin
              </div>
              <p>
                Сервис предложит вам только самый выгодный займ из всех
                найденных у кредиторов.
              </p>
            </div>
          </div>

          <div className='work-point'>
            <div className='work-point-icon'>
              <Icon name='like-icon' />
            </div>
            <div className='work-point-text'>
              <div className='title-3'>Заберите ваши деньги</div>
              <p>
                Больше 10 способов получить деньги, как вам будет удобней: на
                карту, на счет, наличными.
              </p>
            </div>
          </div>

          <Safety text='Мы сотрудничаем только с проверенными кредиторами. Каждый из них состоит в реестре ЦБ РФ и работает с соблюдением всех нормативных актов и предписаний.' />

          <ExpandedInfo caption='Список МФО, предоставляющих услуги по выдаче и обслуживанию займов'>
            <div>
              <span className='no-wrap'>ООО МФК «СМСФИНАНС»,</span>{' '}
              <span className='no-wrap'>ООО МФК «Займ Онлайн»,</span>{' '}
              <span className='no-wrap'>ООО МФК «Кредитех Рус»,</span>{' '}
              <span className='no-wrap'>ООО МКК АН «БизнесИнвест»,</span>{' '}
              <span>
                ООО МФК «Микрокредитная Компания Универсального Финансирования»,
              </span>{' '}
              <span>ООО МФК «Экофинанс»,</span>{' '}
              <span>ООО МФК «ВЭББАНКИР»,</span> <span>ООО МФК «Мани Мен»,</span>{' '}
              <span>ООО МКК «ПОЛУШКА»,</span>{' '}
              <span>ООО МКК АН «БизнесИнвест»,</span>{' '}
              <span>ООО МФК «ДЗП-Центр»,</span> <span>ООО МФК «МигКредит»</span>{' '}
              и <span className='no-wrap'>ООО МКК «Русинтерфинанс»</span>
            </div>
          </ExpandedInfo>
        </div>
      </div>
    </div>
  )
}

class SpecPrograms extends React.Component {
  static propTypes = {
    onGetSpecialOffer: P.func.isRequired,
    calc: P.object,
    onSubmit: P.func.isRequired
  }

  static defaultProps = {
    calc: null
  }

  handleGetSpecialOffer (title, description) {
    this.props.onGetSpecialOffer({
      description,
      title: `Спецпрограмма «${title}»`,
      calc: this.props.calc,
      onSubmit: this.props.onSubmit
    })
  }

  renderSpecialPrograms () {
    const programs = [
      [
        'На любые цели',
        'Чтобы ничего не мешало вам принять правильное решение, когда это будет нужно',
        true
      ],
      [
        'На путешествие',
        'Помогает получить больше ярких впечатлений, куда бы вы ни отправились'
      ],
      [
        'На подарки близким',
        'Дает возможность проявить максимум внимания к тем, кого вы действительно любите'
      ],
      [
        'На срочную покупку',
        'Позволяет уже сегодня получить то, на что пришлось бы копить пару месяцев'
      ],
      [
        'На оплату услуг',
        'Чтобы оплатить квитанции за ЖКХ, закрыть счета из банков, от физических лиц и организаций'
      ],
      [
        'На ремонт автомобиля',
        'Никакие обстоятельства не должны отнимать у вас права на «свободу передвижения»'
      ]
    ]

    return programs.map(([title, description, isHit], index) => (
      <div className='program' key={index}>
        {isHit && (
          <div className='program-hit'>
            <div className='program-hit-icon'>
              <Icon name='like-outline-icon' />
            </div>
            <div className='program-hit-caption'>хит</div>
          </div>
        )}
        <div className={`program-image program-${index + 1}`} />
        <div className='program-content'>
          <div className='title-3'>{title}</div>
          <p>{description}</p>
          <button
            className='btn btn-outline'
            onClick={() => this.handleGetSpecialOffer(title, description)}
          >
            Получить деньги
          </button>
        </div>
      </div>
    ))
  }

  render () {
    return (
      <div className='l-programs'>
        <div className='l-content'>
          <div className='l-content-header'>
            <div className='title-2' id='programs'>
              Мы хотели бы сделать вашу жизнь проще
            </div>
            <div className='subtitle-2'>
              Наши специальные программы помогают найти деньги на все
              необходимое
            </div>
          </div>
          <div className='programs'>{this.renderSpecialPrograms()}</div>
        </div>
      </div>
    )
  }
}

function Statistics ({
  issuedLoansTotal,
  issuedLoansToday,
  approvalPercentage
}) {
  return (
    <div className='l-stat'>
      <div className='l-content'>
        <div className='l-content-header'>
          <div className='title-2' id='why-we'>
            Почему именно мы
          </div>
          <div className='subtitle-2'>
            Мы подбираем и выдаем выгодные займы 24 часа в сутки 365 дней в году
          </div>
        </div>
        <div className='stat-list'>
          <div className='stat'>
            <div className='stat-title'>
              {acc.formatNumber(issuedLoansTotal)}
            </div>
            <div className='stat-subtitle'>всего займов выдано</div>
            <div className='stat-divider' />
          </div>
          <div className='stat'>
            <div className='stat-title'>
              {acc.formatNumber(issuedLoansToday)}
            </div>
            <div className='stat-subtitle'>займов выдано за сегодня</div>
            <div className='stat-divider' />
          </div>
          <div className='stat'>
            <div className='stat-title'>{`${acc.formatNumber(
              approvalPercentage
            )}%`}</div>
            <div className='stat-subtitle'>процент одобрения по заявкам</div>
          </div>
        </div>
      </div>
    </div>
  )
}

const commentsData = [
  {
    text: 'Процент, конечно, большой! Но так все равно спасибо, выручили.',
    author: (
      <span>
        <b>Михаил Мамонтов</b>, г. Екатеринодар
      </span>
    )
  },
  {
    text:
      'РЕБЯТА!ВЫ МОЛОДЦЫ, ОТРАБОТАЛИ КАК НАДО ВООБЩЕ. ПОБОЛЬШЕ БЫ ТАКИХ САЙТОВ.',
    author: (
      <span>
        <b>Петр Стуликов</b>, г. Тюмень
      </span>
    )
  },
  {
    text:
      'Что тут сказать? Хороши. Деньги-то  дают все, кому не лень. На любой остановке по три ларька. Но здесь и выбирать ничего не надо, раз, раз, заявку из дома скинула, и деньги приходят на карту.Спасибо!',
    author: (
      <span>
        <b>Анна Матвеева</b>, г. Тверь
      </span>
    )
  },
  {
    text:
      'Я в целом довольна, мне одобрили максимальную сумму по сервису, но хотелось бы больше, тысяч хотя бы 20-30 чтобы можно было взять. Как раз на небольшой отпуск, например, а то неудобно.',
    author: (
      <span>
        <b>Юлия Гордеева</b>, г. Иркутск
      </span>
    )
  },
  {
    text:
      'Который раз уже выручили деньгами.  Один раз кран в ванной поменял, потек, второй раз теще подарок на юбилей купил, сейчас вот зуб разболелся, сходил починил в платную. Я, конечно, не очень в интернете разбираюсь, но по-моему, здесь самые выгодные кредиты.',
    author: (
      <span>
        <b>Сергей Рыков</b>, г. Тольятти
      </span>
    )
  },
  {
    text:
      'Сегодня обратилась за займом и через полчаса получила перевод Контакт! Никаких проволочек!! Спасибо большое за помощь!',
    author: (
      <span>
        <b>Галина Макарова</b>, г. Смоленск
      </span>
    )
  },
  {
    text:
      'Сайт меня очень выручил! Получила деньги через систему CONTAKT на следующий день. В день погашения займа как назло задержали зарплату, я продлила срок займа и оплатила как смогла. Это очень удобно и быстро! Обращусь еще при случае.',
    author: (
      <span>
        <b>Алия Султанова</b>, г. Сыктывкар
      </span>
    )
  },
  {
    text:
      'Нужно было купить кое-что в интернете. Срочно - у них всегда же “акции”. А денег не хватало немного. Взял полторы тысячи в Finspin на ту же карту, где остальное лежало и заплатил. Погасил в срок - и никаких проблем.',
    author: (
      <span>
        <b>Игорь Биндер</b>, г. Саранск
      </span>
    )
  },
  {
    text:
      'Пожалуй самая практичная компания "Финспин", и деньги получил на руки оперативно, есть конечно переплата, но как и в любом банке, но я был готов переплатить чем заниматься бумажной бюрократией банках!',
    author: (
      <span>
        <b>Виталий Коуров</b>, г. Реж
      </span>
    )
  },
  {
    text:
      'И я хочу оставить хороший отзыв о Финспин, выручили меня уже второй раз, деньги получил через два часа, без проблем. Я доволен качеством обслуживания!',
    author: (
      <span>
        <b>Алексей Филинов</b>, г. Химки
      </span>
    )
  }
]

class Comments extends React.Component {
  handlePrev = () => {
    this.refs.swipe.prev()
  }

  handleNext = () => {
    this.refs.swipe.next()
  }

  render () {
    return (
      <div className='l-comments'>
        <div className='l-content-pad'>
          <div className='title-6'>
            73% клиентов обращаются в Finspin еще раз:
          </div>
          <div className='comment-controls'>
            <div
              className='comment-controls-prev'
              onClick={this.handlePrev}
              data-stat="click:Главная, кнопка 'Показать пред комментарий'"
            >
              <Icon name='arrow-left-icon-lg' />
            </div>

            <div
              className='comment-controls-next'
              onClick={this.handleNext}
              data-stat="click:Главная, кнопка 'Показать след комментарий'"
            >
              <Icon name='arrow-right-icon-lg' />
            </div>
          </div>
          <div className='l-comments-container'>
            <ReactSwipe
              ref='swipe'
              className='carousel'
              swipeOptions={{
                startSlide: random(commentsData.length - 1)
              }}
            >
              {commentsData.map((c, i) => (
                <div className='comment' key={i}>
                  <div className='comment-text'>{c.text}</div>
                  <div className='comment-author'>{c.author}</div>
                </div>
              ))}
            </ReactSwipe>
          </div>
        </div>
      </div>
    )
  }
}

function Mission () {
  return (
    <div className='l-mission'>
      <div className='content-pad'>
        <div className='mission'>
          <div className='mission-photo' />
          <div className='mission-content'>
            <div className='mission-title'>Наша миссия</div>
            <div className='title-3'>Finspin. Когда нужны деньги</div>
            <div className='mission-divider' />
            <p>
              Отовсюду реклама, реклама, реклама. Ставки, суммы, сроки,
              проценты, условия, документы… Разобраться в этом бывает непросто.
              А бывает просто некогда. Особенно если деньги нужны срочно. Мы
              сделали сервис, который делает сложное простым. Вы просто
              заполняете заявку и получаете деньги. Сколько вам нужно и когда
              вам нужно. И точно – выгодно!
            </p>
          </div>
        </div>
      </div>
    </div>
  )
}

const questions = [
  {
    question: '1. Сколько денег я получу здесь?',
    answer:
      'Пока максимальная сумма займа, который вы можете оформить через Finspin, равна 30 000 рублей. Однако не исключено, что скоро лимит будет увеличен.'
  },
  {
    question: '2. Как быстро я получу деньги?',
    answer:
      'Наш опыт показывает, что на заполнение анкеты уходит в среднем минут 10-12. Деньги зачисляются в течение еще 2-3 минут.'
  },
  {
    question: '3. Наверное, нужна куча документов?',
    answer:
      'Нет, для оформления заявки на микрозайм вам понадобится только общегражданский паспорт.'
  },
  {
    question:
      '4. А если у меня не идеальная кредитная история, мне дадут займ?',
    answer:
      'Идеальных кредитных историй не так много. Если у вас нет открытой просрочки, займ вполне возможен.'
  },
  {
    question: '5. Не хочу никуда ходить. Могу я получить деньги на карту?',
    answer:
      'Ходить никуда и не нужно. Мы предлагаем больше 10 удобных способов получить ваши деньги. Вы можете выбрать любой из них, в том числе карту.'
  },
  {
    question: '6. Могу ли я погасить займ досрочно?',
    answer:
      'Конечно. Более того, именно так мы и рекомендуем поступать. Ведь чем дольше вы платите по займу, тем выше переплата.'
  },
  {
    question: '7. Чем еще вы можете быть мне полезны?',
    answer: (
      <div>
        Мы можем предложить вам:
        <div>
          <div className='faq-subpoint'>
            <a
              href='http://mycreditinfo.ru/uluchshit_kreditnuyu_istoriyu/?utm_medium=referral&utm_source=Finspin&utm_campaign=HeadPage'
              target='_blank'
              className='link'
              data-stat="click:Главная, ссылка 'Перейти на сайт с улучшением кред. истории'"
            >
              Улучшение кредитной истории
            </a>
          </div>
        </div>
      </div>
    )
  }
]

class Question extends React.Component {
  static propTypes = {
    text: P.string.isRequired,
    answer: P.any.isRequired
  }

  state = {
    open: false
  }

  handleClick = () => {
    this.setState({ open: !this.state.open })
  }

  render () {
    return (
      <div className='faq-question'>
        <div
          className='faq-question-text'
          onClick={this.handleClick}
          data-stat={`click:Главная, вопрос '${this.props.text}'`}
        >
          {this.props.text}
        </div>
        <div className={cn('faq-answer', { 'is-open': this.state.open })}>
          {this.props.answer}
        </div>
      </div>
    )
  }
}

function FAQ () {
  return (
    <div className='l-faq'>
      <div className='l-content-pad'>
        <div className='title-2' id='faq'>
          Вопросы и ответы
        </div>
        <div className='faq-content'>
          {questions.map((q, i) => (
            <Question key={i} text={q.question} answer={q.answer} />
          ))}
        </div>
      </div>
    </div>
  )
}

function Upstair () {
  return (
    <div className='l-upstair'>
      <div className='l-content-pad'>
        <div className='title-2'>
          Мы готовы подобрать займ для вас прямо сейчас!
        </div>
        <div className='subtitle-2 subtitle-2-dark'>
          Заполните анкету и получите до <span className='no-wrap'>30 000</span>{' '}
          рублей всего за 10 минут
        </div>
        <div className='upstair-up-btn'>
          <button
            className='btn btn-lg btn-shadow'
            onClick={() => scrollTo('#base-loan')}
            data-stat="click:Главная, кнопка внизу страницы 'Получить деньги'"
          >
            Получить деньги
          </button>
        </div>
      </div>
    </div>
  )
}

function Footer ({ onOpenAgreement }) {
  return (
    <div className='l-footer'>
      <div className='l-content-pad'>
        <div className='footer-section'>
          <div className='footer-logo' />
          <div className='footer-slogan'>
            Подбираем микрозаймы на любые цели. До 30 000 рублей за 10 минут.
            Заявка онлайн. Выдача 24 часа в сутки.
          </div>
        </div>

        <div className='footer-section'>
          <div className='menu-old stack'>
            <MenuItems />
          </div>
        </div>

        <div className='footer-section'>
          <div className='about-phone'>
            <Link href={`tel:8${process.env.SUPPORT_PHONE}`}>
              {formatPhone(process.env.SUPPORT_PHONE, { firstDigit: '8' })}
            </Link>
          </div>
          <div className='about-phone-text'>
            С 9:00 до 18:00 по московскому времени звонок по России бесплатный
          </div>
          <br />
          <div className='email'>
            <span>Email:</span>{' '}
            <Link href='mailto:support@finspin.ru'>support@finspin.ru</Link>
          </div>
        </div>

        <div className='about'>{`Общество с ограниченной ответственностью «Финспин». Юридический адрес: 620075, г. Екатеринбург, ул. Мамина-Сибиряка, д. 101, офис 4.06. ОГРН 1146670012444, ИНН 6670424655.
        ООО «Финспин» сотрудничает только с надлежащим образом зарегистрированными МФО, состоящими в государственном реестре микрофинансовых организаций ЦБ РФ.`}</div>

        <Text color='secondary' type='caption' className='u-mt-1'>
          <Link onClick={onOpenAgreement}>
            Политика по обработке персональных данных
          </Link>
        </Text>

        <div className='about-copyright'>{`© 2016-${new Date().getFullYear()} ООО «Финспин»`}</div>
      </div>
    </div>
  )
}

export function Landing () {
  const { state, dispatch } = useContext(StoreContext)
  const { history } = useContext(RouterContext)

  useEffect(() => {
    if (!state.newLoanOptions) {
      dispatch(Actions.fetchNewLoanOptions())
    }
    if (!state.loanStatistics) {
      dispatch(Actions.fetchLoanStatistics())
    }
  }, [])

  useEffect(() => {
    changePage('main')
  }, [])

  useEffect(() => {
    if (/d=1/.test(document.location.search)) {
      scrollTo('#base-loan')
    }
  })

  function onGetSpecialOffer (options) {
    dispatch(Actions.openModal(ModalType.LoanCalculator, options))
  }

  function onSignIn () {
    dispatch(Actions.setWantedLoanConditions(null, null))
    history.push('/auth')
  }

  function onSignUp ({ amount, term }) {
    dispatch(Actions.setWantedLoanConditions(amount, term))
    history.push('/auth')
  }

  function onOpenAgreement () {
    dispatch(
      Actions.openModal(ModalType.Agreement, {
        vendor: AgreementVendor.Finspin,
        agreement: AgreementName.ProcessingPersonalDataPolicy
      })
    )
  }

  return (
    <div className='l-land'>
      <div className='bg-1'>
        <Header onLogin={onSignIn} />
        <LoanForm onSubmit={onSignUp} calc={state.newLoanOptions} />
      </div>
      <HowItWorks />
      <div className='bg-2'>
        <SpecPrograms
          onGetSpecialOffer={onGetSpecialOffer}
          calc={state.newLoanOptions}
          onSubmit={onSignUp}
        />
      </div>
      <Statistics {...state.loanStatistics} />
      <div className='bg-3'>
        <Comments />
        <Mission />
      </div>
      <FAQ />
      <Upstair />
      <Footer onOpenAgreement={onOpenAgreement} />
    </div>
  )
}
