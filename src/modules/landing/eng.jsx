import React, { useContext, useEffect } from 'react'
import P from 'prop-types'
import acc from 'accounting'
import moment from 'moment'

import { Icon, Link, CalcPlaceholder, Text } from '../../base'
import { LoanCalculator } from '../shared/loan_calculator'
import { scrollTo } from '../../helpers/dom_helpers'
import { formatPhone } from '../../helpers/format_helpers'
import { StoreContext, RouterContext } from '../../context'
import { ModalType } from '../../modules/modals/modal_type'
import { changePage } from '../../metrics'
import { AgreementVendor, AgreementName } from '../../domain'
import { Actions } from '../state'

moment.locale('en-EN')

function Safety ({ text }) {
  return (
    <div className='safety'>
      <div className='safety-icon'>
        <Icon name='lock-icon' />
      </div>
      <div className='safety-text'>{text}</div>
    </div>
  )
}

class ExpandedInfo extends React.Component {
  static propTypes = {
    caption: P.string.isRequired,
    children: P.any.isRequired
  }

  state = {
    open: false
  }

  handleClick = () => {
    this.setState(prevState => ({ open: !prevState.open }))
  }

  render () {
    return (
      <div className='expanded-info'>
        <div
          role='presentation'
          className='expanded-info__caption'
          onClick={this.handleClick}
        >
          {this.props.caption}
        </div>
        {this.state.open ? (
          <div className='expanded-info__content'>{this.props.children}</div>
        ) : null}
      </div>
    )
  }
}

function Header ({ onLogin }) {
  return (
    <div className='l-content-pad'>
      <div className='header landing-navbar'>
        <div className='landing-navbar--section'>
          <div className='landing-navbar--logo' />
        </div>
        <div>
          <button
            className='btn btn-outline btn-sm'
            onClick={onLogin}
            data-stat="click:Главная, кнопка 'Войти'"
          >
            Login
          </button>

          <a
            href={
              process.env.ENV !== 'production'
                ? 'https://test.finspin.ru/'
                : 'https://finspin.ru/'
            }
          >
            <button className='btn btn-link'>RU/EN</button>
          </a>
        </div>
      </div>
    </div>
  )
}

class LoanForm extends React.Component {
  static propTypes = {
    onSubmit: P.func.isRequired,
    calc: P.object
  }

  static defaultProps = {
    calc: null
  }

  render () {
    return (
      <div className='l-base-loan'>
        <div className='base-loan-title'>
          <div className='title-1'>Find a loan for any purpose</div>
        </div>
        <div className='base-loan-description'>
          <div className='base-loan-points'>
            <div className='base-loan-point'>
              <div className='base-loan-point-icon'>
                <Icon name='complete-icon' />
              </div>
              <div className='base-loan-point-text'>
                <b>Tell us how much you need</b> and for how long
              </div>
            </div>
            <div className='base-loan-point'>
              <div className='base-loan-point-icon'>
                <Icon name='complete-icon' />
              </div>
              <div className='base-loan-point-text'>
                <b>
                  Finspin compares loans and lenders for free and offers a loan
                  tailored for your needs
                </b>
              </div>
            </div>
            <div className='base-loan-point'>
              <div className='base-loan-point-icon'>
                <Icon name='complete-icon' />
              </div>
              <div className='base-loan-point-text'>
                <b>You get the money on the best terms</b>
              </div>
            </div>
          </div>
          <div className='safety-padding'>
            <Safety text='We strictly comply with the legislation of the Russian Federation. Hereinafter, your data is safe and secure.' />
          </div>
        </div>
        <div className='base-loan-form' id='base-loan'>
          {this.props.calc ? (
            <LoanCalculator
              onSubmit={this.props.onSubmit}
              {...this.props.calc}
            />
          ) : (
            <CalcPlaceholder />
          )}
        </div>
      </div>
    )
  }
}

function HowItWorks () {
  return (
    <div className='l-work'>
      <div className='l-content-pad l-content-line'>
        <div className='work-hand'>
          <Icon name='like-big-icon' />
        </div>

        <div className='work-description' id='how-it-work'>
          <div className='work-title'>
            <div className='title-2'>Microloan in 10 minutes</div>
            <div className='subtitle-2'>How it works</div>
          </div>

          <div className='work-point'>
            <div className='work-point-icon'>
              <Icon name='like-icon' />
            </div>
            <div className='work-point-text'>
              <div className='title-3'>Apply online</div>
              <p>
                Just a few simple steps. All you need is a passport and 8-10
                minutes of your time.
              </p>
            </div>
          </div>

          <div className='work-point'>
            <div className='work-point-icon'>
              <Icon name='like-icon' />
            </div>
            <div className='work-point-text'>
              <div className='title-3'>Get the best deal from Finspin</div>
              <p>
                The service will offer you the best loan with lower interest
                rates.
              </p>
            </div>
          </div>

          <div className='work-point'>
            <div className='work-point-icon'>
              <Icon name='like-icon' />
            </div>
            <div className='work-point-text'>
              <div className='title-3'>Take your money</div>
              <p>
                More than 10 ways to get the money: card, bank account, cash.
              </p>
            </div>
          </div>

          <Safety text='We work with trusted lenders only. Each of them is registeres by the Central Bank of Russia and works in compliance with all regulatory acts and regulations.' />

          <ExpandedInfo caption='The list of MFIs we work with'>
            <div>
              <span className='no-wrap'>OOO MFK SMSFINANCE,</span>{' '}
              <span className='no-wrap'>OOO MFK Zaim Online,</span>{' '}
              <span className='no-wrap'>OOO MFK Kreditech Rus,</span>{' '}
              <span className='no-wrap'>OOO MKK AN BusinesInvest,</span>{' '}
              <span>
                OOO MFK Mikrokreditnaya Kompaniya Universalnogo Finansirovaniya,
              </span>{' '}
              <span>OOO MFK Ecofinance,</span> <span>OOO MFK Webbankir,</span>{' '}
              <span>OOO MFK Money Man,</span> <span>OOO MKK POLUSHKA,</span>{' '}
              <span>OOO MFK DZP-Center,</span> <span>OOO MFK MigCredit</span>{' '}
              and <span className='no-wrap'>OOO MKK Rusinterfinance</span>
            </div>
          </ExpandedInfo>
        </div>
      </div>
    </div>
  )
}

function Statistics ({
  issuedLoansTotal,
  issuedLoansToday,
  approvalPercentage
}) {
  return (
    <div className='l-stat'>
      <div className='l-content'>
        <div className='l-content-header'>
          <div className='title-2' id='why-we'>
            Why us?
          </div>
          <div className='subtitle-2'>
            We help our clients to find the best conditions 24 hours a day 365
            days a year
          </div>
        </div>
        <div className='stat-list'>
          <div className='stat'>
            <div className='stat-title'>
              {acc.formatNumber(issuedLoansTotal)}
            </div>
            <div className='stat-subtitle'>TOTAL LOANS ISSUED</div>
            <div className='stat-divider' />
          </div>
          <div className='stat'>
            <div className='stat-title'>
              {acc.formatNumber(issuedLoansToday)}
            </div>
            <div className='stat-subtitle'>LOANS ISSUED TODAY</div>
            <div className='stat-divider' />
          </div>
          <div className='stat'>
            <div className='stat-title'>{`${acc.formatNumber(
              approvalPercentage
            )}%`}</div>
            <div className='stat-subtitle'>APPROVAL RATE</div>
          </div>
        </div>
      </div>
    </div>
  )
}

function Upstair () {
  return (
    <div className='l-upstair'>
      <div className='l-content-pad'>
        <div className='title-2'>
          We are ready to find a loan for you right now!
        </div>
        <div className='subtitle-2 subtitle-2-dark'>
          Fill in the application and receive up to{' '}
          <span className='no-wrap'>30 000</span> rubles in 10 minutes
        </div>
        <div className='upstair-up-btn'>
          <button
            className='btn btn-lg btn-shadow'
            onClick={() => scrollTo('#base-loan')}
            data-stat="click:Главная, кнопка внизу страницы 'Получить деньги'"
          >
            Apply now
          </button>
        </div>
      </div>
    </div>
  )
}

function Footer ({ onOpenAgreement }) {
  return (
    <div className='l-footer'>
      <div className='l-content-pad'>
        <div className='footer-section'>
          <div className='footer-logo' />
          <div className='footer-slogan'>
            We find loans for any purpose in 10 minutes. Online application.
            24/7 disbursement.
          </div>
        </div>

        <div className='footer-section'>
          <div className='about-phone'>
            <Link href={`tel:8(800) 555-87-16`}>
              {formatPhone('8005558716', { firstDigit: '8' })}
            </Link>
          </div>
          <div className='about-phone-text'>
            From 9:00 a.m. to 06:00 p.m. Moscow time, a call in Russia is free
          </div>
          <br />
          <div className='email'>
            <span>Email:</span>{' '}
            <Link href='mailto:support@finspin.ru'>support@finspin.ru</Link>
          </div>
        </div>

        <div className='about'>
          Limited liability company Finspin. Legal address: 620075,
          Yekaterinburg, ul. Mamina-Sibiryaka, 101, office 4.06. OGRN
          1146670012444, INN 6670424655. ООО Finspin only cooperates with MFIs
          properly registered in the state register of microfinance
          organizations of the Central Bank of the Russian Federation.
        </div>

        <Text color='secondary' type='caption' className='u-mt-1'>
          <Link onClick={onOpenAgreement}>Privacy Policy</Link>
        </Text>

        <div className='about-copyright'>{`© 2016-${new Date().getFullYear()} ООО Finspin`}</div>
      </div>
    </div>
  )
}

export function Landing () {
  const { state, dispatch } = useContext(StoreContext)
  const { history } = useContext(RouterContext)

  useEffect(() => {
    if (!state.newLoanOptions) {
      dispatch(Actions.fetchNewLoanOptions())
    }
    if (!state.loanStatistics) {
      dispatch(Actions.fetchLoanStatistics())
    }
  }, [])

  useEffect(() => {
    changePage('main')
  }, [])

  useEffect(() => {
    if (/d=1/.test(document.location.search)) {
      scrollTo('#base-loan')
    }
  })

  function onSignIn () {
    dispatch(Actions.setWantedLoanConditions(null, null))
    history.push('/auth')
  }

  function onSignUp ({ amount, term }) {
    dispatch(Actions.setWantedLoanConditions(amount, term))
    history.push('/auth')
  }

  function onOpenAgreement () {
    dispatch(
      Actions.openModal(ModalType.Agreement, {
        vendor: AgreementVendor.Finspin,
        agreement: AgreementName.ProcessingPersonalDataPolicy
      })
    )
  }

  return (
    <div className='l-land'>
      <div className='bg-1'>
        <Header onLogin={onSignIn} />
        <LoanForm onSubmit={onSignUp} calc={state.newLoanOptions} />
      </div>
      <HowItWorks />
      <Statistics {...state.loanStatistics} />
      <Upstair />
      <Footer onOpenAgreement={onOpenAgreement} />
    </div>
  )
}
