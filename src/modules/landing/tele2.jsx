import React, {Component, useContext, useEffect} from 'react'
import P from 'prop-types'
import cn from 'classnames'
import acc from 'accounting'
import moment from 'moment'

import {Icon, Link, CalcPlaceholder, Text} from '../../base'
import {LoanCalculator} from '../shared/loan_calculator'
import {scrollTo} from '../../helpers/dom_helpers'
import {formatPhone} from '../../helpers/format_helpers'
import {StoreContext, RouterContext} from '../../context'
import {ModalType} from '../../modules/modals/modal_type'
import {changePage} from '../../metrics'
import {AgreementVendor, AgreementName} from '../../domain'
import {Actions} from '../../modules/state'

moment.locale('ru-RU')

function Safety ({text}) {
  return (
    <div className='safety'>
      <div className='safety-icon'>
        <Icon name='lock-icon' />
      </div>
      <div className='safety-text'>{text}</div>
    </div>
  )
}

class ExpandedInfo extends React.Component {
  static propTypes = {
    caption: P.string.isRequired,
    children: P.any.isRequired
  }

  state = {
    open: false
  }

  handleClick = () => {
    this.setState(prevState => ({open: !prevState.open}))
  }

  render () {
    return (
      <div className='expanded-info'>
        <div
          role='presentation'
          className='expanded-info__caption'
          onClick={this.handleClick}
        >
          {this.props.caption}
        </div>
        {this.state.open ? (
          <div className='expanded-info__content'>{this.props.children}</div>
        ) : null}
      </div>
    )
  }
}

function MenuItems () {
  function scroll (ev) {
    scrollTo(ev.currentTarget.getAttribute('data-scrollto'))
  }

  return (
    <div className='landing-menu-items'>
      <div className='landing-menu-item-container'>
        <div
          className='landing-menu-item'
          data-scrollto='#how-it-work'
          data-stat="click:Главная, меню 'Как это работает'"
          onClick={scroll}
        >
          Как это работает
        </div>
      </div>
      <div className='landing-menu-item-container'>
        <div
          className='landing-menu-item'
          data-scrollto='#programs'
          data-stat="click:Главная, меню 'Спецпрограммы'"
          onClick={scroll}
        >
          Спецпрограммы
        </div>
      </div>
      <div className='landing-menu-item-container'>
        <div
          className='landing-menu-item'
          data-scrollto='#why-we'
          data-stat="click:Главная, меню 'Почему мы'"
          onClick={scroll}
        >
          Почему мы
        </div>
      </div>
      <div className='landing-menu-item-container'>
        <div
          className='landing-menu-item'
          data-scrollto='#faq'
          data-stat="click:Главная, меню 'Вопросы и ответы'"
          onClick={scroll}
        >
          Вопросы и ответы
        </div>
      </div>
    </div>
  )
}

function UpHeader () {
  return (
    <div className='up-header'>
      <div className='up-header-content'>
        <div className='up-header-prunto-logo prunto-logo' />
        <div className='up-header-description'>
          Сервис подбора займов предоставлен партнером
        </div>
      </div>
    </div>
  )
}

function Header () {
  return (
    <div className='header-bg'>
      <div className='l-content-pad'>
        <div className='landing-header'>
          <div>
            <div className='landing-menu'>
              <MenuItems />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

class LoanForm extends Component {
  static propTypes = {
    onSubmit: P.func.isRequired,
    onSignIn: P.func.isRequired,
    calc: P.object // eslint-disable-line react/forbid-prop-types
  }

  static defaultProps = {
    calc: null
  }

  render () {
    return (
      <div className='l-base-loan'>
        <div className='base-loan-title'>
          <div className='title-2'>
            Подбор займов на персональных условиях для клиентов Tele2
          </div>
        </div>
        <div className='base-loan-description'>
          <div className='base-loan-points'>
            <div className='base-loan-point'>
              <div className='base-loan-point-icon'>
                <Icon name='complete-icon' />
              </div>
              <div className='base-loan-point-text'>
                <b>Скажите, сколько денег вам нужно</b>, и на какой срок
              </div>
            </div>

            <div className='base-loan-point'>
              <div className='base-loan-point-icon'>
                <Icon name='complete-icon' />
              </div>
              <div className='base-loan-point-text'>
                <b>Мы бесплатно сравним предложения</b> нескольких кредиторов, и подберем
                займ конкретно для вас
              </div>
            </div>

            <div className='base-loan-point'>
              <div className='base-loan-point-icon'>
                <Icon name='complete-icon' />
              </div>
              <div className='base-loan-point-text'>
                <b>Вы получите деньги</b> на оптимальных условиях
              </div>
            </div>
          </div>

          <div className='safety-padding'>
            <Safety text='Мы работаем в строгом соответствии с законодательством РФ. Здесь и далее ваши данные надежно защищены.' />
          </div>
        </div>
        <div
          className={`base-loan-form ${
            this.props.calc ? '' : 'base-loan-form--is-calc-pending'
          }`}
          id='base-loan'
        >
          {this.props.calc ? (
            <LoanCalculator
              onSubmit={this.props.onSubmit}
              {...this.props.calc}
            />
          ) : (
            <CalcPlaceholder />
          )}
          {this.props.calc ? (
            <div className='base-loan-form-login'>
              <Link onClick={this.props.onSignIn}>Я уже заполнял заявку</Link>
            </div>
          ) : null}
        </div>
      </div>
    )
  }
}

function HowItWorks () {
  return (
    <div className='l-work'>
      <div className='l-content-pad l-content-line'>
        <div className='work-hand'>
          <Icon name='like-big-icon' />
        </div>
        <div className='work-description' id='how-it-work'>
          <div className='work-title'>
            <div className='line-container'>
              <div className='title-2 line'>Деньги за 10 минут</div>
            </div>
            <div className='subtitle-2'>Как это работает</div>
          </div>
          <div className='work-point'>
            <div className='work-point-icon'>
              <Icon name='like-icon' />
            </div>
            <div className='work-point-text'>
              <div className='title-3'>Заполните анкету онлайн</div>
              <p>
                Всего несколько простых полей. Нужен только паспорт и 8–10 минут
                вашего времени.
              </p>
            </div>
          </div>
          <div className='work-point'>
            <div className='work-point-icon'>
              <Icon name='like-icon' />
            </div>
            <div className='work-point-text'>
              <div className='title-3'>
                Получите предложение от Tele2 и Finspin
              </div>
              <p>
                Сервис предложит вам оптимальный займ из всех найденных у
                кредиторов.
              </p>
            </div>
          </div>
          <div className='work-point'>
            <div className='work-point-icon'>
              <Icon name='like-icon' />
            </div>
            <div className='work-point-text'>
              <div className='title-3'>Заберите ваши деньги</div>
              <p>
                Больше 10 способов получить деньги, как вам будет удобней: на
                карту, на счет, наличными.
              </p>
            </div>
          </div>
          <Safety text='Мы сотрудничаем только с проверенными кредиторами. Каждый из них состоит в реестре ЦБ РФ, и работает с соблюдением всех нормативных актов и предписаний.' />
          <ExpandedInfo caption='Список МФО, предоставляющих услуги по выдаче и обслуживанию займов'>
            <div>
              <span className='no-wrap'>ООО МФК «СМСФИНАНС»,</span>{' '}
              <span className='no-wrap'>ООО МФК «Займ Онлайн»,</span>{' '}
              <span className='no-wrap'>ООО МФК «Кредитех Рус»,</span>{' '}
              <span className='no-wrap'>ООО МКК АН «БизнесИнвест»,</span>{' '}
              <span>
                ООО МФК «'Микрокредитная Компания Универсального
                Финансирования»,
              </span>{' '}
              <span>ООО МФК «Экофинанс»,</span>{' '}
              <span>ООО МФК «ВЭББАНКИР»,</span> <span>ООО МФК «Мани Мен»,</span>{' '}
              <span>ООО МКК «ПОЛУШКА»,</span>{' '}
              <span>ООО МКК АН «БизнесИнвест»,</span>{' '}
              <span>ООО МФК «ДЗП-Центр»,</span>{' '}
              <span>ООО МФК «МигКредит»</span> и{' '}
              <span className='no-wrap'>ООО МКК «Русинтерфинанс»</span>
            </div>
          </ExpandedInfo>
        </div>
      </div>
    </div>
  )
}

class SpecPrograms extends Component {
  static propTypes = {
    onGetSpecialOffer: P.func.isRequired,
    calc: P.object, // eslint-disable-line react/forbid-prop-types
    onSubmit: P.func.isRequired
  }

  static defaultProps = {
    calc: null
  }

  handleGetSpecialOffer (title, description) {
    this.props.onGetSpecialOffer({
      description,
      title: `Спецпрограмма «${title}»`,
      calc: this.props.calc,
      onSubmit: this.props.onSubmit
    })
  }

  renderSpecialPrograms () {
    const programs = [
      [
        'На любые цели',
        'Чтобы ничего не мешало вам принять правильное решение, когда это будет нужно',
        true
      ],
      [
        'На путешествие',
        'Помогает получить больше ярких впечатлений, куда бы вы ни отправились'
      ],
      [
        'На подарки близким',
        'Дает возможность проявить максимум внимания к тем, кого вы действительно любите'
      ],
      [
        'На срочную покупку',
        'Позволяет уже сегодня получить то, на что пришлось бы копить пару месяцев'
      ],
      [
        'На оплату услуг',
        'Чтобы оплатить квитанции за ЖКХ, закрыть счета из банков, от физических лиц и организаций'
      ],
      [
        'На ремонт автомобиля',
        'Никакие обстоятельства не должны отнимать у вас права на «свободу передвижения»'
      ]
    ]

    return programs.map(([title, description, isHit], index) => (
      <div className='program' key={index}>
        {isHit && (
          <div className='program-hit'>
            <div className='program-hit-icon'>
              <Icon name='like-outline-icon' />
            </div>
            <div className='program-hit-caption'>хит</div>
          </div>
        )}
        <div className={`program-image program-${index + 1}`} />
        <div className='program-content'>
          <div className='title-3'>{title}</div>
          <p>{description}</p>
          <button
            className='btn btn-outline'
            onClick={() => this.handleGetSpecialOffer(title, description)}
          >
            Получить деньги
          </button>
        </div>
      </div>
    ))
  }

  render () {
    return (
      <div className='l-programs'>
        <div className='l-content'>
          <div className='l-content-header'>
            <div className='line-container'>
              <div className='title-1 line' id='programs'>
                Мы делаем вашу жизнь проще
              </div>
            </div>
            <div className='subtitle-2'>
              Наши специальные программы помогают найти деньги на все
              необходимое
            </div>
          </div>
          <div className='programs'>{this.renderSpecialPrograms()}</div>
        </div>
      </div>
    )
  }
}

function Statistics ({issuedLoansTotal, issuedLoansToday, approvalPercentage}) {
  return (
    <div className='l-stat'>
      <div className='l-content'>
        <div className='l-content-header'>
          <div className='title-2' id='why-we'>
            Почему именно мы
          </div>
          <div className='subtitle-2'>
            Мы подбираем займы для абонентов Tele2 24 часа в сутки и 365 дней в
            году
          </div>
        </div>
        <div className='stat-list'>
          <div className='stat'>
            <div className='stat-title'>
              {acc.formatNumber(issuedLoansTotal)}
            </div>
            <div className='stat-subtitle'>всего займов выдано</div>
            <div className='stat-divider' />
          </div>
          <div className='stat'>
            <div className='stat-title'>
              {acc.formatNumber(issuedLoansToday)}
            </div>
            <div className='stat-subtitle'>займов выдано за сегодня</div>
            <div className='stat-divider' />
          </div>
          <div className='stat'>
            <div className='stat-title'>{`${acc.formatNumber(
              approvalPercentage
            )}%`}</div>
            <div className='stat-subtitle'>процент одобрения по заявкам</div>
          </div>
        </div>
      </div>
    </div>
  )
}

const questions = [
  {
    question: '1. Сколько денег я получу здесь?',
    answer:
      'Пока максимальная сумма займа, который вы можете оформить через Finspin, равна 30 000 рублей. Однако не исключено, что скоро лимит будет увеличен.'
  },
  {
    question: '2. Как быстро я получу деньги?',
    answer:
      'Наш опыт показывает, что на заполнение анкеты уходит в среднем минут 10-12. Деньги зачисляются в течение еще 2-3 минут.'
  },
  {
    question: '3. Наверное, нужна куча документов?',
    answer:
      'Нет, для оформления заявки на микрозайм вам понадобится только общегражданский паспорт.'
  },
  {
    question:
      '4. А если у меня не идеальная кредитная история, мне дадут займ?',
    answer:
      'Идеальных кредитных историй не так много. Если у вас нет открытой просрочки, займ вполне возможен.'
  },
  {
    question: '5. Не хочу никуда ходить. Могу я получить деньги на карту?',
    answer:
      'Ходить никуда и не нужно. Мы предлагаем больше 10 удобных способов получить ваши деньги. Вы можете выбрать любой из них, в том числе карту.'
  },
  {
    question: '6. Могу ли я погасить займ досрочно?',
    answer:
      'Конечно. Более того, именно так мы и рекомендуем поступать. Ведь чем дольше вы платите по займу, тем выше переплата.'
  }
]

class Question extends Component {
  static propTypes = {
    text: P.string.isRequired,
    answer: P.any.isRequired
  }

  state = {
    open: false
  }

  handleClick = () => {
    this.setState({open: !this.state.open})
  }

  render () {
    return (
      <div className='faq-question'>
        <div
          className='faq-question-handler'
          onClick={this.handleClick}
          data-stat={`click:Главная, вопрос '${this.props.text}'`}
        >
          <div className='faq-question-text'> {this.props.text} </div>
          <div className='faq-question-arrow'>
            {this.state.open ? (
              <Icon name='caret-up-icon' />
            ) : (
              <Icon name='caret-down-icon' />
            )}
          </div>
        </div>
        <div className={cn('faq-answer', {'is-open': this.state.open})}>
          {this.props.answer}
        </div>
      </div>
    )
  }
}

function FAQ () {
  return (
    <div className='l-faq'>
      <div className='l-content-pad'>
        <div className='line-container'>
          <div className='title-2 line' id='faq'>
            Вопросы и ответы
          </div>
        </div>
        <div className='faq-content'>
          {questions.map((q, i) => (
            <Question key={i} text={q.question} answer={q.answer} />
          ))}
        </div>
      </div>
    </div>
  )
}

function Upstair () {
  return (
    <div className='l-upstair'>
      <div className='l-content-pad'>
        <div className='title-2-5'>
          Мы готовы подобрать займ для вас прямо сейчас!
        </div>
        <div className='subtitle-2'>
          Заполните анкету и получите до{' '}
          <span className='no-wrap'>30 000</span> рублей всего за 10 минут
        </div>
        <div className='upstair-up-btn'>
          <button
            className='btn is-primary'
            onClick={() => scrollTo('#base-loan')}
            data-stat="click:Главная, кнопка внизу страницы 'Получить деньги'"
          >
            Получить деньги
          </button>
        </div>
      </div>
    </div>
  )
}

function Footer ({onOpenAgreement}) {
  return (
    <div className='l-footer'>
      <div className='landing-menu menu-footer'>
        <MenuItems />
      </div>
      <div className='footer l-content-pad'>
        <div className='footer-phone'>
          <div className='footer-phone-value'>
            <Link href={`tel:8${process.env.SUPPORT_PHONE}`}>
              {formatPhone(process.env.SUPPORT_PHONE, {firstDigit: '8'})}
            </Link>
          </div>
          <div className='footer-phone-text'>
            С 9:00 до 18:00 по московскому времени звонок по России бесплатный
          </div>
          <div className='footer-email'>
            <span className='footer-email-caption'>Email: </span>
            <span className='footer-email-link'>
              <Link
                href='mailto:support@finspin.ru'
                className='footer-email-link'
              >
                support@finspin.ru
              </Link>
            </span>
          </div>
        </div>
        <div className='footer-about'>
          <div className='footer-copyright'>{`© 2016-${new Date().getFullYear()} ООО «Финспин»`}</div>
          <div className='footer-company'>
            Общество с ограниченной ответственностью «Финспин». Юридический
            адрес: 620075, г. Екатеринбург, ул. Мамина-Сибиряка, д. 101, офис
            4.06. ОГРН 1146670012444, ИНН 6670424655. ООО «Финспин»
            сотрудничает только с надлежащим образом зарегистрированными МФО,
            состоящими в государственном реестре микрофинансовых организаций ЦБ
            РФ.
          </div>
          <Text color='secondary' type='caption' className='u-mt-1'>
            <Link onClick={onOpenAgreement} className='footer-email-link'>
              Политика по обработке персональных данных
            </Link>
          </Text>
        </div>
      </div>
    </div>
  )
}

export function Landing () {
  const {state, dispatch} = useContext(StoreContext)
  const {history} = useContext(RouterContext)

  useEffect(() => {
    if (!state.newLoanOptions) {
      dispatch(Actions.fetchNewLoanOptions())
    }
    if (!state.loanStatistics) {
      dispatch(Actions.fetchLoanStatistics())
    }
  }, [])

  useEffect(() => {
    changePage('main')
  }, [])

  useEffect(() => {
    if (/d=1/.test(document.location.search)) {
      scrollTo('#base-loan')
    }
  })

  function onGetSpecialOffer (options) {
    dispatch(Actions.openModal(ModalType.LoanCalculator, options))
  }

  function onSignIn () {
    dispatch(Actions.setWantedLoanConditions(null, null))
    history.push('/auth')
  }

  function onSignUp ({amount, term}) {
    dispatch(Actions.setWantedLoanConditions(amount, term))
    history.push('/auth')
  }

  function onOpenAgreement () {
    dispatch(Actions.openModal(ModalType.Agreement, {
      vendor: AgreementVendor.Finspin,
      agreement: AgreementName.ProcessingPersonalDataPolicy
    }))
  }

  return (
    <div className='l-land'>
      <UpHeader />
      <div className='bg-1'>
        <Header />
        <LoanForm onSubmit={onSignUp} onSignIn={onSignIn} calc={state.newLoanOptions} />
      </div>
      <HowItWorks />
      <div className='bg-2'>
        <SpecPrograms
          onGetSpecialOffer={onGetSpecialOffer}
          calc={state.newLoanOptions}
          onSubmit={onSignUp}
        />
      </div>
      {state.loanStatistics ? <Statistics {...state.loanStatistics} /> : null}
      <FAQ />
      <Upstair />
      <Footer onOpenAgreement={onOpenAgreement} />
    </div>
  )
}
