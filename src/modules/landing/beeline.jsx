import React, {Component, useContext, useEffect} from 'react'
import P from 'prop-types'
import moment from 'moment'

import {
  ContentBeforeFooter,
  ContentWithFooter,
  Footer,
  Link,
  CalcPlaceholder,
  Text
} from '../../base'
import {LoanCalculator} from '../shared/loan_calculator'
import {scrollTo} from '../../helpers/dom_helpers'
import {formatPhone} from '../../helpers/format_helpers'
import {StoreContext, RouterContext} from '../../context'
import {ModalType} from '../../modules/modals/modal_type'
import {changePage} from '../../metrics'
import {AgreementVendor, AgreementName} from '../../domain'
import {Actions} from '../../modules/state'

moment.locale('ru-RU')

function Header () {
  return (
    <div className='l-content-pad'>
      <div className='header'>
        <div className='beeline-logo' />
        <div className='prunto-logo' />
      </div>
    </div>
  )
}

class ExpandedInfo extends React.Component {
  static propTypes = {
    caption: P.string.isRequired,
    children: P.any.isRequired
  }

  state = {
    open: false
  }

  handleClick = () => {
    this.setState(prevState => ({open: !prevState.open}))
  }

  render () {
    return (
      <div className='expanded-info'>
        <div
          role='presentation'
          className='expanded-info__caption'
          onClick={this.handleClick}
        >
          {this.props.caption}
        </div>
        {this.state.open ? (
          <div className='expanded-info__content'>{this.props.children}</div>
        ) : null}
      </div>
    )
  }
}

class LoanForm extends Component {
  static propTypes = {
    onSubmit: P.func.isRequired,
    onSignIn: P.func.isRequired,
    calc: P.object
  }

  static defaultProps = {
    calc: null
  }

  render () {
    return (
      <div className='l-base-loan'>
        <div className='base-loan-title'>
          <div className='title-1'>Получите займ на любые цели</div>
        </div>
        <div className='base-loan-form' id='base-loan'>
          {this.props.calc ? (
            <LoanCalculator
              onSubmit={this.props.onSubmit}
              {...this.props.calc}
            />
          ) : (
            <CalcPlaceholder />
          )}
          {this.props.calc ? (
            <div className='base-loan-form-login'>
              <Link onClick={this.props.onSignIn}>Я уже заполнял заявку</Link>
            </div>
          ) : null}
        </div>
        <ExpandedInfo caption='Список МФО, предоставляющих услуги по выдаче и обслуживанию займов'>
          <div>
            <span className='no-wrap'>ООО МФК «СМСФИНАНС»,</span>{' '}
            <span className='no-wrap'>ООО МФК «Займ Онлайн»,</span>{' '}
            <span className='no-wrap'>ООО МФК «Кредитех Рус»,</span>{' '}
            <span className='no-wrap'>ООО МКК АН «БизнесИнвест»,</span>{' '}
            <span>
              ООО МФК «'Микрокредитная Компания Универсального Финансирования»,
            </span>{' '}
            <span>ООО МФК «Экофинанс»,</span> <span>ООО МФК «ВЭББАНКИР»,</span>{' '}
            <span>ООО МФК «Мани Мен»,</span> <span>ООО МКК «ПОЛУШКА»,</span>{' '}
            <span>ООО МКК АН «БизнесИнвест»,</span>{' '}
            <span>ООО МФК «ДЗП-Центр»,</span>{' '}
            <span>ООО МФК «МигКредит»</span> и{' '}
            <span className='no-wrap'>ООО МКК «Русинтерфинанс»</span>
          </div>
        </ExpandedInfo>
      </div>
    )
  }
}

function FooterContent ({onOpenAgreement}) {
  return (
    <div className='l-footer l-content-pad'>
      <div className='l-about-text'>
        Общество с ограниченной ответственностью «Финспин». Юридический адрес:
        620075, г. Екатеринбург, ул. Мамина-Сибиряка, д. 101, офис 4.06. ОГРН
        1146670012444, ИНН 6670424655. ООО «Финспин» сотрудничает только с
        надлежащим образом зарегистрированными МФО, состоящими в государственном
        реестре микрофинансовых организаций ЦБ РФ.
      </div>

      <Text color='secondary' type='caption' className='u-mt-1 u-mb-2'>
        <Link onClick={onOpenAgreement}>
          Политика по обработке персональных данных
        </Link>
      </Text>
      <div className='l-about-phone'>
        Служба поддержки ,
        <Link href={`tel:8${process.env.SUPPORT_PHONE}`}>
          {formatPhone(process.env.SUPPORT_PHONE, {firstDigit: '8'})}
        </Link>
      </div>
      <div className='l-about-phone'>
        <span>Email </span>
        <Link href='mailto:support@finspin.ru'>support@finspin.ru</Link>
      </div>
    </div>
  )
}

export function Landing () {
  const {state, dispatch} = useContext(StoreContext)
  const {history} = useContext(RouterContext)

  useEffect(() => {
    if (!state.newLoanOptions) {
      dispatch(Actions.fetchNewLoanOptions())
    }
  }, [])

  useEffect(() => {
    changePage('main')
  }, [])

  useEffect(() => {
    if (/d=1/.test(document.location.search)) {
      scrollTo('#base-loan')
    }
  })

  function onSignIn () {
    dispatch(Actions.setWantedLoanConditions(null, null))
    history.push('/auth')
  }

  function onSignUp ({amount, term}) {
    dispatch(Actions.setWantedLoanConditions(amount, term))
    history.push('/auth')
  }

  function onOpenAgreement () {
    dispatch(Actions.openModal(ModalType.Agreement, {
      vendor: AgreementVendor.Finspin,
      agreement: AgreementName.ProcessingPersonalDataPolicy
    }))
  }

  return (
    <div className='l-land'>
      <ContentWithFooter>
        <ContentBeforeFooter>
          <Header />
          <LoanForm
            onSubmit={onSignUp}
            onSignIn={onSignIn}
            calc={state.newLoanOptions}
          />
        </ContentBeforeFooter>
        <Footer>
          <FooterContent onOpenAgreement={onOpenAgreement} />
        </Footer>
      </ContentWithFooter>
    </div>
  )
}
