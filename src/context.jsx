import React, {useState, useEffect} from 'react'
import {BrowserRouter, StaticRouter, withRouter} from 'react-router-dom'

export const StoreContext = React.createContext()
export const RouterContext = React.createContext()

export function StoreProvider ({store, children}) {
  const [value, setState] = useState(store.getState())

  useEffect(() => {
    store.subscribe(() => setState(store.getState()))
  }, [])

  return (
    <StoreContext.Provider value={{state: value, dispatch: store.dispatch}}>
      {children}
    </StoreContext.Provider>
  )
}

const RouterWrapper = withRouter(function RouterWrapper ({history, location, match, children}) {
  return (
    <RouterContext.Provider value={{history, location, match}} >
      {children}
    </RouterContext.Provider>
  )
})

export function RouterProvider ({children}) {
  return (
    <BrowserRouter>
      <RouterWrapper>
        {children}
      </RouterWrapper>
    </BrowserRouter>
  )
}

export function StaticRouterProvider ({context, location, children}) {
  return (
    <StaticRouter context={context} location={location}>
      <RouterWrapper>
        {children}
      </RouterWrapper>
    </StaticRouter>
  )
}
