import test from 'tape'
import {
  errors,
  noError,
  isConfirmationCode,
  isDepartmentCode,
  isEmail,
  isFirstName,
  isFirstNameLastName,
  isLastName,
  isMiddleName,
  isPassportSN,
  isPhone,
  isEmpty,
  isTextFieldDate
} from './validation'

test('Required validation', (t) => {
  t.is(isEmpty(undefined), errors.required)
  t.is(isEmpty(null), errors.required)
  t.is(isEmpty(''), errors.required)
  t.is(isEmpty({}), noError)
  t.is(isEmpty(0), noError)

  t.end()
})

test('Phone validation', (t) => {
  t.is(isPhone('123'), errors.phone)
  t.is(isPhone('abc'), errors.phone)
  t.is(isPhone('1234567890'), errors.phone)
  t.is(isPhone('+7 (123) 456-78-90'), noError)
  t.is(isPhone('8 (123) 456-78-90'), noError)

  t.end()
})

test('FirstName with LastName validation', (t) => {
  t.is(isFirstNameLastName('Bruce Wayne'), errors.firstNameLastName)
  t.is(isFirstNameLastName('Брюс'), errors.firstNameLastName)
  t.is(isFirstNameLastName('1234567890'), errors.firstNameLastName)
  t.is(isFirstNameLastName('Брюс 123'), errors.firstNameLastName)
  t.is(isFirstNameLastName('Брюс Уэйн'), noError)
  t.is(isFirstNameLastName('Брюс Уэйн '), noError)
  t.is(isFirstNameLastName('Раз Два Три'), noError)
  t.is(isFirstNameLastName('Раз Два-Три '), noError)
  t.end()
})

test('Confirmation code validation', (t) => {
  t.equals(isConfirmationCode('1234'), noError)
  t.equals(isConfirmationCode('1 2 3 4'), noError)
  t.equals(isConfirmationCode('123'), errors.confirmationCode)
  t.end()
})

test('First name validation', (t) => {
  t.equals(isFirstName('Абв'), noError)
  t.equals(isFirstName('Абв '), noError)
  t.equals(isFirstName('Абв0'), errors.firstName)
  t.end()
})

test('Middle name validation', (t) => {
  t.equals(isMiddleName('Абв'), noError)
  t.equals(isMiddleName('Абв'), noError)
  t.equals(isMiddleName('Абв Гд'), noError)
  t.equals(isMiddleName('Абв0Гд'), errors.middleName)
  t.end()
})

test('Last name validation', (t) => {
  t.equals(isLastName('Абв'), noError)
  t.equals(isLastName('Абв гд'), noError)
  t.equals(isLastName('Абв-гд'), noError)
  t.equals(isLastName('Абв0гд'), errors.lastName)
  t.end()
})

test('Text field date validation', (t) => {
  t.equals(isTextFieldDate('10.10.2010'), noError)
  t.equals(isTextFieldDate('10.10.20100'), errors.textFieldDate)
  t.end()
})

test('Passport SN validation', (t) => {
  t.equals(isPassportSN('0000 123456'), noError)
  t.equals(isPassportSN('000 123456'), errors.passportSN)
  t.equals(isPassportSN('0000 12345'), errors.passportSN)
  t.end()
})

test('Email validation', (t) => {
  t.equals(isEmail('a@m.com'), noError)
  t.equals(isEmail('a@m'), errors.email)
  t.equals(isEmail('@m.com'), errors.email)
  t.end()
})

test('Department code validation', (t) => {
  t.equals(isDepartmentCode('123-123'), noError)
  t.equals(isDepartmentCode('123123'), errors.departmentCode)
  t.end()
})
