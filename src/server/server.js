import path from 'path'
import cli from 'commander'
import express from 'express'
import cookieParser from 'cookie-parser'
import winston from 'winston'
import expressWinston from 'express-winston'
import compression from 'compression'

import {render} from './render'

const proxy = require('http-proxy-middleware')

cli
  .option('-p --port [port_number]', 'Set port')
  .parse(process.argv)

const app = express()

app.disable('x-powered-by')
app.use(cookieParser())
app.use(compression())

if (process.env.ENV !== 'local') {
  app.use(expressWinston.logger({
    transports: [
      new winston.transports.Console({
        colorize: true,
        level: 'info'
      })
    ]
  }))
}

app.get('/', render)

if (process.env.ENV === 'local') {
  // Serve fake favicon (in test/prod environment favicon is served by nginx)
  app.get('/favicon.ico', (_, res) => res.send(''))
  // Serve static
  app.use(express.static(path.resolve(__dirname, '../..')))
  // Proxy api
  app.use('/api', proxy({
    target: process.env.API_PROXY_URL,
    pathRewrite: {'^/api': ''},
    changeOrigin: true,
    cookieDomainRewrite: ''
  }))
}

app.get('*', render)

// Log errors into the file
app.use(expressWinston.errorLogger({
  transports: [
    new winston.transports.File({
      json: true,
      filename: 'errors.log',
      maxFiles: 1,
      maxsize: 10485760, // 10mb
      tailable: true
    })
  ]
}))

app.listen(cli.port)

console.log(`SSR server listen on port: ${cli.port}`) // eslint-disable-line no-console
