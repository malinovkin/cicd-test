import * as R from 'ramda'
import path from 'path'
import fs from 'fs'
import React from 'react'
import {renderToString} from 'react-dom/server'

import {Col, Container, Icon, Link, Row, Text, Title} from '../base'
import {App} from '../modules/app'
import {getDataFromQuery} from '../get_data_from_query'
import {parseParams, persistParams} from '../modules/shared/filkos_pixel'
import {StoreProvider, StaticRouterProvider} from '../context'
import {Actions, configureStore} from '../modules/state'

function ServerError () {
  return (
    <Container className='u-pb-4'>
      <Row className='u-mt-4-sm u-mt-2 u-pb-8'>
        <Col className='u-text-align-center'>
          <Icon name='finspin-logo-icon' />
        </Col>
      </Row>
      <Row className='u-pt-8-sm'>
        <Col className='u-text-align-center'>
          <Icon name='smile-sad-transparent-icon' />
        </Col>
      </Row>
      <Row className='u-mt-2'>
        <Col className='u-text-align-center'>
          <Title size={2}>Что-то пошло не так</Title>
        </Col>
      </Row>
      <Row className='u-mt-2'>
        <Col md={8} offsetMd={2} className='u-text-align-center'>
          <Text>
            Извините, кажется у нас что-то сломалось.
            Мы уже знаем об этом и работаем над устранением проблемы.
            Пожалуйста, попробуйте повторить попытку через несколько минут,
            или напишите нам в службу поддержки{' '}
            <Link href='mailto:support@finspin.ru'>support@finspin.ru</Link>.
          </Text>
        </Col>
      </Row>
    </Container>
  )
}

function readTemplateFromFile (fileName) {
  const templatePath = path.resolve(process.cwd(), fileName)
  if (!fs.existsSync(templatePath)) {
    console.error(`Can not read the ${fileName} file`)
  }

  return fs.readFileSync(templatePath, 'utf8')
}

function fillTemplate (t, {body, state}) {
  return t.replace('\'STATE\'', JSON.stringify(state))
    .replace('<!--APP-->', `${body}`)
}

function removeJSBundleScriptTag (t) {
  return t.replace(/<!--JS-->.*\n.*\n.*<!--JS-->/g, '')
}

// Persist template in memory
const template = readTemplateFromFile('index.html')

export function render (req, res) {
  console.log('Render for url:', req.url)
  const queryData = getDataFromQuery(req.query)

  const filkoParams = parseParams(queryData, () => req.cookies)
  // let filkoCookies
  // persistCookie(filkoParams, (cookies) => {
  //   filkoCookies = cookies
  // })

  const store = configureStore()

  if (!R.isEmpty(filkoParams)) {
    persistParams(filkoParams, store.dispatch)
  }

  if (queryData.phone) {
    store.dispatch(Actions.setUserPhone(queryData.phone))
  }

  if (!R.isEmpty(queryData.leadProviderParams)) {
    store.dispatch(Actions.addLeadProviderParams(queryData.leadProviderParams))
  }

  R.keys(req.cookies).forEach(name => {
    res.cookie(name, req.cookies[name], {path: '/'})
  })

  try {
    store.dispatch(Actions.checkAuthCookie(req.cookies))

    const routerContext = {}

    const html = renderToString(
      <StoreProvider store={store}>
        <StaticRouterProvider
          context={routerContext}
          location={req.url}
        >
          <App />
        </StaticRouterProvider>
      </StoreProvider>
    )

    // Redirect with 301 HTTP status
    if (routerContext.url) {
      res.writeHead(301, {
        Location: routerContext.url
      })
      res.end()
    } else {
      res.send(fillTemplate(template, {
        body: html,
        state: store.getState()
      }))
    }
  } catch (e) {
    if (process.env.ENV !== 'production') {
      res.send(e.stack)
    } else {
      res.send(fillTemplate(removeJSBundleScriptTag(template), {
        body: renderToString(<ServerError />),
        state: 'Server error occured, state did not prepare'
      }))
    }
  }
}
