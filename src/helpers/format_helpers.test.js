import test from 'tape'
import moment from 'moment'
import {
  formatDayCount,
  formatGender,
  formatISODateToTextField,
  formatMoney,
  formatPhone,
  formatRemainingTime,
  formatTimer,
  parseConfirmationCode,
  parsePhone,
  parseTextFieldDate
} from './format_helpers'

if (process.env.THEME !== 'eng') {
  test('Parse text field date', t => {
    t.equals(parseTextFieldDate('01.10.2010'), '2010-10-01')
    t.end()
  })

  test('Format ISO date for text field', t => {
    t.equals(
      formatISODateToTextField(moment('2010-10-01').toISOString()),
      '01.10.2010'
    )
    t.end()
  })

  test('Format day count', t => {
    t.equals(formatDayCount(1), 'день')
    t.equals(formatDayCount(2), 'дня')
    t.equals(formatDayCount(10), 'дней')
    t.equals(formatDayCount(100), 'дней')
    t.end()
  })

  test('Parse phone', t => {
    t.equals(parsePhone('+7 (123) 456-78-90'), '1234567890')
    t.equals(parsePhone('8 (123) 456-78-90'), '1234567890')
    t.end()
  })

  test('Format phone', t => {
    t.equals(formatPhone('1234567890'), '+7 (123) 456-78-90')
    t.equals(
      formatPhone('1234567890', { firstDigit: '8' }),
      '8 (123) 456-78-90'
    )
    t.end()
  })

  test('Format money', t => {
    t.equals(formatMoney(10), '10 ₽')
    t.equals(formatMoney(1000), '1 000 ₽')
    t.equals(formatMoney(1000000), '1 000 000 ₽')
    t.end()
  })

  test('Format gender', t => {
    t.equals(formatGender('male'), 'Мужчина')
    t.equals(formatGender('female'), 'Женщина')
    t.equals(formatGender(''), '')
    t.end()
  })

  test('Format remaining time', t => {
    t.equals(formatRemainingTime(1), 'сегодня последний день')
    t.equals(formatRemainingTime(2), 'осталось 2 дня')
    t.equals(formatRemainingTime(10), 'осталось 10 дней')
    t.equals(formatRemainingTime(21), 'остался 21 день')
    t.end()
  })

  test('Parse verification code', t => {
    t.equals(parseConfirmationCode('1 2 3'), '123')
    t.equals(parseConfirmationCode('123'), '123')
    t.equals(parseConfirmationCode('1-2-3'), '123')
    t.end()
  })

  test('Format timer', t => {
    t.equals(formatTimer(60), '01:00')
    t.equals(formatTimer(100), '01:40')
    t.equals(formatTimer(61), '01:01')
    t.equals(formatTimer(30), '00:30')
    t.equals(formatTimer(3), '00:03')
    t.end()
  })
}
