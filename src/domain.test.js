import test from 'tape'
import moment from 'moment'
import * as Domain from './domain'

function todayPlus (days) {
  return moment().add(days, 'days').format('YYYY-MM-DD')
}

test('Calc overpayment', (t) => {
  t.equals(Domain.calcOverpayment(100, 10, 2), 20)
  t.end()
})

test('Get loan amount or default', (t) => {
  t.equals(Domain.getLoanAmountOrDefault(100, 200, 120), 120)
  t.equals(Domain.getLoanAmountOrDefault(100, 200), 150)
  t.end()
})

test('Get loan term or default', (t) => {
  t.equals(Domain.getLoanTermOrDefault(10, 20, 12), 12)
  t.equals(Domain.getLoanTermOrDefault(10, 20), 15)
  t.end()
})

test('Calc total loan amount', (t) => {
  t.equals(Domain.calcTotalLoanAmount(1000, 200), 1200)
  t.end()
})

test('Calc remaining days until overdue', (t) => {
  t.equals(Domain.calcRemainingDaysUntilOverdue(todayPlus(0)), 1)
  t.equals(Domain.calcRemainingDaysUntilOverdue(todayPlus(1)), 2)
  t.end()
})

test('Calc remaining days until expired', (t) => {
  t.equals(Domain.calcRemainingDaysUntilExpiration(todayPlus(0)), 1)
  t.equals(Domain.calcRemainingDaysUntilExpiration(todayPlus(1)), 1)
  t.end()
})

const loans = [
  {id: '0', status: Domain.LoanStatus.Approved},
  {id: '1', status: Domain.LoanStatus.Repaid},
  {id: '2', status: Domain.LoanStatus.Overdue}
]

test('Split open loans', (t) => {
  const [active, other] = Domain.splitOpenLoans(loans)

  t.deepEquals(active, [loans[2]])
  t.deepEquals(other, [loans[0], loans[1]])
  t.end()
})
