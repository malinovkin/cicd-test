const needMetrics = process.env.METRICS

export function changePage (pageName) {
  if (needMetrics) {
    window.dataLayer.push({event: 'page', pageName})
  }
}
