import * as R from 'ramda'
import moment from 'moment'

export function calcOverpayment (amount, term, rate) {
  return amount * term * (rate / 100)
}

export function getLoanAmountOrDefault (minAmount, maxAmount, amount) {
  return R.isNil(amount) ? Math.round(R.mean([minAmount, maxAmount])) : amount
}

export function getLoanTermOrDefault (minTerm, maxTerm, term) {
  return R.isNil(term) ? Math.round(R.mean([minTerm, maxTerm])) : term
}

export function calcTotalLoanAmount (base, overpayment) {
  return base + overpayment
}

export function calcRemainingDaysUntilOverdue (deadline) {
  const overdue = moment(deadline).add(1, 'days')
  return moment(overdue).diff(moment(), 'days') + 1
}

export function calcRemainingDaysUntilExpiration (expirationDate) {
  return moment(expirationDate).diff(moment(), 'days') + 1
}

export const Creditor = {
  PayPS: 'payPS',
  SmsFinance: 'smsFinance',
  DoZarplaty: 'dzp',
  Kreditech: 'kreditech',
  SlonFinance: 'slonFinance',
  OneClickMoney: 'oneClickMoney',
  Kviku: 'kviku',
  EcoFinance: 'ecoFinance',
  WebBankir: 'webBankir',
  MoneyMan: 'moneyMan',
  HoneyMoney: 'honeyMoney'

}

export const LoanStatus = {
  Incomplete: 'incomplete',
  Approved: 'approved',
  Rejected: 'rejected',
  Overdue: 'overdue',
  Processed: 'processed',
  Serviced: 'serviced',
  Repaid: 'repaid',
  Expired: 'expired',
  MoneyTransfer: 'moneyTransfer'
}

export const AppDataType = {
  LoanParams: 'loanParams',
  Base: 'base',
  Common: 'common',
  Additional: 'additional',
  OnlyAgreements: 'onlyAgreements'
}

export const MarriageStatuses = [
  {value: 0, caption: 'Не женат (не замужем)'},
  {value: 1, caption: 'В браке'},
  {value: 2, caption: 'В разводе'},
  {value: 3, caption: 'Гражданский брак'},
  {value: 4, caption: 'Повторный брак'},
  {value: 5, caption: 'Вдовец (вдова)'}
]

export const JobTypes = [
  {value: 0, caption: 'Охрана'},
  {value: 1, caption: 'Военная служба'},
  {value: 2, caption: 'Государственная служба'},
  {value: 3, caption: 'Информационные технологии'},
  {value: 4, caption: 'Консалтинг'},
  {value: 5, caption: 'Медицина'},
  {value: 6, caption: 'Наука'},
  {value: 7, caption: 'Образование'},
  {value: 8, caption: 'Промышленность'},
  {value: 9, caption: 'Некоммерческая организация'},
  {value: 10, caption: 'Строительство'},
  {value: 11, caption: 'Услуги'},
  {value: 12, caption: 'Энергетика'},
  {value: 13, caption: 'Продажи'},
  {value: 14, caption: 'Транспорт'},
  {value: 15, caption: 'Туризм'},
  {value: 16, caption: 'Финансы'},
  {value: 17, caption: 'Другое'}
]

export const JobExperience = [
  {value: 0, caption: 'До 6 месяцев'},
  {value: 1, caption: 'От 6 месяцев до года'},
  {value: 2, caption: 'От 1 года до 5 лет'},
  {value: 3, caption: 'Более 5 лет'}
]

export const JobEmployees = [
  {value: 0, caption: '1-10'},
  {value: 1, caption: '11-50'},
  {value: 2, caption: '51-100'},
  {value: 3, caption: '101-1000'},
  {value: 4, caption: 'более 1000'}
]

export const ChildrenAmounts = [
  {value: 0, caption: 'нет'},
  ...R.range(1, 11)
    .map(value => ({value, caption: value.toString()}))
]

export const AgreementName = {
  CreditorRulesAcceptance: 'creditorRulesAcceptance',
  CreditInfoDisclosure: 'creditInfoDisclosure',
  ProcessingPersonalData: 'processingPersonalData',
  ServiceContract: 'serviceContract',
  IsNotStateOfficial: 'isNotStateOfficial',
  IsNotForeignCitizen: 'isNotForeignCitizen',
  BeneficiaryIsAbsent: 'beneficiaryIsAbsent',
  BeneficialOwnerIsAbsent: 'beneficialOwnerIsAbsent',
  InfoReceiving: 'infoReceiving',
  PersonalSignUse: 'personalSignUse',
  CommonConditions: 'commonConditions',
  LoanRequest: 'loanRequest',
  SendInfoTo3rdParty: 'sendInfoTo3rdParty',
  Collector: 'collector',
  ProcessingPersonalDataPolicy: 'processingPersonalDataPolicy'
}

export const AgreementVendor = {
  Finspin: 'finspin',
  SmsFinance: 'smsFinance',
  PayPS: 'payPS',
  DoZarplaty: 'dzp',
  SlonFinance: 'slonFinance',
  OneClickMoney: 'oneClickMoney'
}

export const ContactType = [
  {value: 0, caption: 'Друг/Подруга'},
  {value: 1, caption: 'Коллега'},
  {value: 2, caption: 'Близкий родственник'},
  {value: 3, caption: 'Дальний родственник'},
  {value: 4, caption: 'Знакомый'}
]

export const Education = [
  {value: 0, caption: 'Отсутствует'},
  {value: 1, caption: 'Неполное среднее'},
  {value: 2, caption: 'Среднее'},
  {value: 3, caption: 'Средне-специальное'},
  {value: 4, caption: 'Незаконченное высшее'},
  {value: 5, caption: 'Высшее'},
  {value: 6, caption: '2 высших или учёная степень'}
]

const activeLoanStatuses = [LoanStatus.Serviced, LoanStatus.Overdue]

/**
 * @param loans
 * @returns {Array} - Где 0 элемент - это active loans, 1 - other loans
 */
export function splitOpenLoans (loans) {
  return R.partition(el => R.contains(el.status, activeLoanStatuses), loans)
}
