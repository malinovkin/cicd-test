const R = require('ramda')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const HtmlWebpackExcludeAssetsPlugin = require('html-webpack-exclude-assets-plugin')
const path = require('path')
const autoprefixer = require('autoprefixer')

const AppConfig = require('./app_config')

module.exports = {
  mode: AppConfig.env[process.env.ENV].NODE_ENV,
  stats: 'errors-only',
  devtool: '#eval-source-map',

  entry: {
    'shared.styles': './src/demo/shared.css',
    'default.theme_styles': './src/styles/index_default.scss',
    'tele2.theme_styles': './src/styles/index_tele2.scss',
    'beeline.theme_styles': './src/styles/index_beeline.scss',
    demo: './src/demo/demo.jsx'
  },

  output: {
    path: path.resolve(path.resolve(__dirname, 'src', 'demo'), '__build__'),
    filename: '[name].js'
  },

  resolve: {
    modules: [path.resolve(__dirname, 'src'), 'node_modules'],
    extensions: ['.js', '.jsx']
  },

  devServer: {
    contentBase: path.resolve(__dirname, 'src/demo'),
    stats: 'errors-only'
  },

  module: {
    rules: [
      {test: /\.jsx?$/, exclude: /node_modules/, loader: 'babel-loader'},
      {
        test: /\.(scss|css)$/,
        use: [
          'style-loader',
          'css-loader',
          {loader: 'postcss-loader', options: {plugins: [autoprefixer]}},
          'sass-loader'
        ]
      },
      {
        test: /\.(svg|gif|png|jpg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader',
        options: {
          name: '[name]-[sha512:hash:base64:7].[ext]',
          outputPath: 'static/assets/'
        }
      },
      {
        test: /\.(woff|woff2|eot)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
          outputPath: 'static/assets/'
        }
      }
    ]
  },

  plugins: [
    new webpack.DefinePlugin({
      'process.env': R.mapObjIndexed(
        JSON.stringify,
        R.mergeAll([
          AppConfig.env[process.env.ENV],
          AppConfig.string.default
        ])
      )
    }),

    new HtmlWebpackPlugin({
      template: 'src/demo/index.html',
      hash: false,
      inject: 'body',
      excludeAssets: [/.theme_styles.js/]
    }),

    new HtmlWebpackExcludeAssetsPlugin()
  ]
}
