const R = require('ramda')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const autoprefixer = require('autoprefixer')

const Utils = require('./bin/utils')
const AppConfig = require('./app_config')

const isLocalDevelopment = process.env.ENV === 'local'

module.exports = {
  mode: AppConfig.env[process.env.ENV].NODE_ENV,
  stats: 'errors-only',
  devtool: isLocalDevelopment ? 'cheap-module-eval-source-map' : 'source-map',

  entry: [
    'core-js/es6/map', // required by React
    'core-js/es6/set', // required by React
    'core-js/es6/promise',
    'core-js/es6/object', // TODO: need to check, is used Object API or not, if not, just remove this polyfill
    'core-js/fn/array/find', // required by nouislider
    'raf/polyfill', // required by React
    './src/client.jsx'
  ],

  output: {
    filename: isLocalDevelopment ? 'static/js/app.js' : `static/js/[name].[chunkhash].js`,
    path: path.resolve(__dirname, `build/${process.env.THEME}`),
    publicPath: '/'
  },

  resolve: {
    modules: [path.resolve(__dirname, 'src'), 'node_modules'],
    extensions: ['.js', '.jsx']
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.(scss|css)$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          {loader: 'postcss-loader', options: {plugins: [autoprefixer]}},
          'sass-loader'
        ]
      },
      {
        test: /\.(svg|gif|png|jpg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader',
        options: {
          name: '[name]-[sha512:hash:base64:7].[ext]',
          outputPath: 'static/assets/'
        }
      },
      {
        test: /\.(woff|woff2|eot)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
          outputPath: 'static/assets/'
        }
      }
    ]
  },

  plugins: [
    new webpack.DefinePlugin({
      'process.env': R.mapObjIndexed(
        JSON.stringify,
        R.mergeAll([
          AppConfig.string[process.env.THEME],
          AppConfig.env[process.env.ENV],
          {
            THEME: process.env.THEME,
            COMMIT_SHA: Utils.getHeadSHA(),
            BRANCH_NAME: Utils.getHeadBranchName()
          }
        ]))
    }),

    new HtmlWebpackPlugin({
      inject: false,
      template: 'src/index.html',
      hash: false,
      favicon: isLocalDevelopment ? null : './src/styles/assets/favicon.png'
    }),

    new MiniCssExtractPlugin({
      filename: 'static/css/styles.[contenthash].css'
    }),

    new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /ru/)
  ]
}
